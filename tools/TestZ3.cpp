#include <iostream>
#include <filesystem>

#include "antlr4-runtime.h"
#include "Parser/SMTLIBv2Lexer__gen.h"
#include "Parser/SMTLIBv2Parser__gen.h"
#include "Parser/SMTLIBv2Visitor.h"

#include "Support/Ref.h"
#include "Expr/expr.h"
#include "Solver/Z3/Z3Engine.h"

namespace fs = std::filesystem;

using namespace MBSSL;

int main(int argc, char** argv) {
  std::string in_file_name;
  std::string out_file_name;
  for (int idx = 1; idx < argc; idx++) {
    if (strcmp(argv[idx], "-o") == 0)
      out_file_name = argv[++idx];
    else
      in_file_name = argv[idx];
  }
  fs::path in_file_path(in_file_name);
  fs::path out_file_path(out_file_name);
  std::ifstream in_file_stream;
  in_file_stream.open(in_file_name, std::ios::in);
  std::ofstream out_file_stream;
  out_file_stream.open(out_file_name, std::ios::out);

  antlr4::ANTLRInputStream antlr_input_stream(in_file_stream);
  SMTLIBv2Lexer lexer(&antlr_input_stream);
  antlr4::CommonTokenStream common_token_stream(&lexer);
  SMTLIBv2Parser parser(&common_token_stream);
  MBSSL::SMTLIBv2Visitor visitor(mkref<Solver>(mkref<Z3::Engine>()));
  visitor.visitStart(parser.start());

  return 0;
}
