#===------------------------------------------------------------------------===#
#
#                     The KLEE Symbolic Virtual Machine
#
# This file is distributed under the University of Illinois Open Source
# License. See LICENSE.TXT for details.
#
#===------------------------------------------------------------------------===#

mbssl_add_component(MBSSLSolver
  Response.cpp
  Solver.cpp
  Z3/Z3Adapter.cpp
  Z3/Z3Engine.cpp
  CVC5/CVC5Adapter.cpp
  CVC5/CVC5Engine.cpp
  JFS/JFSBuilder.cpp
  JFS/JFSEngine.cpp
)

target_link_libraries(MBSSLSolver PUBLIC
  ${Z3_LIBRARIES}
  JFSSupport
  JFSCore
  JFSTransform
  JFSZ3Backend
  JFSFuzzingCommon
  JFSCXXFuzzingBackend
  JFSCXXFuzzingBackendCmdLine
  JFSFuzzingCommonCmdLine
)
