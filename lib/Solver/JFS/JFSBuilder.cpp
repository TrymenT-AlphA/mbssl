//===-- DRealBuilder.cpp ------------------------------------------*- C++ -*-====//
//
//                     The KLEE Symbolic Virtual Machine
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include <z3++.h>
#include "jfs/JFSBuilder.h"
#include "llvm/Support/CommandLine.h"
using namespace klee;

namespace klee {
std::unique_ptr<jfs::fuzzingCommon::WorkingDirectoryManager>
JFSSolver::makeWorkingDirectory(jfs::core::JFSContext& ctx) {
  // Use the current working directory as the base directory
  // and use as a prefix the name of the query.
  llvm::SmallVector<char, 256> currentDir;
  if (auto ec = llvm::sys::fs::current_path(currentDir)) {
    llvm::errs()
    << "(error failed to get current working directory because "
    << ec.message() << ")\n";
    exit(1);
  }
  llvm::StringRef currentDirAsStringRef(currentDir.data(), currentDir.size());
  return jfs::fuzzingCommon::WorkingDirectoryManager::makeInDirectory(
          /*directory=*/currentDirAsStringRef, /*prefix=*/"fuzz", ctx,
          true);
}


std::unique_ptr<jfs::core::Solver>
JFSSolver::makeCXXFuzzingSolver(jfs::core::JFSContext& ctx,
                     std::unique_ptr<jfs::fuzzingCommon::WorkingDirectoryManager> wdm,
                     std::map<std::string, uint64_t> &fuzzSeeds,
                     uint64_t maxFuzzTime) {
  std::unique_ptr<jfs::core::Solver> solver;
  // Tell ClangOptions to try and infer all paths
  auto clangOptions =
          jfs::cxxfb::cl::buildClangOptionsFromCmdLine(pathToExecutable);
  // add by zgf to transfer uninterpreter funtions support
  clangOptions->targetLinkedLibs = {};

  auto seedManagerOpts =
          jfs::fuzzingCommon::cl::buildSeedManagerOptionsFromCmdLineWithSeed(fuzzSeeds);

  auto libFuzzerOptions =
          jfs::fuzzingCommon::cl::buildLibFuzzerOptionsFromCmdLine();

  libFuzzerOptions->maxTotalTime = maxFuzzTime;
  libFuzzerOptions->useClangCoverage = true;
  libFuzzerOptions->useCmp = true;
  libFuzzerOptions->useCounters = true;
  libFuzzerOptions->useValueProfile = false;

  auto cxxProgramBuilderOptions =
          jfs::cxxfb::cl::buildCXXProgramBuilderOptionsFromCmdLine();
  auto freeVariableToBufferAssignmentPassOptions =
          jfs::fuzzingCommon::cl::buildFVTBAPOptionsFromCmdLine();
  std::unique_ptr<jfs::cxxfb::CXXFuzzingSolverOptions> solverOptions(
          new jfs::cxxfb::CXXFuzzingSolverOptions(
                  std::move(freeVariableToBufferAssignmentPassOptions),
                  std::move(clangOptions), std::move(libFuzzerOptions),
                  std::move(cxxProgramBuilderOptions), std::move(seedManagerOpts),
                  false));
  // Decide if the clang/LibFuzzer stdout/stderr should be redirected
  solverOptions->redirectClangOutput = false;
  solverOptions->redirectLibFuzzerOutput = true;

  solver.reset(new jfs::cxxfb::CXXFuzzingSolver(std::move(solverOptions),
                                                    std::move(wdm), ctx));
  return solver;
}

bool JFSSolver::invokeJFSGetFuzzingResult(
    const std::string& smtLibStr,
    std::map<std::string, std::vector<unsigned char>> &lastAssign,
    std::map<std::string, uint64_t> &fuzzSeeds){
  // Create context
  int Verbosity = 0;
  unsigned long long Seed = 1;
  // Max allowed solver time (seconds), 0 which means no maximum
//  uint64_t MaxFuzzTime = std::stoi(MaxCoreSolverTime);
  uint64_t MaxFuzzTime = 30;

  jfs::core::JFSContextConfig ctxCfg;
  ctxCfg.verbosity = Verbosity;
  ctxCfg.gathericStatistics = false;
  ctxCfg.seed = Seed;

  jfs::core::JFSContext ctx(ctxCfg);
  jfs::core::ToolErrorHandler toolHandler(/*ignoreCanceled*/ true);
  jfs::core::ScopedJFSContextErrorHandler errorHandler(ctx, &toolHandler);

  // Create parser
  jfs::core::SMTLIB2Parser parser(ctx);
  // Create pass manager
  jfs::transform::QueryPassManager pm;
  // Create working directory and solver
  std::unique_ptr<jfs::core::Solver> jfsSolver(
          makeCXXFuzzingSolver(ctx,
                               makeWorkingDirectory(ctx),
                               fuzzSeeds,
                               MaxFuzzTime));

  // Parse query
  std::shared_ptr<jfs::core::Query> query;

  auto bufferOrError = llvm::MemoryBuffer::getMemBuffer(smtLibStr);
  auto buffer(std::move(bufferOrError));
  query = parser.parseMemoryBuffer(std::move(buffer));

  // add by zgf to assign function declare info
  //query->setFunctionsTypeMap(basicFuncsTypeTable);

  // note by zgf : AddStandardPasses means we can write our pass add into it
  jfs::transform::AddStandardPasses(pm);
  pm.run(*query);

  auto response = jfsSolver->solve(*query, /*produceModel=*/true);

  if (response->sat == jfs::core::SolverResponse::SAT) {
    auto model = response->getModel();
    if (model) {
      // add by zgf to get result from MODEL (Little-Endian str)
      jfs::core::Z3ModelHandle z3Model = model->getRepr();
      uint64_t numAssignments = z3Model.getNumAssignments();
      std::map<std::string, int> arrNameMap;

      for (uint64_t index = 0; index < numAssignments; ++index) {
        jfs::core::Z3FuncDeclHandle decl =
            z3Model.getVariableDeclForIndex(index);
        unsigned varBytes = decl.getSort().getWidth() / 8;
        std::vector<unsigned char> data;
        data.reserve(varBytes);

        jfs::core::Z3ASTHandle assignment = z3Model.getAssignmentFor(decl);
        std::string assignHEXLittleEndian(
            z3Model.getAssignmentFor(decl).toStr().substr(2));
        unsigned int hexSize = assignHEXLittleEndian.size() >> 1;

        unsigned int idx = 0;
        // if array size larger than Fuzz result, fill gap with zero
        for(; idx < varBytes - hexSize; idx++)
          data.push_back(0);

        // actually get Fuzz result
        for(idx = 0; idx < hexSize; idx++) {
          std::string charHex(assignHEXLittleEndian.substr(idx * 2,2));
          unsigned char byteValue = stoi(charHex,0,16);
          data.push_back(byteValue);
        }

        lastAssign.insert(std::make_pair(decl.getName(), data));
      }
    }
  }
  return false;
}


}

