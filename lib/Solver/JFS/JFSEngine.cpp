#include "Solver/JFS/JFSEngine.h"
#include "Support/Error.h"
#include "Expr/expr.h"

#include <iostream>
#include <fstream>
#include <filesystem>
#include <utility>

namespace fs = std::filesystem;

namespace MBSSL {
  namespace JFS {

    JFSEngine::JFSEngine(std::string filename, std::string pathToExecutable) :
      filename(std::move(filename)) {
      jfsSolver.setPathToExecutable(pathToExecutable);
    }

    void JFSEngine::Assert(ref<Expr> expr) noexcept {}

    ref<Response> JFSEngine::CheckSat() noexcept {
      std::ifstream in_file_stream;
      in_file_stream.open(filename, std::ios::in);
      std::stringstream ss;
      ss << in_file_stream.rdbuf();
      std::string smtLibStr = ss.str();
      success = jfsSolver.invokeJFSGetFuzzingResult(smtLibStr, lastAssign, fuzzSeeds);
      if (success) return Sat();
      return Unknown();
    }

    void JFSEngine::ResetAssertions() noexcept {
      throw Unsupported(__PRETTY_FUNCTION__);
    }

    ref<Response> JFSEngine::Prove(ref<Expr> expr) const noexcept {
      throw Unsupported(__PRETTY_FUNCTION__);
    }

    void JFSEngine::getAssignFromModel() noexcept {
      for (const auto& assign : lastAssign){
        std::cout << "Var : " << assign.first << "\n";
        for (auto val : assign.second)
          std::cout << std::hex << std::setfill('0') << std::setw(2) << (int) val ;
        std::cout << "\n";
      }
    }

  }
}
