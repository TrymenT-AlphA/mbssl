#include "Solver/CVC5/CVC5Adapter.h"
#include "Expr/expr.h"

using namespace qsym;

namespace MBSSL {
  namespace CVC5 {
      // cvc5::Sort CVC5Builder::getRoundingMode() {
      //   return solver.mkRoundingMode();
      // }
      cvc5::Sort CVC5Builder::getBvSort(unsigned width) {
        return solver.mkBitVectorSort(width);
      }
      cvc5::Term CVC5Builder::getTrue() {
        return solver.mkBoolean(true);
      }
      cvc5::Term CVC5Builder::getFalse() {
        return solver.mkBoolean(false);
      }
      cvc5::Term CVC5Builder::varExpr(const std::string &name, cvc5::Sort sort) {
        return solver.mkConst(sort, name);
      }
      cvc5::Sort CVC5Builder::toCVC5Sort(ref<Sort> sort) {
        switch (sort->getSortT()) {
          case Sort_t::Uninterpreted:{
            // Z3_symbol nameSym = Z3_mk_string_symbol(
            //     getCtx(), std::to_string(reinterpret_cast<size_t>(sort.get())).c_str());
            // return Z3SortHandle(Z3_mk_uninterpreted_sort(getCtx(), nameSym), getCtx());
            std:string nameSym = std::to_string(reinterpret_cast<size_t>(sort.get()));
            return solver.mkUninterpretedSort(nameSym);
          }
          case Sort_t::Bool:{
            // return Z3SortHandle(Z3_mk_bool_sort(getCtx()), getCtx());
            return solver.getBooleanSort();
          }
          case Sort_t::Array:{
            // Z3SortHandle domainSort = toZ3Sort(getDomain(sort));
            // Z3SortHandle rangeSort = toZ3Sort(getRange(sort));
            // return Z3SortHandle(Z3_mk_array_sort(getCtx(), domainSort, rangeSort), getCtx());
            cvc5::Sort domainSort = toCVC5Sort(getDomain(sort));
            cvc5::Sort rangeSort = toCVC5Sort(getRange(sort));
            return solver.mkArraySort(domainSort, rangeSort);
          }
          case Sort_t::BitVec:{
            // return Z3SortHandle(Z3_mk_bv_sort(getCtx(), getWidth(sort)), getCtx());
            return solver.mkBitVectorSort(getWidth(sort));
          }
          case Sort_t::RoundingMode:{
            // return Z3SortHandle(Z3_mk_fpa_rounding_mode_sort(getCtx()), getCtx());
            return solver.getRoundingModeSort();
          }
  //    case Sort_t::Real:{
  //      return Z3SortHandle(context.real_sort());
  //    }
          case Sort_t::FloatingPoint:{
            // return Z3SortHandle(
                // Z3_mk_fpa_sort(getCtx(),geteb(sort),getsb(sort)), getCtx());
            return solver.mkFloatingPointSort(geteb(sort), getsb(sort));
          }
          default:{
            throw Unsupported(__PRETTY_FUNCTION__);
          }
        }
      }
      cvc5::Term CVC5Builder::toCVC5Expr(ref<Expr> e) {
        if (e->isConstant()) {
          return toCVC5ExprActual(e);
        } else {
          ExprHashMap<cvc5::Term>::iterator it = consCache.find(e);
          if (it != consCache.end()) {
            return it->second;
          } else {
            cvc5::Term res = toCVC5ExprActual(e);
            consCache.insert(std::make_pair(e,res));
            return res;
          }
        }
      }
      cvc5::Term CVC5Builder::toCVC5ExprActual(ref<Expr> expr) {
        if (auto appExpr = castAs<AppExpr>(expr)) {
          if (appExpr->num_children() == 0){
            ref<Expr> func = appExpr->funcExpr;
            if (auto it = consCache.find(func); it != consCache.end())
              return it->second;
            // Z3_symbol nameSym = Z3_mk_string_symbol(
            //     getCtx(), std::to_string(reinterpret_cast<size_t>(expr.get())).c_str());
            std:string nameSym = std::to_string(reinterpret_cast<size_t>(expr.get()));
            cvc5::Sort sort = toCVC5Sort(expr->getSort());
            // cvc5::Term variable = auto(Z3_mk_const(getCtx(), nameSym, sort), getCtx());
            cvc5::Term variable = solver.mkConst(sort, nameSym);
            consCache[func] = variable;
            return variable;
          }
        }

        switch (expr->kind()) {
                //    case Forall:{
        //      z3::expr_vector xs(context);
        //      for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
        //        xs.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
        //      return mkref<Adapter::toCVC5Expr_t>(z3::forall(
        //        xs,
        //        *(*this)(expr->getChild(0))
        //      ));
        //    }
        //    case Exists:{
        //      z3::expr_vector xs(context);
        //      for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
        //        xs.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
        //      return mkref<Adapter::toCVC5Expr_t>(z3::forall(
        //        xs,
        //        *(*this)(expr->getChild(0))
        //      ));
        //    }
        case Para:{
          // Z3_symbol nameSym = Z3_mk_string_symbol(
          //     getCtx(), std::to_string(reinterpret_cast<size_t>(expr.get())).c_str());
          // Z3SortHandle sort = toZ3Sort(expr->getSort());
          // auto variable = auto(Z3_mk_const(getCtx(), nameSym, sort), getCtx());
          // return variable;
          string nameSym = std::to_string(reinterpret_cast<size_t>(expr.get()));
          cvc5::Sort sort = toCVC5Sort(expr->getSort());
          cvc5::Term variable = solver.mkConst(sort, nameSym);
          return variable;
        }
        case App:{
          ref<AppExpr> appExpr = castAs<AppExpr>(expr);
          ref<FuncExpr> funcExpr = castAs<FuncExpr>(appExpr->funcExpr);
          cvc5::Term f = toCVC5Func(funcExpr);
          unsigned argNum = expr->num_children();
          std::vector<cvc5::Term> args(argNum);
          for (unsigned idx = 0; idx < argNum; idx ++){
            args[idx] = toCVC5Expr(expr->getChild(idx));
          }
          // return auto(Z3_mk_app(getCtx(), f, argNum, args), getCtx());
          // return 
          // std::vector<Adapter::ToExpr_t> children;
          // children.reserve(expr->getOperandNum() + 1);
          // children.push_back(*(*this)(expr->getOp()->getParameter<ref<Function>>(0)));
          // for (const auto& each : expr->getOperands())
            // children.push_back(*(*this)(each));
          // return mkref<Adapter::ToExpr_t>(factory.mkTerm(cvc5::Kind::APPLY_UF, children));
          return solver.mkTerm(cvc5::Kind::APPLY_UF, args);
        }
        case Read:{
          ref<ReadExpr> readExprRef = castAs<ReadExpr>(expr);
          std::string name = readExprRef->name();
          UINT32 index = readExprRef->index();
          std::string symbol_name = name + "_" + std::to_string(index);
          cvc5::Sort sort = getBvSort(8);
          return varExpr(symbol_name, sort);
        }

          // Common
        case Bool:{
          ref<BoolExpr> boolExpr = castAs<BoolExpr>(expr);
          bool value = boolExpr->value();
          auto result = value ? getTrue() : getFalse();
          return result;
        }
        case Constant:{
          ConstantExprRef constantExprRef = castAs<ConstantExpr>(expr);
          llvm::APInt value = constantExprRef->value();
          UINT32 bits = constantExprRef->bits();
          if (bits == 32 || bits == 64){
            // FIXME: uint64_t ?
            uint64_t v = value.getZExtValue();
            // return auto(
            //     Z3_mk_unsigned_int64(getCtx(), v, getBvSort(bits)), getCtx());
            return solver.mkBitVector(bits, v);
          }else {
            std::string val_str = value.toString(10, false);
            // return auto(
            //     Z3_mk_numeral(getCtx(), val_str.c_str(), getBvSort(bits)), getCtx());
            return solver.mkInteger(val_str);
          }
        }
        case Concat:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // return auto(
          //     Z3_mk_concat(
          //         getCtx(),
          //         firstChild,
          //         secondChild) ,
          //         getCtx());
          return solver.mkTerm(
            cvc5::Kind::BITVECTOR_CONCAT,{
            firstChild,
            secondChild,
          });
        }
        case Extract:{
          ref<ExtractExpr> extractExprRef = castAs<ExtractExpr>(expr);
          UINT32 index = extractExprRef->index();
          UINT32 bits  = extractExprRef->bits();
          auto child = toCVC5Expr(expr->getChild(0));
          // return auto(Z3_mk_extract(getCtx(), index + bits - 1, index, child), getCtx());
          // return 
          return solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::BITVECTOR_EXTRACT,
              {index + bits - 1, index}
            ),
            {child}
          );
        }
        case Ite:{
          auto cond = toCVC5Expr(expr->getChild(0));
          auto trueVal = toCVC5Expr(expr->getChild(1));
          auto falseVal = toCVC5Expr(expr->getChild(2));
          // return auto(Z3_mk_ite(getCtx(), cond, trueVal, falseVal), getCtx());
          return solver.mkTerm(
            cvc5::Kind::ITE,
            {cond, trueVal, falseVal}
          );
        }

        // Array
        case Select:{
          auto _z3_array = toCVC5Expr(expr->getFirstChild()); //is already array, not bv sort !
          auto _z3_index = toCVC5Expr(expr->getSecondChild());
          // return auto(Z3_mk_select(getCtx(), _z3_array, _z3_index), getCtx());
          return solver.mkTerm(
            cvc5::Kind::SELECT,
            {_z3_array, _z3_index}
          );
        }
        case Store:{
          auto _z3_array = toCVC5Expr(expr->getChild(0)); //is already array, not bv sort !
          auto _z3_index = toCVC5Expr(expr->getChild(1));
          auto _z3_newV = toCVC5Expr(expr->getChild(2));
          // return auto(Z3_mk_store(getCtx(), _z3_array, _z3_index, _z3_newV), getCtx());
          return solver.mkTerm(
            cvc5::Kind::SELECT,
            {_z3_array, _z3_index, _z3_newV}
          );
        }

          // Logic
        case LNot:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_not(getCtx(), firstChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::NOT, {firstChild});
          return result;
        }
        case LAnd:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // Z3_ast args[2] = {firstChild, secondChild};
          // auto result = auto(Z3_mk_and(getCtx(), 2, args),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::AND, {firstChild, secondChild});
          return result;
        }
        case LOr:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // Z3_ast args[2] = {firstChild, secondChild};
          // auto result = auto(Z3_mk_or(getCtx(), 2, args),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::OR, {firstChild, secondChild});
          return result;
        }

        // Bits
        case Not:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_not(getCtx(), e),getCtx());
          return solver.mkTerm(cvc5::Kind::BITVECTOR_NOT, {e});
        }
        case Neg:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_bvneg(getCtx(), e),getCtx());
          return solver.mkTerm(cvc5::Kind::BITVECTOR_NEG, {e});
        }
        case And:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvand(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_AND, {firstChild, secondChild});
          return result;
        }
        case Or:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvor(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_OR, {firstChild, secondChild});
          return result;
        }
        case Xor:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvxor(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_XOR, {firstChild, secondChild});
          return result;
        }
        case Shl:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvshl(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SHL, {firstChild, secondChild});
          return result;
        }
        case LShr:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvlshr(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_LSHR, {firstChild, secondChild});
          return result;
        }
        case AShr:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvashr(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_ASHR, {firstChild, secondChild});
          return result;
        }
        case Rol:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_rotate_left(getCtx(), expr->bits(), e),getCtx());
          return solver.mkTerm(
            solver.mkOp(cvc5::Kind::BITVECTOR_ROTATE_LEFT, {expr->bits()}),
            {e}
          );
        }
        case Ror:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_rotate_right(getCtx(), expr->bits(), e),getCtx());
          return solver.mkTerm(
            solver.mkOp(cvc5::Kind::BITVECTOR_ROTATE_RIGHT, {expr->bits()}),
            {e}
          );
        }
        case ZExt:{
          UINT32 bits = expr->bits();
          UINT32 childBits = expr->getChild(0)->bits();
          UINT32 extBits = bits - childBits;
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_zero_ext(getCtx(), extBits, e),getCtx());
          return solver.mkTerm(
            solver.mkOp(cvc5::Kind::BITVECTOR_ZERO_EXTEND, {extBits}),
            {e}
          );
        }
        case SExt:{
          UINT32 bits = expr->bits();
          UINT32 childBits = expr->getChild(0)->bits();
          UINT32 extBits = bits - childBits;
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_sign_ext(getCtx(), extBits, e),getCtx());
                    return solver.mkTerm(
            solver.mkOp(cvc5::Kind::BITVECTOR_SIGN_EXTEND, {extBits}),
            {e}
          );
        }

        // Bv Arithmetic
        case Add:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvadd(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_ADD, {firstChild, secondChild});
          return result;
        }
        case Sub:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsub(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SUB, {firstChild, secondChild});
          return result;
        }
        case Mul:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvmul(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_MULT, {firstChild, secondChild});
          return result;
        }
        case UDiv:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvudiv(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_UDIV, {firstChild, secondChild});
          return result;
        }
        case SDiv:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsdiv(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SDIV, {firstChild, secondChild});
          return result;
        }
        case URem:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvurem(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_UREM, {firstChild, secondChild});
          return result;
        }
        case SRem:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsrem(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SREM, {firstChild, secondChild});
          return result;
        }
        case SMod:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsmod(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SMOD, {firstChild, secondChild});
          return result;
        }

        // Bv Compare
        case Equal:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_eq(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::EQUAL, {firstChild, secondChild});
          return result;
        }
        case Distinct:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto eqExpr = auto(Z3_mk_eq(getCtx(), firstChild, secondChild),getCtx());
          auto eqExpr = solver.mkTerm(cvc5::Kind::EQUAL, {firstChild, secondChild});
          // auto neqExpr = auto(Z3_mk_not(getCtx(), eqExpr),getCtx());
          auto neqExpr = solver.mkTerm(cvc5::Kind::NOT, {eqExpr});
          return neqExpr;
        }
        case Ult:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvult(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_ULT, {firstChild, secondChild});
          return result;
        }
        case Ule:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvule(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_ULE, {firstChild, secondChild});
          return result;
        }
        case Ugt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvugt(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_UGT, {firstChild, secondChild});
          return result;
        }
        case Uge:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvuge(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_UGE, {firstChild, secondChild});
          return result;
        }
        case Slt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvslt(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SLT, {firstChild, secondChild});
          return result;
        }
        case Sle:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsle(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SLE, {firstChild, secondChild});
          return result;
        }
        case Sgt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsgt(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SGT, {firstChild, secondChild});
          return result;
        }
        case Sge:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          // auto result = auto(Z3_mk_bvsge(getCtx(), firstChild, secondChild),getCtx());
          auto result = solver.mkTerm(cvc5::Kind::BITVECTOR_SGE, {firstChild, secondChild});
          return result;
        }

          // Rounding Mode
        case RNE:{
          // context.set_rounding_mode(z3::rounding_mode::RNE);
          // return auto(Z3_mk_fpa_rne(getCtx()), getCtx());
          return solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN);
        }
        case RNA:{
          // context.set_rounding_mode(z3::rounding_mode::RNA);
          // return auto(Z3_mk_fpa_rna(getCtx()), getCtx());
          return solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_AWAY);
        }
        case RTP:{
          // context.set_rounding_mode(z3::rounding_mode::RTP);
          // return auto(Z3_mk_fpa_rtp(getCtx()), getCtx());
          return solver.mkRoundingMode(cvc5::RoundingMode::ROUND_TOWARD_POSITIVE);
        }
        case RTN:{
          // context.set_rounding_mode(z3::rounding_mode::RTN);
          // return auto(Z3_mk_fpa_rtn(getCtx()), getCtx());
          return solver.mkRoundingMode(cvc5::RoundingMode::ROUND_TOWARD_NEGATIVE);
        }
        case RTZ:{
          // context.set_rounding_mode(z3::rounding_mode::RNA);
          // return auto(Z3_mk_fpa_rtz(getCtx()), getCtx());
          return solver.mkRoundingMode(cvc5::RoundingMode::ROUND_TOWARD_ZERO);
        }

          // Floating-point
//    case Fp:{
//      return mkref<Adapter::toCVC5Expr_t>(z3::fpa_fp(
//        *toCVC5Expr(expr->getChild(0)),
//        *toCVC5Expr(expr->getChild(1)),
//        *toCVC5Expr(expr->getChild(2))
//      ));
//    }
//    case FpPlusInfinity:{
//      return mkref<Adapter::toCVC5Expr_t>(context.fpa_inf(
//        *toCVC5Expr(expr->getSort()),
//        false
//      ));
//    }
//    case FpMinusInfinity:{
//      return mkref<Adapter::toCVC5Expr_t>(context.fpa_inf(
//        *toCVC5Expr(expr->getSort()),
//        true
//      ));
//    }
//    case FpPlusZero:{
//      return mkref<Adapter::toCVC5Expr_t>(context.fpa_val(.0f));
//    }
//    case FpMinusZero:{
//      return mkref<Adapter::toCVC5Expr_t>(context.fpa_val(.0f));
//    }
//    case FpNan:{
//      return mkref<Adapter::toCVC5Expr_t>(context.fpa_nan(
//        *toCVC5Expr(expr->getSort())
//      ));
//    }

        case Float:{
          ConstantFloatExprRef constantFloatExprRef = castAs<ConstantFloatExpr>(expr);
          llvm::APFloat value = constantFloatExprRef->value();
          if (expr->bits() == 32){
            float val = value.convertToFloat();
            // return auto(
            //     Z3_mk_fpa_numeral_float(
            //         getCtx(),
            //         val,
            //         Z3_mk_fpa_sort_double(getCtx())),
            //         getCtx());
            return solver.mkFloatingPoint(8, 24, solver.mkBitVector(expr->bits(), val));
          }else if (expr->bits() == 64){
            double val = value.convertToFloat();
            // return auto(
            //     Z3_mk_fpa_numeral_double(
            //         getCtx(),
            //         val,
            //         Z3_mk_fpa_sort_double(getCtx())),
            //     getCtx());
            return solver.mkFloatingPoint(11, 53, solver.mkBitVector(expr->bits(), val));
          }else{
            assert(false && "Only support 32/64 FP Constant !");
          }
        }
        case FAbs:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_fpa_abs(getCtx(), e), getCtx());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_ABS, {e});
        }
        case FNeg:{
          auto e = toCVC5Expr(expr->getFirstChild());
          // return auto(Z3_mk_fpa_neg(getCtx(), e), getCtx());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_NEG, {e});
        }
        case FAdd:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_ADD, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild, secondChild
          });
          // return auto(
          //     Z3_mk_fpa_add(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild,
          //         secondChild),
          //         getCtx());
        }
        case FSub:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_SUB, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild, secondChild
          });
          // return auto(
          //     Z3_mk_fpa_sub(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FMul:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_MULT, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild, secondChild
          });
          // return auto(
          //     Z3_mk_fpa_mul(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FDiv:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_DIV, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild, secondChild
          });
          // return auto(
          //     Z3_mk_fpa_div(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FSqrt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_SQRT, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild
          });
          // return auto(
          //     Z3_mk_fpa_sqrt(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild),
          //     getCtx());
        }
        case FRem:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_REM, {
            solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild
          });
          // return auto(
          //     Z3_mk_fpa_rem(
          //         getCtx(),
          //         getRoundingMode(),
          //         firstChild),
          //     getCtx());
        }
//    case FpRoundToIntegeral:{
//      (void)toCVC5Expr(expr->getChild(0));
//      return mkref<Adapter::toCVC5Expr_t>(z3::round_fpa_to_closest_integer(
//        *toCVC5Expr(expr->getChild(1))
//      ));
//    }
        case FMin:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_MIN, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_min(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FMax:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_MAX, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_max(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FOle:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_LEQ, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_leq(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FOlt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_LT, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_lt(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FOge:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_GEQ, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_geq(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FOgt:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_GT, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_gt(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FOeq:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto secondChild = toCVC5Expr(expr->getSecondChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_EQ, {firstChild, secondChild});
          // return auto(
          //     Z3_mk_fpa_eq(
          //         getCtx(),
          //         firstChild,
          //         secondChild),
          //     getCtx());
        }
        case FPIsNorm:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_NORMAL, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_normal(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsSubNorm:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_SUBNORMAL, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_subnormal(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsZero:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_ZERO, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_zero(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsInf:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_INF, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_infinite(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsNan:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_NAN, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_nan(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsNeg:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_NEG, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_negative(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case FPIsPos:{
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          return solver.mkTerm(cvc5::Kind::FLOATINGPOINT_IS_POS, {firstChild});
          // return auto(
          //     Z3_mk_fpa_is_positive(
          //         getCtx(),
          //         firstChild),
          //     getCtx());
        }
        case BVToFP:{
          UINT32 bits = expr->bits();
          // cvc5::Sort sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
          //                                  : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          // cvc5::Sort sort = (bits == 64) ? solver.mkFloatingPointSort(11, 53) : solver.mkFloatingPoint(8, 24);
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_fpa_to_fp_bv(
          //     getCtx(), firstChild, sort),getCtx());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_IEEE_BV,
              {bits == 64U ? 11U : 8U, bits == 64U ? 53U : 24U}
            ),
            {firstChild}
          );
          return result;
        }
        case FPToFP:{
          UINT32 bits = expr->bits();
          // Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
          //                                  : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_fpa_to_fp_float(
          //     getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_FP,
              {bits == 64U ? 11U : 8U, bits == 64U ? 53U : 24U}
            ),
            {solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild}
          );
          return result;
        }
//    case RealToFp:{
//      throw Unsupported(__PRETTY_FUNCTION__);
//    }
        case SIToFP:{
          UINT32 bits = expr->bits();
          // Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
          //                                  : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_SBV,
              {bits == 64U ? 11U : 8U, bits == 64U ? 53U : 24U}
            ),
            {solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild}
          );
          // auto result = auto(Z3_mk_fpa_to_fp_signed(
          //     getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          return result;
        }
        case UIToFP:{
          UINT32 bits = expr->bits();
          // Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
          //                                  : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_fpa_to_fp_unsigned(
          //     getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_UBV,
              {bits == 64U ? 11U : 8U, bits == 64U ? 53U : 24U}
            ),
            {solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild}
          );
          return result;
        }
        case FPToUI:{
          UINT32 bits = expr->bits();
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_fpa_to_ubv(
          //     getCtx(), getRoundingMode(), firstChild, bits),getCtx());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_UBV, {bits}
            ),
            {solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild}
          );
          return result;
        }
        case FPToSI:{
          UINT32 bits = expr->bits();
          auto firstChild = toCVC5Expr(expr->getFirstChild());
          // auto result = auto(Z3_mk_fpa_to_sbv(
          //     getCtx(), getRoundingMode(), firstChild, bits),getCtx());
          auto result = solver.mkTerm(
            solver.mkOp(
              cvc5::Kind::FLOATINGPOINT_TO_SBV, {bits}
            ),
            {solver.mkRoundingMode(cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN), firstChild}
          );
          return result;
        }
//    case FpToReal:{
//      throw Unsupported(__PRETTY_FUNCTION__);
//    }
        default: throw Unsupported(__PRETTY_FUNCTION__);
      }
      }
      cvc5::Term CVC5Builder::toCVC5Func(ref<FuncExpr> func) {
        if (auto it = funcCache.find(func); it != funcCache.end())
          return it->second;
        std::string name = std::to_string(reinterpret_cast<size_t>(func.get()));
        // z3::symbol name = context.str_symbol(std::to_string(reinterpret_cast<size_t>(func.get())).c_str());
  //      z3::sort_vector domain(context);

        UINT32 argNum = func->num_children();
        // Z3_sort domain[argNum];
        std::vector<cvc5::Sort> domain(argNum);
        for (unsigned idx = 0; idx < argNum; idx ++)
          domain[idx] = toCVC5Sort(func->getChild(idx)->getSort());
        cvc5::Sort range = toCVC5Sort(func->getSort());
        // Z3FuncDeclHandle funcDecl =
        //     Z3FuncDeclHandle(
        //         Z3_mk_func_decl(getCtx(), name, argNum, domain, range), getCtx());
        cvc5::Term funcDecl = solver.mkConst(solver.mkFunctionSort(domain, range));
        funcCache[func] = funcDecl;
        return funcDecl;
      }

    cvc5::Term CVC5Builder::toExpr(ref<Expr> expr) {
      return toCVC5Expr(expr);
    }

    ref<Response> CVC5Builder::toResponse(cvc5::Result res) {
      if (res.isSat()) return Sat();
      if (res.isUnknown()) return Unknown();
      if (res.isUnsat()) return Unsat();
      throw Unsupported(__PRETTY_FUNCTION__);
      // switch (res) {
      //   case cvc5::sat: return Sat();
      //   case cvc5::unknown: return Unknown();
      //   case cvc5::unsat: return Unsat();
      //   default: throw Unsupported(__PRETTY_FUNCTION__);
      // }
    }
  }
}
// #include "Solver/CVC5/CVC5Adapter.h"

// #include "Expr/sort.h"
// #include "Expr/expr.h"

// // #include "IR/Sort.hpp"
// // #include "IR/Op.hpp"
// // #include "IR/Expr.hpp"
// // #include "IR/Function.hpp"
// // #include "IR/Solver.hpp"

// // #include "IR/Logic/ALL.hpp"

// namespace MBSSL {
// namespace CVC5 {

// // using namespace Logic::ALL;

// [[nodiscard]] Adapter::Adapter() noexcept :
//   cons_cache(ConsCache_t()),
//   func_cache(FuncCache_t()),
//   factory(Factory_t()) {}

// [[nodiscard]] ref<Adapter::ToSort_t> Adapter::operator()(ref<Sort> sort) {
//   switch (sort->getSortT()) {
//     case Sort_t::Uninterpreted:{
//       return mkref<Adapter::ToSort_t>(factory.mkUninterpretedSort());
//     } break;
//     case Sort_t::Bool:{
//       return mkref<Adapter::ToSort_t>(factory.getBooleanSort());
//     } break;
//     case Sort_t::Array:{
//       return mkref<Adapter::ToSort_t>(factory.mkArraySort(
//         *(*this)(getDomain(sort)),
//         *(*this)(getRange(sort))
//       ));
//     } break;
//     case Sort_t::BitVec:{
//       return mkref<Adapter::ToSort_t>(factory.mkBitVectorSort(getWidth(sort)));
//     } break;
//     case Sort_t::RoundingMode:{
//       return mkref<Adapter::ToSort_t>(factory.getRoundingModeSort());
//     } break;
//     case Sort_t::Real:{
//       return mkref<Adapter::ToSort_t>(factory.getRealSort());
//     } break;
//     case Sort_t::FloatingPoint:{
//       return mkref<Adapter::ToSort_t>(factory.mkFloatingPointSort(
//         geteb(sort),
//         getsb(sort)
//       ));
//     } break;
//     default: throw Unsupported(__PRETTY_FUNCTION__);
//   }
// }

// [[nodiscard]] ref<Adapter::ToExpr_t> Adapter::operator()(ref<Expr> expr) {
//   // if (expr->isConstant()) {
//   //   // ref<Expr> func = expr->getOp()->getParameter<ref<Function>>(0);
//   //   ref<FuncExpr> funcExpr = castAs<FuncExpr>(expr->funcExpr);
//   //   if (auto it = cons_cache.find(func); it != cons_cache.end())
//   //     return it->second;
//   //   return cons_cache[func] = mkref<ToExpr_t>(factory.mkConst(*(*this)(expr->getSort())));
//   // }
//   // switch (expr->getOp()->getOpT()) {
//   //   case Op_t::Par:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkVar(*(*this)(expr->getSort())));
//   //   } break;
//   //   case Op_t::Forall:{
//   //     std::vector<Adapter::ToExpr_t> parameters;
//   //     for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
//   //       parameters.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FORALL,{
//   //       factory.mkTerm(cvc5::Kind::VARIABLE_LIST, parameters),
//   //       *(*this)(expr->getOperand(0))
//   //       }));
//   //   } break;
//   //   case Op_t::Exists:{
//   //     std::vector<Adapter::ToExpr_t> parameters;
//   //     for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
//   //       parameters.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::EXISTS,{
//   //       factory.mkTerm(cvc5::Kind::VARIABLE_LIST, parameters),
//   //       *(*this)(expr->getOperand(0))
//   //       }));
//   //   } break;
//   //   case Op_t::Application:{
//   //     std::vector<Adapter::ToExpr_t> children;
//   //     children.reserve(expr->getOperandNum() + 1);
//   //     children.push_back(*(*this)(expr->getOp()->getParameter<ref<Function>>(0)));
//   //     for (const auto& each : expr->getOperands())
//   //       children.push_back(*(*this)(each));
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(cvc5::Kind::APPLY_UF, children));
//   //   } break;
//   //   case Op_t::BoolValue:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkBoolean(
//   //       getBoolValue(expr->getOp())
//   //     ));
//   //   } break;
//   //   case Op_t::Not:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::NOT, {
//   //       *(*this)(expr->getOperand(0))
//   //     }));
//   //   } break;
//   //   case Op_t::Implies:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::IMPLIES, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::And:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::AND, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Or:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::OR, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Xor:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::XOR, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Equal:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::EQUAL, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Distinct:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::DISTINCT, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Ite:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::ITE, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::Select:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::SELECT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Store:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::STORE,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::BitVecValue:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkBitVector(
//   //       getWidth(expr->getSort()),
//   //       getBitVecValue(expr->getOp())
//   //     ));
//   //   } break;
//   //   case Op_t::Concat:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_CONCAT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::Extract:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::BITVECTOR_EXTRACT, {
//   //         static_cast<uint32_t>(gethi(expr->getOp())),
//   //         static_cast<uint32_t>(getlo(expr->getOp())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvNot:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_NOT,{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvNeg:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_NEG,{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvAnd:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_AND,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvOr:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_OR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvAdd:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_ADD,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvMul:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_MULT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvUdiv:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_UDIV,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvUrem:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_UREM,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvShl:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SHL,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvLshr:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_LSHR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvUlt:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_ULT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvNand:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_NAND,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvNor:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_NOR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvXor:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_XOR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvXnor:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_XNOR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvComp:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_COMP,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSub:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SUB,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSdiv:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SDIV,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSrem:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SREM,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSmod:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SMOD,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvAshr:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_ASHR,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::Repeat:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       factory.mkOp(cvc5::Kind::BITVECTOR_REPEAT, {
//   //         static_cast<uint32_t>(expr->getOp()->getParameter<uint64_t>(0))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::ZeroExtend:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       factory.mkOp(cvc5::Kind::BITVECTOR_ZERO_EXTEND, {
//   //         static_cast<uint32_t>(expr->getOp()->getParameter<uint64_t>(0))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::SignExtend:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       factory.mkOp(cvc5::Kind::BITVECTOR_SIGN_EXTEND, {
//   //         static_cast<uint32_t>(expr->getOp()->getParameter<uint64_t>(0))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::RotateLeft:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       factory.mkOp(cvc5::Kind::BITVECTOR_ROTATE_LEFT, {
//   //         static_cast<uint32_t>(expr->getOp()->getParameter<uint64_t>(0))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::RotateRight:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       factory.mkOp(cvc5::Kind::BITVECTOR_ROTATE_RIGHT, {
//   //         static_cast<uint32_t>(expr->getOp()->getParameter<uint64_t>(0))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvUle:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_ULE,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvUgt:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_UGT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvUge:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_UGE,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSlt:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SLT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSle:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SLE,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSgt:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SGT,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::BvSge:{
//   //     return mkref<cvc5::Term>(factory.mkTerm(
//   //       cvc5::Kind::BITVECTOR_SGE,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1))
//   //     }));
//   //   } break;
//   //   case Op_t::RoundingModeValue:{
//   //     switch (getRoundingModeValue(expr->getOp())){
//   //       case RoundingMode_t::RoundNearestTiesToEven:{
//   //         return mkref<Adapter::ToExpr_t>(factory.mkRoundingMode(
//   //           cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_EVEN
//   //         ));
//   //       } break;
//   //       case RoundingMode_t::RoundNearestTiesToAway:{
//   //         return mkref<Adapter::ToExpr_t>(factory.mkRoundingMode(
//   //           cvc5::RoundingMode::ROUND_NEAREST_TIES_TO_AWAY
//   //         ));
//   //       } break;
//   //       case RoundingMode_t::RoundTowardPositive:{
//   //         return mkref<Adapter::ToExpr_t>(factory.mkRoundingMode(
//   //           cvc5::RoundingMode::ROUND_TOWARD_POSITIVE
//   //         ));
//   //       } break;
//   //       case RoundingMode_t::RoundTowardNegative:{
//   //         return mkref<Adapter::ToExpr_t>(factory.mkRoundingMode(
//   //           cvc5::RoundingMode::ROUND_TOWARD_NEGATIVE
//   //         ));
//   //       } break;
//   //       case RoundingMode_t::RoundTowardZero:{
//   //         return mkref<Adapter::ToExpr_t>(factory.mkRoundingMode(
//   //           cvc5::RoundingMode::ROUND_TOWARD_ZERO
//   //         ));
//   //       } break;
//   //     }
//   //   } break;
//   //   case Op_t::Fp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_FP, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpPlusInfinity:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkFloatingPointPosInf(
//   //       geteb(expr->getSort()),
//   //       getsb(expr->getSort())
//   //     ));
//   //   } break;
//   //   case Op_t::FpMinusInfinity:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkFloatingPointNegInf(
//   //       geteb(expr->getSort()),
//   //       getsb(expr->getSort())
//   //     ));
//   //   } break;
//   //   case Op_t::FpPlusZero:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkFloatingPointPosZero(
//   //       geteb(expr->getSort()),
//   //       getsb(expr->getSort())
//   //     ));
//   //   } break;
//   //   case Op_t::FpMinusZero:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkFloatingPointNegZero(
//   //       geteb(expr->getSort()),
//   //       getsb(expr->getSort())
//   //     ));
//   //   } break;
//   //   case Op_t::FpNan:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkFloatingPointNaN(
//   //       geteb(expr->getSort()),
//   //       getsb(expr->getSort())
//   //     ));
//   //   } break;
//   //   case Op_t::FpAbs:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_ABS, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpNeg:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_NEG, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpAdd:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_ADD, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpSub:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_SUB, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpMul:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_MULT, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpDiv:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_DIV, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpFma:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_FMA, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //       *(*this)(expr->getOperand(2)),
//   //       *(*this)(expr->getOperand(3)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpSqrt:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_SQRT, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpRem:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_REM, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpRoundToIntegeral:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_RTI, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpMin:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_MIN, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpMax:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_MAX, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpLeq:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_LEQ, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpLt:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_LT, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpGeq:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_GEQ, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpGt:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_GT, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpEq:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_EQ, {
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsNormal:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_NORMAL, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsSubnormal:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_SUBNORMAL, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsZero:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_ZERO, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsInfinite:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_INF, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsNaN:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_NAN, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsNegative:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_NEG, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpIsPositive:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_IS_POS, {
//   //       *(*this)(expr->getOperand(0)),
//   //     }));
//   //   } break;
//   //   case Op_t::BvToFp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_IEEE_BV,{
//   //         static_cast<uint32_t>(geteb(expr->getSort())),
//   //         static_cast<uint32_t>(getsb(expr->getSort())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0))
//   //     }));
//   //   } break;
//   //   case Op_t::FpToFp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_FP,{
//   //         static_cast<uint32_t>(geteb(expr->getSort())),
//   //         static_cast<uint32_t>(getsb(expr->getSort())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::RealToFp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_REAL,{
//   //         static_cast<uint32_t>(geteb(expr->getSort())),
//   //         static_cast<uint32_t>(getsb(expr->getSort())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::SignedToFp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_SBV,{
//   //         static_cast<uint32_t>(geteb(expr->getSort())),
//   //         static_cast<uint32_t>(getsb(expr->getSort())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::UnsignedToFp:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_FP_FROM_UBV,{
//   //         static_cast<uint32_t>(geteb(expr->getSort())),
//   //         static_cast<uint32_t>(getsb(expr->getSort())),
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpToUbv:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_UBV,{
//   //         static_cast<uint32_t>(getWidth(expr->getSort()))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpToSbv:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       factory.mkOp(
//   //         cvc5::Kind::FLOATINGPOINT_TO_SBV,{
//   //         static_cast<uint32_t>(getWidth(expr->getSort()))
//   //       }),{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   case Op_t::FpToReal:{
//   //     return mkref<Adapter::ToExpr_t>(factory.mkTerm(
//   //       cvc5::Kind::FLOATINGPOINT_TO_REAL,{
//   //       *(*this)(expr->getOperand(0)),
//   //       *(*this)(expr->getOperand(1)),
//   //     }));
//   //   } break;
//   //   default: throw Unsupported(__PRETTY_FUNCTION__);
//   // }
// }

// [[nodiscard]] ref<Adapter::ToFunc_t> Adapter::toCVC5Func(ref<Expr> func) {
//   if (auto it = func_cache.find(func); it != func_cache.end())
//     return it->second;
//   std::vector<Adapter::ToSort_t> sorts;
//   sorts.reserve(func->getOperandNum());
//   for (const auto& each : func->getOperands())
//     sorts.push_back(*(*this)(each->getSort()));
//   Adapter::ToSort_t codomain = *(*this)(func->getSort());
//   return func_cache[func] = mkref<Adapter::ToFunc_t>(factory.mkConst(factory.mkFunctionSort(sorts, codomain)));
// }

// [[nodiscard]] ref<Response> Adapter::operator()(ref<Adapter::FromResponse_t> res) {
//   if (res->isSat()) return Sat();
//   if (res->isUnknown()) return Unknown();
//   if (res->isUnsat()) return Unsat();
//   throw Unsupported(__PRETTY_FUNCTION__);
// }

// }
// }
