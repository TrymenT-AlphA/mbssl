#include "Solver/CVC5/CVC5Engine.h"
#include <regex>

namespace MBSSL {
  namespace CVC5 {

    Engine::Engine() noexcept : adapter() {}

    Engine::ConsVar_t& Engine::ConsVar() noexcept {
      return cons_var;
    }


    void Engine::Assert(ref<Expr> expr) noexcept {
      adapter.solver.assertFormula(adapter.toExpr(expr));
    }

    ref<Response> Engine::CheckSat() noexcept {
      return adapter.toResponse(adapter.solver.checkSat());
    }

    void Engine::ResetAssertions() noexcept {
      adapter.solver.resetAssertions();
    }

    ref<Response> Engine::Prove(ref<Expr> expr) const noexcept {
      Adapter_t t_adapter;
      t_adapter.solver.assertFormula(t_adapter.toExpr(expr));
      return t_adapter.toResponse(t_adapter.solver.checkSat());
    }

    std::string Engine::getModel() noexcept {
      std::vector<cvc5::Term> consts;
      for (auto [first, second] : cons_var) {
        auto x = adapter.toExpr(second);
        if (x.getKind() == cvc5::Kind::CONSTANT) {
          consts.push_back(x);
        }
      }
      return adapter.solver.getModel({}, consts);
    }

    pair<string, unsigned> extractSymbolName(string symbolName) {
      size_t pos = symbolName.rfind('_');
      string varName = symbolName.substr(0, pos);
      unsigned offset = symbolName[pos+1] - '0';
      return make_pair(varName, offset);
    }

    void Engine::getAssignFromModel() noexcept {
      Assign_t assignment;
      Model_t model = getModel();
      set<unsigned> symArrayIndices;

      vector<string> raw_assignments;
      
      regex e("\\(define-fun \\b([0-9a-zA-Z\\~\\!\\@\\$\\%\\^\\&\\*\\_\\-\\+\\=\\<\\>\\.\\?\\/]+)\\b \\(\\) (.+) #b([01]+)\\)");
      unsigned idx = 0;
      for (sregex_iterator it(model.cbegin(), model.cend(), e), itend; it != itend; it++, idx++) {
        std::string name = it->str(1);
        std::string sort = it->str(2);
        const std::string arrayPrefix = "(Array";
        const std::string bvPrefix = "(_ BitVec";
        if (sort.length() >= arrayPrefix.length() && std::equal(arrayPrefix.begin(), arrayPrefix.end(), sort.begin())) {
          symArrayIndices.insert(idx);
          continue;
        }

        assert(sort.length() >= bvPrefix.length()
          && std::equal(bvPrefix.begin(), bvPrefix.end(), sort.begin()));
        UINT8 value = std::stoi(it->str(3), nullptr, 2);

        pair<string, unsigned> tmp = extractSymbolName(name);
        if (assignment.count(tmp.first) == 0) {
          assignment[tmp.first] = vector<UINT8>(tmp.second + 1);
          assignment[tmp.first][tmp.second] = value;
        } else {
          if (assignment[tmp.first].size() < tmp.second + 1)
            assignment[tmp.first].resize(tmp.second + 1);
          assignment[tmp.first][tmp.second] = value;
        }
      }

      for (const auto& assign : assignment){
        std::cout << "Var : " << assign.first << "\n";
        for (auto val : assign.second)
          std::cout << std::hex << std::setfill('0') << std::setw(2) << (int) val ;
        std::cout << "\n";
      }
    }
  }
}
