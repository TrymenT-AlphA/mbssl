#include "Solver/Solver.h"
#include "Expr/expr.h"

namespace MBSSL {

[[nodiscard]] Solver::Solver(
  ref<EngineInterface> engine
) noexcept :
  engine(engine) {
  assertion_stack = mkref<AssertionStack_t>();
  assertion_stack->push(mkref<AssertionLevel_t>());
  current_assertion_level = assertion_stack->top();
}

void Solver::Assert(ref<Expr> expr) noexcept {
  current_assertion_level->push_back(expr);
}

void Solver::Push() noexcept {
  assertion_stack->push(mkref<AssertionLevel_t>());
  current_assertion_level = assertion_stack->top();
}

void Solver::Pop() noexcept {
  assertion_stack->pop();
  if (assertion_stack->empty()) assertion_stack->push(mkref<AssertionLevel_t>());
  current_assertion_level = assertion_stack->top();
}

void Solver::ResetAssertions() noexcept {
  while(assertion_stack->size()) assertion_stack->pop();
  assertion_stack->push(mkref<AssertionLevel_t>());
  current_assertion_level = assertion_stack->top();
}

[[nodiscard]] ref<Response> Solver::CheckSat() const noexcept {
  ref<Expr> expr = std::make_shared<BoolExpr>(true);
  for (const auto& each : *current_assertion_level)
    expr = std::make_shared<AndExpr>(expr, each);
  return engine->Prove(expr);
}

}
