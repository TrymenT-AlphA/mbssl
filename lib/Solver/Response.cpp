#include "Solver/Response.h"

#include "Support/Ref.h"
#include "Support/Error.h"

namespace MBSSL {

[[nodiscard]] Response::Response(
  Response_t response_t
) noexcept :
  response_t(response_t) {}

[[nodiscard]] Response_t Response::getResponseT() const noexcept {
  return response_t;
}

[[nodiscard]] bool Response::isSat() const noexcept {
  return response_t == Response_t::Sat;
}

[[nodiscard]] bool Response::isUnsat() const noexcept {
  return response_t == Response_t::Unsat;
}

[[nodiscard]] bool Response::isUnknown() const noexcept {
  return response_t == Response_t::Unknown;
}

std::ostream& operator<<(std::ostream& os, const Response& res) noexcept {
  switch (res.getResponseT()) {
    case Response_t::Sat: {
      return os << "sat";
    } break;
    case Response_t::Unsat: {
      return os << "unsat";
    } break;
    case Response_t::Unknown: {
      return os << "unknown";
    } break;
    default: {
      throw Unreachable(__PRETTY_FUNCTION__);
    } break;
  }
}

[[nodiscard]] ref<Response> Sat() noexcept {
  static ref<Response> sat = mkref<Response>(Response_t::Sat);
  return sat;
}

[[nodiscard]] ref<Response> Unsat() noexcept {
  static ref<Response> unsat = mkref<Response>(Response_t::Unsat);
  return unsat;
}

[[nodiscard]] ref<Response> Unknown() noexcept {
  static ref<Response> unknown = mkref<Response>(Response_t::Unknown);
  return unknown;
}

}

