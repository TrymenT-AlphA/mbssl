#include "Solver/Z3/Z3Adapter.h"
#include "Expr/expr.h"

using namespace qsym;

namespace MBSSL {
  namespace Z3 {
    Z3ASTHandle Z3Builder::getRoundingMode() {
      return Z3ASTHandle(context.fpa_rounding_mode(), getCtx());
    }

    Z3SortHandle Z3Builder::getBvSort(unsigned width) {
      return Z3SortHandle(Z3_mk_bv_sort(getCtx(), width), getCtx());
    }

    Z3ASTHandle Z3Builder::varExpr(const std::string& name, Z3SortHandle sort){
      Z3_symbol nameSym = Z3_mk_string_symbol(getCtx(), name.c_str());
      return Z3ASTHandle(Z3_mk_const(getCtx(), nameSym, sort), getCtx());
    }

    Z3ASTHandle Z3Builder::getTrue() {
      return Z3ASTHandle(Z3_mk_true(getCtx()), getCtx());
    }

    Z3ASTHandle Z3Builder::getFalse() {
      return Z3ASTHandle(Z3_mk_false(getCtx()), getCtx());
    }

    z3::expr Z3Builder::toExpr(ref<Expr> expr){
      Z3ASTHandle e = toZ3Expr(expr);
      return z3::expr(context, e);
    }

    Z3SortHandle Z3Builder::toZ3Sort(ref<Sort> sort) {
      switch (sort->getSortT()) {
        case Sort_t::Uninterpreted:{
          Z3_symbol nameSym = Z3_mk_string_symbol(
              getCtx(), std::to_string(reinterpret_cast<size_t>(sort.get())).c_str());
          return Z3SortHandle(Z3_mk_uninterpreted_sort(getCtx(), nameSym), getCtx());
        }
        case Sort_t::Bool:{
          return Z3SortHandle(Z3_mk_bool_sort(getCtx()), getCtx());
        }
        case Sort_t::Array:{
          Z3SortHandle domainSort = toZ3Sort(getDomain(sort));
          Z3SortHandle rangeSort = toZ3Sort(getRange(sort));
          return Z3SortHandle(Z3_mk_array_sort(getCtx(), domainSort, rangeSort), getCtx());
        }
        case Sort_t::BitVec:{
          return Z3SortHandle(Z3_mk_bv_sort(getCtx(), getWidth(sort)), getCtx());
        }
        case Sort_t::RoundingMode:{
          return Z3SortHandle(Z3_mk_fpa_rounding_mode_sort(getCtx()), getCtx());
        }
//    case Sort_t::Real:{
//      return Z3SortHandle(context.real_sort());
//    }
        case Sort_t::FloatingPoint:{
          return Z3SortHandle(
              Z3_mk_fpa_sort(getCtx(),geteb(sort),getsb(sort)), getCtx());
        }
        default:{
          throw Unsupported(__PRETTY_FUNCTION__);
        }
      }
    }

    Z3ASTHandle Z3Builder::toZ3Expr(ExprRef e) {
      // TODO: We could potentially use Z3_simplify() here
      // to store simpler expressions.
      if (e->isConstant()) {
        return toZ3ExprActual(e);
      } else {
        ExprHashMap<Z3ASTHandle>::iterator it = consCache.find(e);
        if (it != consCache.end()) {
          return it->second;
        } else {
          Z3ASTHandle res = toZ3ExprActual(e);
          consCache.insert(std::make_pair(e,res));
          return res;
        }
      }
    }

    Z3ASTHandle Z3Builder::toZ3ExprActual(ExprRef expr) {
      if (auto appExpr = castAs<AppExpr>(expr)) {
        if (appExpr->num_children() == 0){
          ref<Expr> func = appExpr->funcExpr;
          if (auto it = consCache.find(func); it != consCache.end())
            return it->second;
          Z3_symbol nameSym = Z3_mk_string_symbol(
              getCtx(), std::to_string(reinterpret_cast<size_t>(expr.get())).c_str());
          Z3SortHandle sort = toZ3Sort(expr->getSort());
          Z3ASTHandle variable = Z3ASTHandle(Z3_mk_const(getCtx(), nameSym, sort), getCtx());
          consCache[func] = variable;
          return variable;
        }
      }

      switch (expr->kind()) {
        //    case Forall:{
//      z3::expr_vector xs(context);
//      for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
//        xs.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
//      return mkref<Adapter::toZ3Expr_t>(z3::forall(
//        xs,
//        *(*this)(expr->getChild(0))
//      ));
//    }
//    case Exists:{
//      z3::expr_vector xs(context);
//      for (size_t idx = 0; idx < expr->getOp()->getParameterNum(); idx++)
//        xs.push_back(*(*this)(expr->getOp()->getParameter<ref<Expr>>(idx)));
//      return mkref<Adapter::toZ3Expr_t>(z3::forall(
//        xs,
//        *(*this)(expr->getChild(0))
//      ));
//    }
        case Para:{
          Z3_symbol nameSym = Z3_mk_string_symbol(
              getCtx(), std::to_string(reinterpret_cast<size_t>(expr.get())).c_str());
          Z3SortHandle sort = toZ3Sort(expr->getSort());
          Z3ASTHandle variable = Z3ASTHandle(Z3_mk_const(getCtx(), nameSym, sort), getCtx());
          return variable;
        }
        case App:{
          ref<AppExpr> appExpr = castAs<AppExpr>(expr);
          ref<FuncExpr> funcExpr = castAs<FuncExpr>(appExpr->funcExpr);
          Z3FuncDeclHandle f = toZ3Func(funcExpr);
          unsigned argNum = expr->num_children();
          Z3_ast args[argNum];
          for (unsigned idx = 0; idx < argNum; idx ++){
            args[idx] = toZ3Expr(expr->getChild(idx));
          }
          return Z3ASTHandle(Z3_mk_app(getCtx(), f, argNum, args), getCtx());
        }
        case Read:{
          ref<ReadExpr> readExprRef = castAs<ReadExpr>(expr);
          std::string name = readExprRef->name();
          UINT32 index = readExprRef->index();
          std::string symbol_name = name + "_" + std::to_string(index);
          Z3SortHandle sort = getBvSort(8);
          return varExpr(symbol_name, sort);
        }

          // Common
        case Bool:{
          ref<BoolExpr> boolExpr = castAs<BoolExpr>(expr);
          bool value = boolExpr->value();
          Z3ASTHandle result = value ? getTrue() : getFalse();
          return result;
        }
        case Constant:{
          ConstantExprRef constantExprRef = castAs<ConstantExpr>(expr);
          llvm::APInt value = constantExprRef->value();
          UINT32 bits = constantExprRef->bits();
          if (bits == 32 || bits == 64){
            // fix by chk
            uint64_t v = value.getZExtValue();
            return Z3ASTHandle(
                Z3_mk_unsigned_int64(getCtx(), v, getBvSort(bits)), getCtx());
          }else {
            std::string val_str = value.toString(10, false);
            return Z3ASTHandle(
                Z3_mk_numeral(getCtx(), val_str.c_str(), getBvSort(bits)), getCtx());
          }
        }
        case Concat:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_concat(
                  getCtx(),
                  firstChild,
                  secondChild) ,
                  getCtx());
        }
        case Extract:{
          ref<ExtractExpr> extractExprRef = castAs<ExtractExpr>(expr);
          UINT32 index = extractExprRef->index();
          UINT32 bits  = extractExprRef->bits();
          Z3ASTHandle child = toZ3Expr(expr->getChild(0));
          return Z3ASTHandle(Z3_mk_extract(getCtx(), index + bits - 1, index, child), getCtx());
        }
        case Ite:{
          Z3ASTHandle cond = toZ3Expr(expr->getChild(0));
          Z3ASTHandle trueVal = toZ3Expr(expr->getChild(1));
          Z3ASTHandle falseVal = toZ3Expr(expr->getChild(2));
          return Z3ASTHandle(Z3_mk_ite(getCtx(), cond, trueVal, falseVal), getCtx());
        }

        // Array
        case Select:{
          Z3ASTHandle _z3_array = toZ3Expr(expr->getFirstChild()); //is already array, not bv sort !
          Z3ASTHandle _z3_index = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(Z3_mk_select(getCtx(), _z3_array, _z3_index), getCtx());
        }
        case Store:{
          Z3ASTHandle _z3_array = toZ3Expr(expr->getChild(0)); //is already array, not bv sort !
          Z3ASTHandle _z3_index = toZ3Expr(expr->getChild(1));
          Z3ASTHandle _z3_newV = toZ3Expr(expr->getChild(2));
          return Z3ASTHandle(Z3_mk_store(getCtx(), _z3_array, _z3_index, _z3_newV), getCtx());
        }

          // Logic
        case LNot:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_not(getCtx(), firstChild),getCtx());
          return result;
        }
        case LAnd:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3_ast args[2] = {firstChild, secondChild};
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_and(getCtx(), 2, args),getCtx());
          return result;
        }
        case LOr:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3_ast args[2] = {firstChild, secondChild};
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_or(getCtx(), 2, args),getCtx());
          return result;
        }

        // Bits
        case Not:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_not(getCtx(), e),getCtx());
        }
        case Neg:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_bvneg(getCtx(), e),getCtx());
        }
        case And:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvand(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Or:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvor(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Xor:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvxor(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Shl:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvshl(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case LShr:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvlshr(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case AShr:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvashr(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Rol:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_rotate_left(getCtx(), expr->bits(), e),getCtx());
        }
        case Ror:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_rotate_right(getCtx(), expr->bits(), e),getCtx());
        }
        case ZExt:{
          UINT32 bits = expr->bits();
          UINT32 childBits = expr->getChild(0)->bits();
          UINT32 extBits = bits - childBits;
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_zero_ext(getCtx(), extBits, e),getCtx());
        }
        case SExt:{
          UINT32 bits = expr->bits();
          UINT32 childBits = expr->getChild(0)->bits();
          UINT32 extBits = bits - childBits;
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_sign_ext(getCtx(), extBits, e),getCtx());
        }

        // Bv Arithmetic
        case Add:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvadd(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Sub:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsub(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Mul:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvmul(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case UDiv:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvudiv(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case SDiv:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsdiv(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case URem:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvurem(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case SRem:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsrem(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case SMod:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsmod(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }

        // Bv Compare
        case Equal:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_eq(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Distinct:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle eqExpr = Z3ASTHandle(Z3_mk_eq(getCtx(), firstChild, secondChild),getCtx());
          Z3ASTHandle neqExpr = Z3ASTHandle(Z3_mk_not(getCtx(), eqExpr),getCtx());
          return neqExpr;
        }
        case Ult:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvult(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Ule:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvule(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Ugt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvugt(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Uge:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvuge(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Slt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvslt(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Sle:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsle(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Sgt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsgt(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }
        case Sge:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_bvsge(getCtx(), firstChild, secondChild),getCtx());
          return result;
        }

          // Rounding Mode
        case RNE:{
          context.set_rounding_mode(z3::rounding_mode::RNE);
          return Z3ASTHandle(Z3_mk_fpa_rne(getCtx()), getCtx());
        }
        case RNA:{
          context.set_rounding_mode(z3::rounding_mode::RNA);
          return Z3ASTHandle(Z3_mk_fpa_rna(getCtx()), getCtx());
        }
        case RTP:{
          context.set_rounding_mode(z3::rounding_mode::RTP);
          return Z3ASTHandle(Z3_mk_fpa_rtp(getCtx()), getCtx());
        }
        case RTN:{
          context.set_rounding_mode(z3::rounding_mode::RTN);
          return Z3ASTHandle(Z3_mk_fpa_rtn(getCtx()), getCtx());
        }
        case RTZ:{
          context.set_rounding_mode(z3::rounding_mode::RNA);
          return Z3ASTHandle(Z3_mk_fpa_rtz(getCtx()), getCtx());
        }

          // Floating-point
//    case Fp:{
//      return mkref<Adapter::toZ3Expr_t>(z3::fpa_fp(
//        *toZ3Expr(expr->getChild(0)),
//        *toZ3Expr(expr->getChild(1)),
//        *toZ3Expr(expr->getChild(2))
//      ));
//    }
//    case FpPlusInfinity:{
//      return mkref<Adapter::toZ3Expr_t>(context.fpa_inf(
//        *toZ3Expr(expr->getSort()),
//        false
//      ));
//    }
//    case FpMinusInfinity:{
//      return mkref<Adapter::toZ3Expr_t>(context.fpa_inf(
//        *toZ3Expr(expr->getSort()),
//        true
//      ));
//    }
//    case FpPlusZero:{
//      return mkref<Adapter::toZ3Expr_t>(context.fpa_val(.0f));
//    }
//    case FpMinusZero:{
//      return mkref<Adapter::toZ3Expr_t>(context.fpa_val(.0f));
//    }
//    case FpNan:{
//      return mkref<Adapter::toZ3Expr_t>(context.fpa_nan(
//        *toZ3Expr(expr->getSort())
//      ));
//    }

        case Float:{
          ConstantFloatExprRef constantFloatExprRef = castAs<ConstantFloatExpr>(expr);
          llvm::APFloat value = constantFloatExprRef->value();
          if (expr->bits() == 32){
            float val = value.convertToFloat();
            return Z3ASTHandle(
                Z3_mk_fpa_numeral_float(
                    getCtx(),
                    val,
                    Z3_mk_fpa_sort_double(getCtx())),
                    getCtx());
          }else if (expr->bits() == 64){
            double val = value.convertToFloat();
            return Z3ASTHandle(
                Z3_mk_fpa_numeral_double(
                    getCtx(),
                    val,
                    Z3_mk_fpa_sort_double(getCtx())),
                getCtx());
          }else{
            assert(false && "Only support 32/64 FP Constant !");
          }
        }
        case FAbs:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_fpa_abs(getCtx(), e), getCtx());
        }
        case FNeg:{
          Z3ASTHandle e = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(Z3_mk_fpa_neg(getCtx(), e), getCtx());
        }
        case FAdd:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_add(
                  getCtx(),
                  getRoundingMode(),
                  firstChild,
                  secondChild),
                  getCtx());
        }
        case FSub:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_sub(
                  getCtx(),
                  getRoundingMode(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FMul:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_mul(
                  getCtx(),
                  getRoundingMode(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FDiv:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_div(
                  getCtx(),
                  getRoundingMode(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FSqrt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_sqrt(
                  getCtx(),
                  getRoundingMode(),
                  firstChild),
              getCtx());
        }
        case FRem:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_rem(
                  getCtx(),
                  getRoundingMode(),
                  firstChild),
              getCtx());
        }
//    case FpRoundToIntegeral:{
//      (void)toZ3Expr(expr->getChild(0));
//      return mkref<Adapter::toZ3Expr_t>(z3::round_fpa_to_closest_integer(
//        *toZ3Expr(expr->getChild(1))
//      ));
//    }
        case FMin:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_min(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FMax:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_max(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FOle:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_leq(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FOlt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_lt(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FOge:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_geq(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FOgt:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_gt(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FOeq:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle secondChild = toZ3Expr(expr->getSecondChild());
          return Z3ASTHandle(
              Z3_mk_fpa_eq(
                  getCtx(),
                  firstChild,
                  secondChild),
              getCtx());
        }
        case FPIsNorm:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_normal(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsSubNorm:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_subnormal(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsZero:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_zero(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsInf:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_infinite(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsNan:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_nan(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsNeg:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_negative(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case FPIsPos:{
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          return Z3ASTHandle(
              Z3_mk_fpa_is_positive(
                  getCtx(),
                  firstChild),
              getCtx());
        }
        case BVToFP:{
          UINT32 bits = expr->bits();
          Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
                                           : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_fp_bv(
              getCtx(), firstChild, sort),getCtx());
          return result;
        }
        case FPToFP:{
          UINT32 bits = expr->bits();
          Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
                                           : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_fp_float(
              getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          return result;
        }
//    case RealToFp:{
//      throw Unsupported(__PRETTY_FUNCTION__);
//    }
        case SIToFP:{
          UINT32 bits = expr->bits();
          Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
                                           : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_fp_signed(
              getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          return result;
        }
        case UIToFP:{
          UINT32 bits = expr->bits();
          Z3SortHandle sort = (bits == 64) ? Z3SortHandle(Z3_mk_fpa_sort_double(getCtx()),getCtx())
                                           : Z3SortHandle(Z3_mk_fpa_sort_single(getCtx()),getCtx());
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_fp_unsigned(
              getCtx(), getRoundingMode(), firstChild, sort),getCtx());
          return result;
        }
        case FPToUI:{
          UINT32 bits = expr->bits();
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_ubv(
              getCtx(), getRoundingMode(), firstChild, bits),getCtx());
          return result;
        }
        case FPToSI:{
          UINT32 bits = expr->bits();
          Z3ASTHandle firstChild = toZ3Expr(expr->getFirstChild());
          Z3ASTHandle result = Z3ASTHandle(Z3_mk_fpa_to_sbv(
              getCtx(), getRoundingMode(), firstChild, bits),getCtx());
          return result;
        }
//    case FpToReal:{
//      throw Unsupported(__PRETTY_FUNCTION__);
//    }
        default: throw Unsupported(__PRETTY_FUNCTION__);
      }
    }

    Z3FuncDeclHandle Z3Builder::toZ3Func(ref<FuncExpr> func) {
      if (auto it = funcCache.find(func); it != funcCache.end())
        return it->second;
      z3::symbol name = context.str_symbol(std::to_string(reinterpret_cast<size_t>(func.get())).c_str());
//      z3::sort_vector domain(context);

      UINT32 argNum = func->num_children();
      Z3_sort domain[argNum];
      for (unsigned idx = 0; idx < argNum; idx ++)
        domain[idx] = toZ3Sort(func->getChild(idx)->getSort());
      Z3SortHandle range = toZ3Sort(func->getSort());
      Z3FuncDeclHandle funcDecl =
          Z3FuncDeclHandle(
              Z3_mk_func_decl(getCtx(), name, argNum, domain, range), getCtx());
      funcCache[func] = funcDecl;
      return funcDecl;
    }

    ref<Response> Z3Builder::toResponse(z3::check_result res) {
      switch (res) {
        case z3::sat: return Sat();
        case z3::unknown: return Unknown();
        case z3::unsat: return Unsat();
        default: throw Unsupported(__PRETTY_FUNCTION__);
      }
    }

  }
}
