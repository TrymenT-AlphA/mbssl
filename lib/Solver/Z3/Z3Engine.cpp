#include "Solver/Z3/Z3Engine.h"
#include "Expr/expr.h"
using namespace qsym;

namespace MBSSL {
  namespace Z3 {

    Engine::Engine() noexcept : adapter(), solver(adapter.context) {}

    Engine::ConsVar_t& Engine::ConsVar() noexcept {
      return cons_var;
    }

    void Engine::Assert(ref<Expr> expr) noexcept {
      solver.add(adapter.toExpr(expr));
    }

    ref<Response> Engine::CheckSat() noexcept {
      return adapter.toResponse(solver.check());
    }

    void Engine::ResetAssertions() noexcept {
      solver.reset();
    }

    ref<Response> Engine::Prove(ref<Expr> expr) const noexcept {
      Adapter_t t_adapter;
      Solver_t t_solver(t_adapter.context);
      t_solver.add(t_adapter.toExpr(expr));
      return t_adapter.toResponse(t_solver.check());
    }

    z3::model Engine::getModel() noexcept {
      return solver.get_model();
    }

    pair<string, unsigned> extractSymbolName(string symbolName) {
      size_t pos = symbolName.rfind('_');
      string varName = symbolName.substr(0, pos);
      unsigned offset = symbolName[pos + 1] - '0';
      return make_pair(varName, offset);
    }

    void Engine::getAssignFromModel() noexcept {
      Assign_t assignment;
      z3::model m = getModel();
      set<unsigned> symArrayIndices;

      for (unsigned i = 0; i < m.num_consts(); i++) {
        z3::func_decl decl = m.get_const_decl(i);

        /// FIXME: Ugly code, a general method is needed.
        if (decl.decl_kind() == Z3_OP_FPA_RM_NEAREST_TIES_TO_EVEN ||
            decl.decl_kind() == Z3_OP_FPA_RM_NEAREST_TIES_TO_AWAY ||
            decl.decl_kind() == Z3_OP_FPA_RM_TOWARD_POSITIVE ||
            decl.decl_kind() == Z3_OP_FPA_RM_TOWARD_NEGATIVE ||
            decl.decl_kind() == Z3_OP_FPA_RM_TOWARD_ZERO ||
            decl.decl_kind() == Z3_OP_FPA_NUM ||
            decl.decl_kind() == Z3_OP_FPA_PLUS_INF ||
            decl.decl_kind() == Z3_OP_FPA_MINUS_INF ||
            decl.decl_kind() == Z3_OP_FPA_NAN ||
            decl.decl_kind() == Z3_OP_FPA_PLUS_ZERO ||
            decl.decl_kind() == Z3_OP_FPA_MINUS_ZERO)
          continue;

        string name = decl.name().str();
        z3::sort decl_range = decl.range();
        if (decl_range.sort_kind() == Z3_ARRAY_SORT) {
          symArrayIndices.insert(i);
          continue;
        }

        assert(decl_range.sort_kind() == Z3_BV_SORT);
        z3::expr e = m.get_const_interp(decl);
        UINT8 value = e.get_numeral_uint();

        pair<string, unsigned> tmp = extractSymbolName(name);
        if (assignment.count(tmp.first) == 0) {
          assignment[tmp.first] = vector<UINT8>(tmp.second + 1);
          assignment[tmp.first][tmp.second] = value;
        } else {
          if (assignment[tmp.first].size() < tmp.second + 1)
            assignment[tmp.first].resize(tmp.second + 1);
          assignment[tmp.first][tmp.second] = value;
        }
      }

      for (const auto& assign : assignment){
        std::cout << "Var : " << assign.first << "\n";
        for (auto val : assign.second)
          std::cout << std::hex << std::setfill('0') << std::setw(2) << (int) val ;
        std::cout << "\n";
      }
    }

  }
}
