#include "Expr/ExprBuilder.h"
#include "Expr/expr.h"

namespace qsym {
  // Use ConstantFoldingExprBuilder for evaluation
  static ExprBuilder* CEB = ConstantFoldingExprBuilder::create();

  ExprRef checkExpr(ExprRef e) {
    Kind kind = e->kind();
    QSYM_ASSERT(kind == Constant || kind == Bool);
    return e;
  }

  ExprRef Expr::evaluate() {
    if (evaluation_ == NULL)
      evaluation_ = evaluateImpl();
    return evaluation_;
  }

  ExprRef ConcatExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    ExprRef c1 = getChild(1)->evaluate();
    return checkExpr(CEB->createConcat(c0, c1));
  }

  ExprRef UnaryExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createUnaryExpr(kind_, c0));
  }

  ExprRef ReadExpr::evaluateImpl() {
    //return std::make_shared<ConstantExpr>(g_solver->getInput(index_), 8);
    // add by zgf : if expr is seperated with solver, this ReadExpr will not
    //              get correct actual solution from solver, so return itself.
    return std::make_shared<ReadExpr>(this->index(),this->name());
  }

  ExprRef RNEExpr::evaluateImpl() { return std::make_shared<RNEExpr>();}
  ExprRef RNAExpr::evaluateImpl() { return std::make_shared<RNAExpr>();}
  ExprRef RTPExpr::evaluateImpl() { return std::make_shared<RTPExpr>();}
  ExprRef RTNExpr::evaluateImpl() { return std::make_shared<RTNExpr>();}
  ExprRef RTZExpr::evaluateImpl() { return std::make_shared<RTZExpr>();}

  ExprRef ConstantExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(this->value(), this->bits());
  }
  ExprRef ConstantFloatExpr::evaluateImpl() {
    return std::make_shared<ConstantFloatExpr>(this->value(), this->bits());
  }

  ExprRef BoolExpr::evaluateImpl() {
    return std::make_shared<BoolExpr>(this->value());
  }

  ExprRef BinaryExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    ExprRef c1 = getChild(1)->evaluate();
    return checkExpr(CEB->createBinaryExpr(kind_, c0, c1));
  }

  ExprRef ExtractExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createExtract(c0, index_, bits_));
  }

  ExprRef ZExtExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createZExt(c0, bits_));
  }

  ExprRef SExtExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createSExt(c0, bits_));
  }

  ExprRef RolExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createRol(c0, bits_));
  }

  ExprRef RorExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    return checkExpr(CEB->createRor(c0, bits_));
  }

  ExprRef IteExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    ExprRef c1 = getChild(1)->evaluate();
    ExprRef c2 = getChild(2)->evaluate();
    return checkExpr(CEB->createIte(c0, c1, c2));
  }

  // I don't know how to evaluate an *ArrayExpr*. So I just
  // return the id here.
  ExprRef ArrayExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }

  /// FIXME:
  ExprRef StoreExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    ExprRef c1 = getChild(1)->evaluate();
    ExprRef c2 = getChild(2)->evaluate();
    return checkExpr(CEB->createStore(c0, c1, c2));
    // return std::make_shared<ConstantExpr>(1, 32);
  }

  /// FIXME:
  ExprRef SelectExpr::evaluateImpl() {
    ExprRef c0 = getChild(0)->evaluate();
    ExprRef c1 = getChild(1)->evaluate();
    return checkExpr(CEB->createSelect(c0, c1));
    // return std::make_shared<ConstantExpr>(1, 32);
  }
  ExprRef BVToFPExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }

  ExprRef FPToBVExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
  ExprRef FPToFPExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }

  ExprRef FPToSIExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }

  ExprRef FPToUIExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
  ExprRef SIToFPExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
  ExprRef UIToFPExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }

  ExprRef ParaExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
  ExprRef FuncExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
  ExprRef AppExpr::evaluateImpl() {
    return std::make_shared<ConstantExpr>(0, 32);
  }
} // namespace qsym