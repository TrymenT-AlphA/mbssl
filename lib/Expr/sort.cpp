#include <unordered_map>

#include "Expr/sort.h"

namespace MBSSL {

  /// Common ///
  [[nodiscard]] Sort::Sort(
      Sort_t sort_t,
      size_t arity
  ) noexcept :
      sort_t(sort_t),
      symbol(std::nullopt),
      arity(arity),
      parameters({}) {}

  [[nodiscard]] Sort::Sort(
      Sort_t sort_t,
      const std::vector<std::any>& parameters
  ) noexcept :
      sort_t(sort_t),
      symbol(std::nullopt),
      arity(parameters.size()),
      parameters(parameters) {}

  [[nodiscard]] Sort::Sort(
      Sort_t sort_t,
      const std::string& symbol,
      size_t arity
  ) noexcept : sort_t(sort_t),
               symbol(symbol),
               arity(arity),
               parameters({}) {}

  [[nodiscard]] Sort::Sort(
      Sort_t sort_t,
      const std::string& symbol,
      const std::vector<std::any>& parameters
  ) noexcept : sort_t(sort_t),
               symbol(symbol),
               arity(parameters.size()),
               parameters(parameters) {}

  [[nodiscard]] Sort_t Sort::getSortT() const noexcept {
    return sort_t;
  }

  [[nodiscard]] bool Sort::hasSymbol() const noexcept {
    return symbol.has_value();
  }

  [[nodiscard]] const std::string& Sort::getSymbol() const {
    assert(hasSymbol());
    return symbol.value();
  }

  [[nodiscard]] size_t Sort::getArity() const noexcept {
    return arity;
  }

  [[nodiscard]] const std::vector<std::any>& Sort::getParameters() const noexcept {
    return parameters;
  }

  [[nodiscard]] bool operator==(const Sort& lhs, const Sort& rhs) noexcept {
    return &lhs == &rhs;
  }

  /// Array ///
  [[nodiscard]] bool isArray(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::Array;
  }

  [[nodiscard]] ref<Sort> getDomain(ref<Sort> sort) {
    assert(isArray(sort));
    return sort->getParameter<ref<Sort>>(0);
  }

  [[nodiscard]] ref<Sort> getRange(ref<Sort> sort) {
    assert(isArray(sort));
    return sort->getParameter<ref<Sort>>(1);
  }

  [[nodiscard]] ref<Sort> SortArray(ref<Sort> domain, ref<Sort> range) noexcept {
    /* static pool contains all array sorts */
    static std::unordered_map<ref<Sort>, std::unordered_map<ref<Sort>, ref<Sort>>> array_sort_pool;

    if /* we already have the sort with domain in the pool */
        (auto it1 = array_sort_pool.find(domain); it1 != array_sort_pool.end()) {
      auto& tmp = it1->second;
      if /* we also have the sort with range in the pool */
          (auto it2 = tmp.find(range); it2 != tmp.end()) {
        return it2->second;
      } else /* we do not have the sort with range in the pool */ {
        std::string sym = "(Array " + domain->getSymbol() + " " + range->getSymbol() + ")";
        std::vector<std::any> params = {domain, range};
        return tmp[range] = mkref<Sort>(Sort_t::Array, sym, params);
      }
    }
    else /* we do not have the sort in the pool */ {
      /* first we create a map for domain */
      auto& tmp = array_sort_pool[domain];
      /* then we create a sort */
      std::string sym = "(Array " + domain->getSymbol() + " " + range->getSymbol() + ")";
      std::vector<std::any> params = {domain, range};
      return tmp[range] = mkref<Sort>(Sort_t::Array, sym, params);
    }
  }

  /// Bool ///
  [[nodiscard]] bool isBool(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::Bool;
  }

  [[nodiscard]] ref<Sort> SortBool() noexcept {
    static ref<Sort> bool_sort = mkref<Sort>(Sort_t::Bool, "Bool", 0);
    return bool_sort;
  }

  /// Bitvector ///
  [[nodiscard]] bool isBitVec(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::BitVec;
  }

  [[nodiscard]] uint64_t getWidth(ref<Sort> sort) {
    assert(isBitVec(sort));
    return sort->getParameter<uint64_t>(0);
  }

  [[nodiscard]] ref<Sort> SortBitVec(uint64_t width) noexcept {
    /* static pool contains all bitvec sorts */
    static std::unordered_map<uint64_t, ref<Sort>> bit_vec_sort_pool;

    if /* we already have the sort in the pool */
        (auto it = bit_vec_sort_pool.find(width); it != bit_vec_sort_pool.end()) {
      return it->second;
    }
    else /* we do not have the sort in the pool */ {
      std::string sym = "(_ BitVec " + std::to_string(width) + ")";
      std::vector<std::any> params = {width};
      return bit_vec_sort_pool[width] = mkref<Sort>(Sort_t::BitVec, sym, params);
    }
  }

  /// Floating-point ///
  [[nodiscard]] bool isRoundingMode(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::RoundingMode;
  }

  [[nodiscard]] ref<Sort> SortRoundingMode() noexcept {
    static const ref<Sort> rounding_mode_sort = mkref<Sort>(Sort_t::RoundingMode, "RoundingMode", 0);
    return rounding_mode_sort;
  }

  [[nodiscard]] bool isReal(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::Real;
  }

  [[nodiscard]] ref<Sort> SortReal() noexcept {
    static const ref<Sort> real_sort = mkref<Sort>(Sort_t::Real, "Real", 0);
    return real_sort;
  }

  [[nodiscard]] bool isFloatingPoint(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::FloatingPoint;
  }

  [[nodiscard]] uint64_t geteb(ref<Sort> sort) {
    assert(isFloatingPoint(sort));
    return sort->getParameter<uint64_t>(0);
  }

  [[nodiscard]] uint64_t getsb(ref<Sort> sort) {
    assert(isFloatingPoint(sort));
    return sort->getParameter<uint64_t>(1);
  }

  [[nodiscard]] ref<Sort> SortFloatingPoint(uint64_t eb, uint64_t sb) noexcept {
    assert(eb > 1 && sb > 1);
    /* static pool contains all floating point sorts */
    static std::unordered_map<uint64_t, std::unordered_map<uint64_t, ref<Sort>>> floating_point_sort_pool;

    if /* we already have the sort with eb in the pool */
        (auto it1 = floating_point_sort_pool.find(eb); it1 != floating_point_sort_pool.end()) {
      auto& tmp = it1->second;
      if /* we also have the sort with sb in the pool */
          (auto it2 = tmp.find(sb); it2 != tmp.end()) {
        return it2->second;
      } else /* we do not have the sort with sb in the pool */ {
        std::string sym = "(_ FloatingPoint " + std::to_string(eb) + " " + std::to_string(sb) + ")";
        std::vector<std::any> params = {eb, sb};
        return tmp[eb] = mkref<Sort>(Sort_t::FloatingPoint, sym, params);
      }
    }
    else /* we do not have the sort in the pool */ {
      /* first we create a map for eb */
      auto& tmp = floating_point_sort_pool[eb];
      /* then we create a sort */
      std::string sym = "(_ FloatingPoint " + std::to_string(eb) + " " + std::to_string(sb) + ")";
      std::vector<std::any> params = {eb, sb};
      return tmp[sb] = mkref<Sort>(Sort_t::FloatingPoint, sym, params);
    }
  }

  [[nodiscard]] ref<Sort> SortFloat16() noexcept {
    return SortFloatingPoint(5, 11);
  }
  [[nodiscard]] ref<Sort> SortFloat32() noexcept {
    return SortFloatingPoint(8, 24);
  }
  [[nodiscard]] ref<Sort> SortFloat64() noexcept {
    return SortFloatingPoint(11, 53);
  }
  [[nodiscard]] ref<Sort> SortFloat128() noexcept {
    return SortFloatingPoint(15, 113);
  }

  [[nodiscard]] ref<Sort> SortFloatingPoint(uint64_t bits) noexcept {
    if (bits == 16)
      return SortFloatingPoint(5, 11);
    else if (bits == 32)
      return SortFloatingPoint(8, 24);
    else if (bits == 64)
      return SortFloatingPoint(11, 53);
    else if (bits == 128)
      return SortFloatingPoint(15, 113);
    assert(false && "Unsupport FP Sort !");
  }

  [[nodiscard]] bool isUninterpreted(ref<Sort> sort) noexcept {
    return sort->getSortT() == Sort_t::Uninterpreted;
  }

  [[nodiscard]] ref<Sort> SortUninterpreted(size_t arity) noexcept {
    return mkref<Sort>(Sort_t::Uninterpreted, arity);
  }

  [[nodiscard]] ref<Sort> SortUninterpreted(const std::string& symbol, size_t arity) noexcept {
    return mkref<Sort>(Sort_t::Uninterpreted, symbol, arity);
  }

}
