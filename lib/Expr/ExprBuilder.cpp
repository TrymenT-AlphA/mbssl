#include "Expr/ExprBuilder.h"
#include <llvm/ADT/StringRef.h>

namespace qsym {

  namespace {
    const INT32 kComplexityThresholdForSimplify = 16;

    void addUses(ExprRef e) {
      for (INT i = 0; i < e->num_children(); i++)
        e->getChild(i)->addUse(e);
    }

// utility function for checking values
    bool isZero(ExprRef e) {
      ConstantExprRef ce = castAs<ConstantExpr>(e);
      return ce != NULL && ce->isZero();
    }

    bool isOne(ExprRef e) {
      ConstantExprRef ce = castAs<ConstantExpr>(e);
      return ce != NULL && ce->isOne();
    }

    bool isAllOnes(ExprRef e) {
      ConstantExprRef ce = castAs<ConstantExpr>(e);
      return ce != NULL && ce->isAllOnes();
    }

  } // namespace

  bool canEvaluateTruncated(ExprRef e, UINT bits, UINT depth=0) {
    if (depth > 1)
      return false;

    switch (e->kind()) {
      default:
        return false;
      case Mul:
        return canEvaluateTruncated(e->getChild(0), depth + 1)
               && canEvaluateTruncated(e->getChild(1), depth + 1);
      case UDiv:
      case URem: {
        UINT high_bits = e->bits() - bits;
        if (e->getChild(0)->countLeadingZeros() >= high_bits
            && e->getChild(1)->countLeadingZeros() >= high_bits) {
          return canEvaluateTruncated(e->getChild(0), depth + 1)
                 && canEvaluateTruncated(e->getChild(1), depth + 1);
        }
        else
          return false;
      }
      case ZExt:
      case SExt:
      case Constant:
      case Concat:
        return true;
    }
  }

  ExprRef evaluateInDifferentType(ExprBuilder* builder, ExprRef op, UINT32 index, UINT32 bits) {
    // TODO: recursive evaluation
    switch (op->kind()) {
      default:
        return NULL;
      case Mul:
      case UDiv:
      case URem: {
        ExprRef e1 = builder->createExtract(op->getChild(0), index, bits);
        ExprRef e2 = builder->createExtract(op->getChild(1), index, bits);

        return builder->createBinaryExpr(op->kind(),
                                         builder->createExtract(op->getChild(0), index, bits),
                                         builder->createExtract(op->getChild(1), index, bits));
      }
    }
  }


  ////////////////// ExprBuilder ////////////////////

  ExprBuilder::ExprBuilder() : next_(NULL) {}

  void ExprBuilder::setNext(ExprBuilder* next) {
    next_ = next;
  }

  ExprRef ExprBuilder::createTrue() {
    return createBool(true);
  }

  ExprRef ExprBuilder::createFalse() {
    return createBool(false);
  }

  ExprRef ExprBuilder::createMsb(ExprRef e) {
    return createExtract(e, e->bits() - 1, 1);
  }

  ExprRef ExprBuilder::createLsb(ExprRef e) {
    return createExtract(e, 0, 1);
  }

  ExprRef ExprBuilder::createPara(SortRef sort) {
    return next_->createPara(sort);
  }

  ExprRef ExprBuilder::createFunc(SortRef sort, const std::vector<ExprRef>& para_sort) {
    return next_->createFunc(sort, para_sort);
  }

  ExprRef ExprBuilder::createApp(ExprRef func, const std::vector<ExprRef> &para_expr) {
    return next_->createApp(func, para_expr);
  }

  ExprRef ExprBuilder::bitToBool(ExprRef e) {
    //QSYM_ASSERT(e->bits() == 1);
    ExprRef one = createConstant(1, e->bits());
    return createEqual(e, one);
  }

  ExprRef ExprBuilder::boolToBit(ExprRef e, UINT32 bits) {
    ExprRef e1 = createConstant(1, bits);
    ExprRef e0 = createConstant(0, bits);
    return createIte(e->bits() == 1 ? e : bitToBool(e) , e1, e0);
  }

  ExprRef ExprBuilder::intToFloat(ExprRef e, int is_double, int is_signed) {
    UINT32 bits = is_double ? 64 : 32;
    if(is_signed)
      return std::make_shared<SIToFPExpr>(e, bits);
    else
      return std::make_shared<UIToFPExpr>(e, bits);
  }

  ExprRef ExprBuilder::floatToFloat(ExprRef e, int to_double) {
    UINT32 bits = to_double ? 64 : 32;
    return std::make_shared<FPToFPExpr>(e, bits);
  }

  ExprRef ExprBuilder::floatToBits(ExprRef e) {
    return std::make_shared<FPToBVExpr>(e, e->bits());
  }

  ExprRef ExprBuilder::bitsToFloat(ExprRef e, int to_double) {
    UINT32 bits = to_double ? 64 : 32;
    if(e->kind()==FPToBV)
      return e->getFirstChild();
    return std::make_shared<BVToFPExpr>(e,bits);
  }

  ExprRef ExprBuilder::floatToSignInt(ExprRef e, uint8_t bits) {
    return std::make_shared<FPToSIExpr>(e,bits);
  }

  ExprRef ExprBuilder::floatToUnsignInt(ExprRef e, uint8_t bits) {
    return std::make_shared<FPToUIExpr>(e,bits);
  }

  ExprRef ExprBuilder::createConcat(std::list<ExprRef> exprs) {
    assert(!exprs.empty());
    auto it = exprs.begin();

    // get a first element from the list
    ExprRef e = *it;
    it++;

    for (; it != exprs.end(); it++)
      e = createConcat(e, *it);

    return e;
  }

  ExprRef ExprBuilder::createLAnd(std::list<ExprRef> exprs) {
    assert(!exprs.empty());
    auto it = exprs.begin();

    // get a first element from the list
    ExprRef e = *it;
    it++;

    for (; it != exprs.end(); it++)
      e = createLAnd(e, *it);

    return e;
  }

  ExprRef ExprBuilder::createTrunc(ExprRef e, UINT32 bits) {
    // fix by zgf : may use as bool in cmp
    if (bits == 1)
      return createEqual(e, createConstant(1, e->bits()));
    return createExtract(e, 0, bits);
  }

  ExprRef ExprBuilder::createReadWithName(ADDRINT off,std::string var_name) {
    return std::make_shared<ReadExpr>(off, var_name);
  }

  ExprRef ExprBuilder::createBool(bool b){
    return next_->createBool(b);
  }

  ExprRef ExprBuilder::createConstant(ADDRINT value, UINT32 bits){
    return next_->createConstant(value, bits);
  }

  ExprRef ExprBuilder::createConstant(llvm::APInt value, UINT32 bits){
    return next_->createConstant(value, bits);
  }

  ExprRef ExprBuilder::createConstantFloat(llvm::APFloat value, UINT32 bits){
    return next_->createConstantFloat(value, bits);
  }

  ExprRef ExprBuilder::createRead(ADDRINT off){
    return next_->createRead(off);
  }

  ExprRef ExprBuilder::createExtract(ExprRef e, UINT32 index, UINT32 bits){
    return next_->createExtract(e, index, bits);
  }

  ExprRef ExprBuilder::createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false){
    return next_->createIte(expr_cond, expr_true, expr_false);
  }

  ExprRef ExprBuilder::createArray(std::string name)
  {
    return next_->createArray(name);
  }

  ExprRef ExprBuilder::createStore(ExprRef array, ExprRef index, ExprRef newV)
  {
    return next_->createStore(array, index, newV);
  }

  ExprRef ExprBuilder::createSelect(ExprRef array, ExprRef index)
  {
    return next_->createSelect(array, index);
  }

  /// Change Expr Base Generator
#define CHANGE_EXPR_CLASS_GEN(_class_kind)                           \
ExprRef ExprBuilder::create ## _class_kind(ExprRef e, UINT32 bits) { \
  return next_->create ## _class_kind(e, bits);                      \
}

  CHANGE_EXPR_CLASS_GEN(ZExt)
  CHANGE_EXPR_CLASS_GEN(SExt)
  CHANGE_EXPR_CLASS_GEN(Rol)
  CHANGE_EXPR_CLASS_GEN(Ror)
#undef CHANGE_EXPR_CLASS_GEN


/// Single Expr Generator
#define SINGLE_EXPR_CLASS_GEN(_class_kind)               \
ExprRef ExprBuilder::create ## _class_kind (ExprRef e) { \
  return next_->create ## _class_kind(e);                \
}

  SINGLE_EXPR_CLASS_GEN(Neg)
  SINGLE_EXPR_CLASS_GEN(Not)
  SINGLE_EXPR_CLASS_GEN(LNot)
  SINGLE_EXPR_CLASS_GEN(FAbs)
  SINGLE_EXPR_CLASS_GEN(FNeg)
  SINGLE_EXPR_CLASS_GEN(FSqrt)

  SINGLE_EXPR_CLASS_GEN(FPIsZero)
  SINGLE_EXPR_CLASS_GEN(FPIsNorm)
  SINGLE_EXPR_CLASS_GEN(FPIsSubNorm)
  SINGLE_EXPR_CLASS_GEN(FPIsInf)
  SINGLE_EXPR_CLASS_GEN(FPIsNan)
  SINGLE_EXPR_CLASS_GEN(FPIsNeg)
  SINGLE_EXPR_CLASS_GEN(FPIsPos)
#undef SINGLE_EXPR_CLASS_GEN


/// Binary Expr Generator
#define BINARY_EXPR_CLASS_GEN(_class_kind)                           \
ExprRef ExprBuilder::create ## _class_kind (ExprRef l, ExprRef r) {  \
  return next_->create ## _class_kind(l, r);                         \
}
  // Common
  BINARY_EXPR_CLASS_GEN(Concat)

  // Logical
  BINARY_EXPR_CLASS_GEN(LOr)
  BINARY_EXPR_CLASS_GEN(LAnd)

  // Bv Arithmetic
  BINARY_EXPR_CLASS_GEN(Add)
  BINARY_EXPR_CLASS_GEN(Sub)
  BINARY_EXPR_CLASS_GEN(Mul)
  BINARY_EXPR_CLASS_GEN(UDiv)
  BINARY_EXPR_CLASS_GEN(SDiv)
  BINARY_EXPR_CLASS_GEN(URem)
  BINARY_EXPR_CLASS_GEN(SRem)
  BINARY_EXPR_CLASS_GEN(SMod)

  // Bits
  BINARY_EXPR_CLASS_GEN(And)
  BINARY_EXPR_CLASS_GEN(Or)
  BINARY_EXPR_CLASS_GEN(Xor)
  BINARY_EXPR_CLASS_GEN(Shl)
  BINARY_EXPR_CLASS_GEN(LShr)
  BINARY_EXPR_CLASS_GEN(AShr)

  // Bv Compare
  BINARY_EXPR_CLASS_GEN(Equal)
  BINARY_EXPR_CLASS_GEN(Distinct)
  BINARY_EXPR_CLASS_GEN(Ult)
  BINARY_EXPR_CLASS_GEN(Ule)
  BINARY_EXPR_CLASS_GEN(Ugt)
  BINARY_EXPR_CLASS_GEN(Uge)
  BINARY_EXPR_CLASS_GEN(Slt)
  BINARY_EXPR_CLASS_GEN(Sle)
  BINARY_EXPR_CLASS_GEN(Sgt)
  BINARY_EXPR_CLASS_GEN(Sge)

  // Float Arithmetic
  BINARY_EXPR_CLASS_GEN(FAdd)
  BINARY_EXPR_CLASS_GEN(FSub)
  BINARY_EXPR_CLASS_GEN(FMul)
  BINARY_EXPR_CLASS_GEN(FDiv)
  BINARY_EXPR_CLASS_GEN(FRem)
  BINARY_EXPR_CLASS_GEN(FMax)
  BINARY_EXPR_CLASS_GEN(FMin)

  // Float Compare
  BINARY_EXPR_CLASS_GEN(FOgt)
  BINARY_EXPR_CLASS_GEN(FOge)
  BINARY_EXPR_CLASS_GEN(FOlt)
  BINARY_EXPR_CLASS_GEN(FOle)
  BINARY_EXPR_CLASS_GEN(FOeq)
  BINARY_EXPR_CLASS_GEN(FOne)
  BINARY_EXPR_CLASS_GEN(FOrd)
  BINARY_EXPR_CLASS_GEN(FUno)
  BINARY_EXPR_CLASS_GEN(FUgt)
  BINARY_EXPR_CLASS_GEN(FUge)
  BINARY_EXPR_CLASS_GEN(FUlt)
  BINARY_EXPR_CLASS_GEN(FUle)
  BINARY_EXPR_CLASS_GEN(FUeq)
  BINARY_EXPR_CLASS_GEN(FUne)

#undef BINARY_EXPR_CLASS_GEN

/// Floating-Point Rounding Mode Expr Base Generator
#define ROUNDINGMODE_EXPR_CLASS_GEN(_class_kind)         \
ExprRef ExprBuilder::create ## _class_kind () {          \
  ExprRef ref = std::make_shared<_class_kind ## Expr>(); \
  return ref;                                            \
}

  ROUNDINGMODE_EXPR_CLASS_GEN(RNE)
  ROUNDINGMODE_EXPR_CLASS_GEN(RNA)
  ROUNDINGMODE_EXPR_CLASS_GEN(RTP)
  ROUNDINGMODE_EXPR_CLASS_GEN(RTN)
  ROUNDINGMODE_EXPR_CLASS_GEN(RTZ)
#undef ROUNDINGMODE_EXPR_CLASS_GEN

  ExprRef ExprBuilder::createBinaryExpr(Kind kind, ExprRef l, ExprRef r) {
    switch (kind) {
      case Add: return createAdd(l, r);
      case Sub: return createSub(l, r);
      case Mul: return createMul(l, r);
      case UDiv: return createUDiv(l, r);
      case SDiv: return createSDiv(l, r);
      case URem: return createURem(l, r);
      case SRem: return createSRem(l, r);
      case SMod: return createSMod(l, r);
      case And: return createAnd(l, r);
      case Or: return createOr(l, r);
      case Xor: return createXor(l, r);
      case Shl: return createShl(l, r);
      case LShr: return createLShr(l, r);
      case AShr: return createAShr(l, r);
      case Equal: return createEqual(l, r);
      case Distinct: return createDistinct(l, r);
      case Ult: return createUlt(l, r);
      case Ule: return createUle(l, r);
      case Ugt: return createUgt(l, r);
      case Uge: return createUge(l, r);
      case Slt: return createSlt(l, r);
      case Sle: return createSle(l, r);
      case Sgt: return createSgt(l, r);
      case Sge: return createSge(l, r);
      case FAdd: return createFAdd(l, r);
      case FSub: return createFSub(l, r);
      case FMul: return createFMul(l, r);
      case FDiv: return createFDiv(l, r);
      case FRem: return createFRem(l, r);
      case FMax: return createFMax(l, r);
      case FMin: return createFMin(l, r);
      case FOgt: return createFOgt(l, r);
      case FOge: return createFOge(l, r);
      case FOlt: return createFOlt(l, r);
      case FOle: return createFOle(l, r);
      case FOeq: return createFOeq(l, r);
      case FOne: return createFOne(l, r);
      case FOrd: return createFOrd(l, r);
      case FUno: return createFUno(l, r);
      case FUgt: return createFUgt(l, r);
      case FUge: return createFUge(l, r);
      case FUlt: return createFUlt(l, r);
      case FUle: return createFUle(l, r);
      case FUeq: return createFUeq(l, r);
      case FUne: return createFUne(l, r);
      case LOr: return createLOr(l, r);
      case LAnd: return createLAnd(l, r);
      case Concat: return createConcat(l, r);
      default:
        LOG_FATAL("Non-binary expr: " + std::to_string(kind) + "\n");
        return NULL;
    }
  }

  ExprRef ExprBuilder::createUnaryExpr(Kind kind, ExprRef e) {
    switch (kind) {
      case Not: return createNot(e);
      case Neg: return createNeg(e);
      case LNot: return createLNot(e);
      case FAbs: return createFAbs(e);
      case FNeg: return createFNeg(e);
      case FSqrt: return createFSqrt(e);
      default:
        LOG_FATAL("Non-unary expr: " + std::to_string(kind) + "\n");
        return NULL;
    }
  }

  ExprRef ExprBuilder::createExtExpr(Kind kind, ExprRef e, uint32_t bits) {
    switch (kind) {
      case SExt: return createSExt(e, bits);
      case ZExt: return createZExt(e, bits);
      default:
        LOG_FATAL("Non-unary expr: " + std::to_string(kind) + "\n");
        return NULL;
    }
  }

  //////////////// BaseExprBuilder /////////////////

  ExprRef BaseExprBuilder::createBool(bool b) {
    ExprRef ref = std::make_shared<BoolExpr>(b);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createRead(ADDRINT off) {
    static std::vector<ExprRef> cache;
    if (off >= cache.size())
      cache.resize(off + 1);

    if (cache[off] == NULL)
      cache[off] = std::make_shared<ReadExpr>(off);

    return cache[off];
  }

  ExprRef BaseExprBuilder::createExtract(ExprRef e, UINT32 index, UINT32 bits)
  {
    if (bits == e->bits())
      return e;
    ExprRef ref = std::make_shared<ExtractExpr>(e, index, bits);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createArray(std::string name)
  {
    ExprRef ref = std::make_shared<ArrayExpr>(name);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createConstant(ADDRINT value, UINT32 bits) {
    ExprRef ref = std::make_shared<ConstantExpr>(value, bits);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createConstant(llvm::APInt value, UINT32 bits) {
    ExprRef ref = std::make_shared<ConstantExpr>(value, bits);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createConstantFloat(llvm::APFloat value, UINT32 bits) {
    ExprRef ref = std::make_shared<ConstantFloatExpr>(value, bits);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false) {
    ExprRef ref = std::make_shared<IteExpr>(expr_cond, expr_true, expr_false);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createStore(ExprRef array, ExprRef index, ExprRef newV) {
    ExprRef ref = std::make_shared<StoreExpr>(array, index, newV);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createSelect(ExprRef array, ExprRef index) {
    ExprRef ref = std::make_shared<SelectExpr>(array, index);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createPara(SortRef sort) {
    ExprRef ref = std::make_shared<ParaExpr>(sort);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createFunc(SortRef sort, const std::vector<ExprRef>& para_sort) {
    ExprRef ref = std::make_shared<FuncExpr>(sort, para_sort);
    addUses(ref);
    return ref;
  }

  ExprRef BaseExprBuilder::createApp(ExprRef func, const std::vector<ExprRef> &para_expr) {
    ExprRef ref = std::make_shared<AppExpr>(func, para_expr);
    addUses(ref);
    return ref;
  }

/// Change Expr Base Generator
#define CHANGE_EXPR_CLASS_BASE_GEN(_class_kind)                          \
ExprRef BaseExprBuilder::create ## _class_kind(ExprRef e, UINT32 bits) { \
  ExprRef ref = std::make_shared<_class_kind ## Expr>(e, bits);          \
  addUses(ref);                                                          \
  return ref;                                                            \
}

  CHANGE_EXPR_CLASS_BASE_GEN(ZExt)
  CHANGE_EXPR_CLASS_BASE_GEN(SExt)
  CHANGE_EXPR_CLASS_BASE_GEN(Rol)
  CHANGE_EXPR_CLASS_BASE_GEN(Ror)
#undef CHANGE_EXPR_CLASS_BASE_GEN


/// Single Expr Base Generator
#define SINGLE_EXPR_CLASS_BASE_GEN(_class_kind)              \
ExprRef BaseExprBuilder::create ## _class_kind (ExprRef e) { \
  ExprRef ref = std::make_shared<_class_kind ## Expr>(e);    \
  addUses(ref);                                              \
  return ref;                                                \
}                                                            \

  SINGLE_EXPR_CLASS_BASE_GEN(Neg)
  SINGLE_EXPR_CLASS_BASE_GEN(Not)
  SINGLE_EXPR_CLASS_BASE_GEN(LNot)
  SINGLE_EXPR_CLASS_BASE_GEN(FAbs)
  SINGLE_EXPR_CLASS_BASE_GEN(FNeg)
  SINGLE_EXPR_CLASS_BASE_GEN(FSqrt)

  SINGLE_EXPR_CLASS_BASE_GEN(FPIsZero)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsNorm)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsSubNorm)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsInf)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsNan)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsNeg)
  SINGLE_EXPR_CLASS_BASE_GEN(FPIsPos)
#undef SINGLE_EXPR_CLASS_BASE_GEN

/// Binary Expr Base Generator
#define BINARY_EXPR_CLASS_BASE_GEN(_class_kind)                         \
ExprRef BaseExprBuilder::create ## _class_kind (ExprRef l, ExprRef r) { \
  ExprRef ref = std::make_shared<_class_kind ## Expr>(l ,r);            \
  addUses(ref);                                                         \
  return ref;                                                           \
}

  BINARY_EXPR_CLASS_BASE_GEN(Concat)

  // Logical
  BINARY_EXPR_CLASS_BASE_GEN(LOr)
  BINARY_EXPR_CLASS_BASE_GEN(LAnd)

  // Bv Arithmetic
  BINARY_EXPR_CLASS_BASE_GEN(Add)
  BINARY_EXPR_CLASS_BASE_GEN(Sub)
  BINARY_EXPR_CLASS_BASE_GEN(Mul)
  BINARY_EXPR_CLASS_BASE_GEN(UDiv)
  BINARY_EXPR_CLASS_BASE_GEN(SDiv)
  BINARY_EXPR_CLASS_BASE_GEN(URem)
  BINARY_EXPR_CLASS_BASE_GEN(SRem)
  BINARY_EXPR_CLASS_BASE_GEN(SMod)

  // Bits
  BINARY_EXPR_CLASS_BASE_GEN(And)
  BINARY_EXPR_CLASS_BASE_GEN(Or)
  BINARY_EXPR_CLASS_BASE_GEN(Xor)
  BINARY_EXPR_CLASS_BASE_GEN(Shl)
  BINARY_EXPR_CLASS_BASE_GEN(LShr)
  BINARY_EXPR_CLASS_BASE_GEN(AShr)

  // Bv Compare
  BINARY_EXPR_CLASS_BASE_GEN(Equal)
  BINARY_EXPR_CLASS_BASE_GEN(Distinct)
  BINARY_EXPR_CLASS_BASE_GEN(Ult)
  BINARY_EXPR_CLASS_BASE_GEN(Ule)
  BINARY_EXPR_CLASS_BASE_GEN(Ugt)
  BINARY_EXPR_CLASS_BASE_GEN(Uge)
  BINARY_EXPR_CLASS_BASE_GEN(Slt)
  BINARY_EXPR_CLASS_BASE_GEN(Sle)
  BINARY_EXPR_CLASS_BASE_GEN(Sgt)
  BINARY_EXPR_CLASS_BASE_GEN(Sge)

  // Float Arithmetic
  BINARY_EXPR_CLASS_BASE_GEN(FAdd)
  BINARY_EXPR_CLASS_BASE_GEN(FSub)
  BINARY_EXPR_CLASS_BASE_GEN(FMul)
  BINARY_EXPR_CLASS_BASE_GEN(FDiv)
  BINARY_EXPR_CLASS_BASE_GEN(FRem)
  BINARY_EXPR_CLASS_BASE_GEN(FMax)
  BINARY_EXPR_CLASS_BASE_GEN(FMin)

  // Float Compare
  BINARY_EXPR_CLASS_BASE_GEN(FOgt)
  BINARY_EXPR_CLASS_BASE_GEN(FOge)
  BINARY_EXPR_CLASS_BASE_GEN(FOlt)
  BINARY_EXPR_CLASS_BASE_GEN(FOle)
  BINARY_EXPR_CLASS_BASE_GEN(FOeq)
  BINARY_EXPR_CLASS_BASE_GEN(FOne)
  BINARY_EXPR_CLASS_BASE_GEN(FOrd)
  BINARY_EXPR_CLASS_BASE_GEN(FUno)
  BINARY_EXPR_CLASS_BASE_GEN(FUgt)
  BINARY_EXPR_CLASS_BASE_GEN(FUge)
  BINARY_EXPR_CLASS_BASE_GEN(FUlt)
  BINARY_EXPR_CLASS_BASE_GEN(FUle)
  BINARY_EXPR_CLASS_BASE_GEN(FUeq)
  BINARY_EXPR_CLASS_BASE_GEN(FUne)

#undef BINARY_EXPR_CLASS_BASE_GEN



  //////////////// CacheExprBuilder /////////////////

  ExprRef CacheExprBuilder::findOrInsert(ExprRef new_expr) {
    if (ExprRef cached = findInCache(new_expr))
      return cached;
    QSYM_ASSERT(new_expr != NULL);
    insertToCache(new_expr);
    return new_expr;
  }

  void CacheExprBuilder::insertToCache(ExprRef e) {
    cache_.insert(e);
  }

  ExprRef CacheExprBuilder::findInCache(ExprRef e) {
    return cache_.find(e);
  }

  ExprRef CacheExprBuilder::createExtract(ExprRef e, UINT32 index, UINT32 bits) {
    ExprRef new_expr = ExprBuilder::createExtract(e, index, bits);
    return findOrInsert(new_expr);
  }

  ExprRef CacheExprBuilder::createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false) {
    ExprRef new_expr = ExprBuilder::createIte(expr_cond, expr_true, expr_false);
    return findOrInsert(new_expr);
  }

  ExprRef CacheExprBuilder::createStore(ExprRef array, ExprRef index, ExprRef newV) {
    ExprRef new_expr = ExprBuilder::createStore(array, index, newV);
    return findOrInsert(new_expr);
  }

  ExprRef CacheExprBuilder::createSelect(ExprRef array, ExprRef index) {
    ExprRef new_expr = ExprBuilder::createSelect(array, index);
    return findOrInsert(new_expr);
  }

/// Change Expr Base Generator
#define CHANGE_EXPR_CLASS_CACHE_GEN(_class_kind)                          \
ExprRef CacheExprBuilder::create ## _class_kind(ExprRef e, UINT32 bits) { \
  ExprRef new_expr = ExprBuilder::create ## _class_kind(e, bits);         \
  return findOrInsert(new_expr);                                          \
}

  CHANGE_EXPR_CLASS_CACHE_GEN(ZExt)
  CHANGE_EXPR_CLASS_CACHE_GEN(SExt)
  CHANGE_EXPR_CLASS_CACHE_GEN(Rol)
  CHANGE_EXPR_CLASS_CACHE_GEN(Ror)
#undef CHANGE_EXPR_CLASS_CACHE_GEN

/// Single Expr Base Generator
#define SINGLE_EXPR_CLASS_CACHE_GEN(_class_kind)              \
ExprRef CacheExprBuilder::create ## _class_kind (ExprRef e) {  \
  ExprRef new_expr = ExprBuilder::create ## _class_kind(e);   \
  return findOrInsert(new_expr);                              \
}

  SINGLE_EXPR_CLASS_CACHE_GEN(Neg)
  SINGLE_EXPR_CLASS_CACHE_GEN(Not)
  SINGLE_EXPR_CLASS_CACHE_GEN(LNot)
  SINGLE_EXPR_CLASS_CACHE_GEN(FAbs)
  SINGLE_EXPR_CLASS_CACHE_GEN(FNeg)
  SINGLE_EXPR_CLASS_CACHE_GEN(FSqrt)

  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsZero)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsNorm)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsSubNorm)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsInf)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsNan)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsNeg)
  SINGLE_EXPR_CLASS_CACHE_GEN(FPIsPos)
#undef SINGLE_EXPR_CLASS_CACHE_GEN

/// Binary Expr Cache Generator
#define BINARY_EXPR_CLASS_CACHE_GEN(_class_kind)                        \
ExprRef CacheExprBuilder::create ## _class_kind (ExprRef l, ExprRef r) {\
  ExprRef new_expr = ExprBuilder::create ## _class_kind(l, r);          \
  return findOrInsert(new_expr);                                        \
}

  BINARY_EXPR_CLASS_CACHE_GEN(Concat)

  // Logical
  BINARY_EXPR_CLASS_CACHE_GEN(LOr)
  BINARY_EXPR_CLASS_CACHE_GEN(LAnd)

  // Bv Arithmetic
  BINARY_EXPR_CLASS_CACHE_GEN(Add)
  BINARY_EXPR_CLASS_CACHE_GEN(Sub)
  BINARY_EXPR_CLASS_CACHE_GEN(Mul)
  BINARY_EXPR_CLASS_CACHE_GEN(UDiv)
  BINARY_EXPR_CLASS_CACHE_GEN(SDiv)
  BINARY_EXPR_CLASS_CACHE_GEN(URem)
  BINARY_EXPR_CLASS_CACHE_GEN(SRem)
  BINARY_EXPR_CLASS_CACHE_GEN(SMod)

  // Bits
  BINARY_EXPR_CLASS_CACHE_GEN(And)
  BINARY_EXPR_CLASS_CACHE_GEN(Or)
  BINARY_EXPR_CLASS_CACHE_GEN(Xor)
  BINARY_EXPR_CLASS_CACHE_GEN(Shl)
  BINARY_EXPR_CLASS_CACHE_GEN(LShr)
  BINARY_EXPR_CLASS_CACHE_GEN(AShr)

  // Bv Compare
  BINARY_EXPR_CLASS_CACHE_GEN(Equal)
  BINARY_EXPR_CLASS_CACHE_GEN(Distinct)
  BINARY_EXPR_CLASS_CACHE_GEN(Ult)
  BINARY_EXPR_CLASS_CACHE_GEN(Ule)
  BINARY_EXPR_CLASS_CACHE_GEN(Ugt)
  BINARY_EXPR_CLASS_CACHE_GEN(Uge)
  BINARY_EXPR_CLASS_CACHE_GEN(Slt)
  BINARY_EXPR_CLASS_CACHE_GEN(Sle)
  BINARY_EXPR_CLASS_CACHE_GEN(Sgt)
  BINARY_EXPR_CLASS_CACHE_GEN(Sge)

  // Float Arithmetic
  BINARY_EXPR_CLASS_CACHE_GEN(FAdd)
  BINARY_EXPR_CLASS_CACHE_GEN(FSub)
  BINARY_EXPR_CLASS_CACHE_GEN(FMul)
  BINARY_EXPR_CLASS_CACHE_GEN(FDiv)
  BINARY_EXPR_CLASS_CACHE_GEN(FRem)
  BINARY_EXPR_CLASS_CACHE_GEN(FMax)
  BINARY_EXPR_CLASS_CACHE_GEN(FMin)

  // Float Compare
  BINARY_EXPR_CLASS_CACHE_GEN(FOgt)
  BINARY_EXPR_CLASS_CACHE_GEN(FOge)
  BINARY_EXPR_CLASS_CACHE_GEN(FOlt)
  BINARY_EXPR_CLASS_CACHE_GEN(FOle)
  BINARY_EXPR_CLASS_CACHE_GEN(FOeq)
  BINARY_EXPR_CLASS_CACHE_GEN(FOne)
  BINARY_EXPR_CLASS_CACHE_GEN(FOrd)
  BINARY_EXPR_CLASS_CACHE_GEN(FUno)
  BINARY_EXPR_CLASS_CACHE_GEN(FUgt)
  BINARY_EXPR_CLASS_CACHE_GEN(FUge)
  BINARY_EXPR_CLASS_CACHE_GEN(FUlt)
  BINARY_EXPR_CLASS_CACHE_GEN(FUle)
  BINARY_EXPR_CLASS_CACHE_GEN(FUeq)
  BINARY_EXPR_CLASS_CACHE_GEN(FUne)
#undef BINARY_EXPR_CLASS_CACHE_GEN


  ////////////////// CommonSimplifyExprBuilder ////////////////////

  ExprRef CommonSimplifyExprBuilder::createConcat(ExprRef l, ExprRef r) {
    // C(E(e, x, y), E(e, y, z)) => E(e, x, z)
    if (auto ee_l = castAs<ExtractExpr>(l)) {
      if (auto ee_r = castAs<ExtractExpr>(r)) {
        if (ee_l->expr() == ee_r->expr()
            && ee_r->index() + ee_r->bits() == ee_l->index()) {
          return createExtract(ee_l->expr(), ee_r->index(), ee_r->bits() + ee_l->bits());
        }
      }
    }

    // C(E(Ext(e), e->bits(), bits), e) == E(Ext(e), 0, e->bits() + bits)
    if (auto ee_l = castAs<ExtractExpr>(l)) {
      if (auto ext = castAs<ExtExpr>(ee_l->expr())) {
        if (ee_l->index() == r->bits()
            && equalShallowly(*ext->expr(), *r)) {
          // Here we used equalShallowly
          // because same ExtractExpr can have different addresses,
          // but using deep comparison is expensive
          return createExtract(ee_l->expr(), 0, ee_l->bits() + r->bits());
        }
      }
    }

    return ExprBuilder::createConcat(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createExtract(
      ExprRef e, UINT32 index, UINT32 bits) {
    if (auto ce = castAs<ConcatExpr>(e)) {
      // skips right part
      if (index >= ce->getRight()->bits())
        return createExtract(ce->getLeft(), index - ce->getRight()->bits(), bits);

      // skips left part
      if (index + bits <= ce->getRight()->bits())
        return createExtract(ce->getRight(), index, bits);

      // E(C(C_0,y)) ==> C(E(C_0), E(y))
      if (ce->getLeft()->isConstant()) {
        return createConcat(
            createExtract(ce->getLeft(), 0, bits - ce->getRight()->bits() + index),
            createExtract(ce->getRight(), index, ce->getRight()->bits() - index));
      }
    }
    else if (auto ee = castAs<ExtExpr>(e)) {
      // E(Ext(x), i, b) && len(x) >= i + b == E(x, i, b)
      if (ee->expr()->bits() >= index + bits)
        return createExtract(ee->expr(), index, bits);

      // E(ZExt(x), i, b) && len(x) < i == 0
      if (ee->kind() == ZExt
          && index >= ee->expr()->bits())
        return createConstant(0, bits);
    }
    else if (auto extractExpr = castAs<ExtractExpr>(e)) {
      // E(E(x, i1, b1), i2, b2) == E(x, i1 + i2, b2)
      return createExtract(extractExpr->expr(), extractExpr->index() + index, bits);
    }

    if (index == 0 && e->bits() == bits)
      return e;
    return ExprBuilder::createExtract(e, index, bits);
  }

  ExprRef CommonSimplifyExprBuilder::createZExt(ExprRef e, UINT32 bits) {
    // allow shrinking
    if (e->bits() > bits)
      return createExtract(e, 0, bits);
    if (e->bits() == bits)
      return e;
    return ExprBuilder::createZExt(e, bits);
  }

  ExprRef CommonSimplifyExprBuilder::createAdd(ExprRef l, ExprRef r){
    if (isZero(l))
      return r;

    return ExprBuilder::createAdd(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createMul(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    // 0 * X ==> 0
    if (isZero(l))
      return l;

    // 1 * X ==> X
    if (isOne(l))
      return r;

    return ExprBuilder::createMul(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createFAdd(ExprRef l, ExprRef r){
    return ExprBuilder::createFAdd(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createFMul(ExprRef l, ExprRef r) {
    return ExprBuilder::createFMul(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::simplifyAnd(ExprRef l, ExprRef r) {
    // l & 0 ==> 0
    if (isZero(l))
      return l;

    // l & 11...1b ==> l;
    if (isAllOnes(l))
      return r;

    return NULL;
  }

  ExprRef CommonSimplifyExprBuilder::createAnd(ExprRef l, ExprRef r)
  {
    if (ExprRef simplified = simplifyAnd(l, r))
      return simplified;

    // 0x00ff0000  & 0xaabbccdd = 0x00bb0000
    if (auto const_l = castAs<ConstantExpr>(l)) {
      if (auto concat_r = castAs<ConcatExpr>(r)) {
        ExprRef r_left = concat_r->getLeft();
        ExprRef r_right = concat_r->getRight();
        ExprRef l_left = createExtract(l, r_right->bits(), r_left->bits());

        if (ExprRef and_left = simplifyAnd(l_left, r_left)) {
          return createConcat(
              and_left,
              createAnd(
                  createExtract(l, 0,  r_right->bits()),
                  r_right));
        }
      }
    }

    return ExprBuilder::createAnd(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::simplifyOr(ExprRef l, ExprRef r) {
    // 0 | X ==> 0
    if (isZero(l))
      return r;

    // 111...1b | X ==> 111...1b
    if (isAllOnes(l))
      return l;

    return NULL;
  }

  ExprRef CommonSimplifyExprBuilder::createOr(ExprRef l, ExprRef r) {
    if (ExprRef simplified = simplifyOr(l, r))
      return simplified;

    if (auto const_l = castAs<ConstantExpr>(l)) {
      if (auto concat_r = castAs<ConcatExpr>(r)) {
        ExprRef r_left = concat_r->getLeft();
        ExprRef r_right = concat_r->getRight();
        ExprRef l_left = createExtract(l, r_right->bits(), r_left->bits());

        if (ExprRef and_left = simplifyOr(l_left, r_left)) {
          return createConcat(
              and_left,
              createOr(
                  createExtract(l, 0,  r_right->bits()),
                  r_right));
        }
      }
    }

    return ExprBuilder::createOr(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::simplifyXor(ExprRef l, ExprRef r) {
    // 0 ^ X ==> X
    if (isZero(l))
      return r;

    return NULL;
  }

  ExprRef CommonSimplifyExprBuilder::createXor(ExprRef l, ExprRef r) {
    if (ExprRef simplified = simplifyXor(l, r))
      return simplified;

    if (auto const_l = castAs<ConstantExpr>(l)) {
      if (auto concat_r = castAs<ConcatExpr>(r)) {
        ExprRef r_left = concat_r->getLeft();
        ExprRef r_right = concat_r->getRight();
        ExprRef l_left = createExtract(l, r_right->bits(), r_left->bits());

        if (ExprRef and_left = simplifyXor(l_left, r_left)) {
          return createConcat(
              and_left,
              createXor(
                  createExtract(l, 0,  r_right->bits()),
                  r_right));
        }
      }
    }

    return ExprBuilder::createXor(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createShl(ExprRef l, ExprRef r) {
    if (isZero(l))
      return l;

    if (ConstantExprRef ce_r = castAs<ConstantExpr>(r)) {
      ADDRINT rval = ce_r->value().getLimitedValue();
      if (rval == 0)
        return l;

      // l << larger_than_l_size == 0
      if (rval >= l->bits())
        return createConstant(0, l->bits());

      // from z3: (bvshl x k) -> (concat (extract [n-1-k:0] x) bv0:k)
      // but byte granuality
      if (rval % CHAR_BIT == 0) {
        ExprRef zero = createConstant(0, rval);
        ExprRef partial = createExtract(l, 0, l->bits() - rval);
        return createConcat(partial, zero);
      }
    }

    return ExprBuilder::createShl(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createLShr(ExprRef l, ExprRef r) {
    if (isZero(l))
      return l;

    if (ConstantExprRef ce_r = castAs<ConstantExpr>(r)) {
      ADDRINT rval = ce_r->value().getLimitedValue();
      if (rval == 0)
        return l;

      // l << larger_than_l_size == 0
      if (rval >= l->bits())
        return createConstant(0, l->bits());

      // from z3: (bvlshr x k) -> (concat bv0:k (extract [n-1:k] x))
      // but byte granuality
      if (rval % CHAR_BIT == 0) {
        ExprRef zero = createConstant(0, rval);
        ExprRef partial = createExtract(l, rval, l->bits() - rval);
        return createConcat(zero, partial);
      }
    }

    return ExprBuilder::createLShr(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createAShr(ExprRef l, ExprRef r) {
    if (ConstantExprRef ce_r = castAs<ConstantExpr>(r)) {
      ADDRINT rval = ce_r->value().getLimitedValue();
      if (rval == 0)
        return l;
    }

    return ExprBuilder::createAShr(l, r);
  }

  ExprRef CommonSimplifyExprBuilder::createEqual(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (auto be_l = castAs<BoolExpr>(l))
      return (be_l->value()) ? r : createLNot(r);

    if (l->isZero() || l->isOne()) {
      ExprRef maybeIte = r;
      if (auto ee_r = castAs<ExtExpr>(r))
        maybeIte = ee_r->expr();

      if (auto ite = castAs<IteExpr>(maybeIte)) {
        // fix by zgf
        // case ITE(cond,1,0)
        if (ite->expr_true()->isOne()){
          return l->isZero() ? createLNot(ite->expr_cond()) : ite->expr_cond();
        }else{
          // case ITE(cond,0,1)
          return l->isZero() ? ite->expr_cond() : createLNot(ite->expr_cond()) ;
        }
      }
    }

    return ExprBuilder::createEqual(l, r);
  }



  ////////////////// SymbolicExprBuilder ////////////////////

  ExprBuilder* SymbolicExprBuilder::create() {
    ExprBuilder* base = new BaseExprBuilder();
    ExprBuilder* commu = new CommutativeExprBuilder();
    ExprBuilder* common = new CommonSimplifyExprBuilder();
    ExprBuilder* const_folding = new ConstantFoldingExprBuilder();
    ExprBuilder* symbolic = new SymbolicExprBuilder();
    ExprBuilder* cache = new CacheExprBuilder();

    // commu -> symbolic -> common -> constant folding -> base
    commu->setNext(symbolic);
    symbolic->setNext(common);
    common->setNext(const_folding);
    const_folding->setNext(cache);
    cache->setNext(base);
    return commu;
  }

  ExprRef SymbolicExprBuilder::createConcat(ExprRef l, ExprRef r) {
    // C(l, C(x, y)) && l, x == constant => C(l|x, y)
    if (auto ce = castAs<ConcatExpr>(r)) {
      ConstantExprRef ce_l = castAs<ConstantExpr>(l);
      ConstantExprRef ce_x = castAs<ConstantExpr>(ce->getLeft());
      if (ce_l != NULL && ce_x != NULL)
        return createConcat(createConcat(ce_l, ce_x), ce->getRight());
    }

    // C(C(x ,y), z) => C(x, C(y, z))
    if (auto ce = castAs<ConcatExpr>(l)) {
      return createConcat(l->getLeft(),
                          createConcat(l->getRight(), r));
    }

    return ExprBuilder::createConcat(l, r);
  }

  ExprRef SymbolicExprBuilder::createExtract(ExprRef op, UINT32 index, UINT32 bits) {
    // Only byte-wise simplification
    if (index == 0
        && bits % 8 == 0
        && canEvaluateTruncated(op, bits)) {
      if (ExprRef e = evaluateInDifferentType(this, op, index, bits))
        return e;
    }

    return ExprBuilder::createExtract(op, index, bits);
  }

  ExprRef SymbolicExprBuilder::simplifyExclusiveExpr(ExprRef l, ExprRef r) {
    // From z3
    // (bvor (concat x #x00) (concat #x00 y)) --> (concat x y)
    // (bvadd (concat x #x00) (concat #x00 y)) --> (concat x y)

    for (UINT i = 0; i < l->bits(); i++)
      if (!isZeroBit(l, i) && !isZeroBit(r, i))
        return NULL;

    std::list<ExprRef> exprs;
    UINT32 i = 0;
    while (i < l->bits()) {
      UINT32 prev = i;
      while (i < l->bits() && isZeroBit(l, i))
        i++;
      if (i != prev)
        exprs.push_front(createExtract(r, prev, i - prev));
      prev = i;
      while (i < r->bits() && isZeroBit(r, i))
        i++;
      if (i != prev)
        exprs.push_front(createExtract(l, prev, i - prev));
    }

    return ExprBuilder::createConcat(exprs);
  }

  ExprRef SymbolicExprBuilder::createFAdd(ExprRef l, ExprRef r) {
    if (ExprRef e = simplifyExclusiveExpr(l, r))
      return e;
    return ExprBuilder::createFAdd(l, r);
  }

  ExprRef SymbolicExprBuilder::createFSub(ExprRef l, ExprRef r) {
    return ExprBuilder::createFSub(l, r);
  }

  ExprRef SymbolicExprBuilder::createFMul(ExprRef l, ExprRef r) {
    return ExprBuilder::createFMul(l, r);
  }

  ExprRef SymbolicExprBuilder::createFDiv(ExprRef l, ExprRef r) {
    return ExprBuilder::createFDiv(l, r);
  }

  ExprRef SymbolicExprBuilder::createFMax(ExprRef l, ExprRef r) {
    return ExprBuilder::createFMax(l, r);
  }

  ExprRef SymbolicExprBuilder::createFMin(ExprRef l, ExprRef r) {
    return ExprBuilder::createFMin(l, r);
  }

  ExprRef SymbolicExprBuilder::createFAbs(ExprRef e) {
    return ExprBuilder::createFAbs(e);
  }

  ExprRef SymbolicExprBuilder::createFNeg(ExprRef e) {
    return ExprBuilder::createFNeg(e);
  }

  ExprRef SymbolicExprBuilder::createFSqrt(ExprRef e) {
    return ExprBuilder::createFSqrt(e);
  }


  ExprRef SymbolicExprBuilder::createAdd(ExprRef l, ExprRef r) {
    if (ExprRef e = simplifyExclusiveExpr(l, r))
      return e;

    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (ConstantExprRef ce_l = castAs<ConstantExpr>(l))
        return createAdd(ce_l, nce_r);
      else {
        NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
        QSYM_ASSERT(nce_l != NULL);
        return createAdd(nce_l, nce_r);
      }
    }
    else
      return ExprBuilder::createAdd(l, r);
  }

  ExprRef SymbolicExprBuilder::createAdd(ConstantExprRef l, NonConstantExprRef r) {
    switch (r->kind()) {
      case Add: {
        // C_0 + (C_1 + X) ==> (C_0 + C_1) + X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createAdd(createAdd(l, CE), r->getSecondChild());
        // C_0 + (X + C_1) ==> (C_0 + C_1) + X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(createAdd(l, CE), r->getFirstChild());
        break;
      }

      case Sub: {
        // C_0 + (C_1 - X) ==> (C_0 + C1) - X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createSub(createAdd(l, CE), r->getSecondChild());
        // C_0 + (X - C_1) ==> (C_0 - C1) + X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(createSub(l, CE), r->getFirstChild());
        break;
      }
      default:
        break;
    }

    return ExprBuilder::createAdd(l, r);
  }

  ExprRef SymbolicExprBuilder::createAdd(NonConstantExprRef l, NonConstantExprRef r) {
    if (l == r) {
      // l + l ==> 2 * l
      ExprRef two = createConstant(2, l->bits());
      return createMul(two, l);
    }

    switch (l->kind()) {
      default: break;
      case Add:
      case Sub: {
        // (X + Y) + Z ==> Z + (X + Y)
        // Or (X - Y) + Z ==> Z + (X - Y)
        std::swap(l, r);
      }
    }

    switch (r->kind()) {
      case Add: {
        // X + (C_0 + Y) ==> C_0 + (X + Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createAdd(CE, createAdd(l, r->getSecondChild()));
        // X + (Y + C_0) ==> C_0 + (X + Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(CE, createAdd(l, r->getFirstChild()));
        break;
      }

      case Sub: {
        // X + (C_0 - Y) ==> C_0 + (X - Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createAdd(CE, createSub(l, r->getSecondChild()));
        // X + (Y - C_0) ==> -C_0 + (X + Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(createNeg(CE), createAdd(l, r->getFirstChild()));
        break;
      }
      default:
        break;
    }

    return ExprBuilder::createAdd(l, r);
  }

  ExprRef SymbolicExprBuilder::createSub(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (ConstantExprRef ce_l = castAs<ConstantExpr>(l))
        return createSub(ce_l, nce_r);
      else {
        NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
        QSYM_ASSERT(nce_l != NULL);
        return createSub(nce_l, nce_r);
      }
    }
    else
      return ExprBuilder::createSub(l, r);
  }

  ExprRef SymbolicExprBuilder::createSub(ConstantExprRef l, NonConstantExprRef r) {
    switch (r->kind()) {
      case Add: {
        // C_0 - (C_1 + X) ==> (C_0 - C1) - X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createSub(createSub(l, CE), r->getSecondChild());
        // C_0 - (X + C_1) ==> (C_0 - C1) - X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createSub(createSub(l, CE), r->getFirstChild());
        break;
      }

      case Sub: {
        // C_0 - (C_1 - X) ==> (C_0 - C1) + X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild())) {
          return createAdd(createSub(l, CE), r->getSecondChild());
        }
        // C_0 - (X - C_1) ==> (C_0 + C1) - X
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild())) {
          return createSub(createAdd(l, CE), r->getFirstChild());
        }
        break;
      }
      default:
        break;
    }

    return ExprBuilder::createSub(l, r);
  }

  ExprRef SymbolicExprBuilder::createSub(
      NonConstantExprRef l,
      NonConstantExprRef r) {
    // X - X ==> 0
    if (l == r)
      return createConstant(0, l->bits());

    switch (l->kind()) {
      default: break;
      case Add:
        if (l->getChild(0)->isConstant()) {
          // (C + Y) - Z ==> C + (Y - Z)
          return createAdd(l->getChild(0),
                           createSub(l->getChild(1), r));
        }
        break;
      case Sub: {
        if (l->getChild(0)->isConstant()) {
          // (C - Y) - Z ==> C - (Y + Z)
          return createSub(l->getChild(0),
                           createAdd(l->getChild(1), r));
        }
        break;
      }
    }

    switch (r->kind()) {
      case Add: {
        // X - (C_0 + Y) ==> -C_0 + (X - Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createAdd(createNeg(CE), createSub(l, r->getSecondChild()));
        // X - (Y + C_0) ==> -C_0 + (X - Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(createNeg(CE), createSub(l, r->getFirstChild()));
        break;
      }

      case Sub: {
        // X - (C_0 - Y) ==> -C_0 + (X + Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getFirstChild()))
          return createAdd(createNeg(CE), createAdd(l, r->getSecondChild()));
        // X - (Y - C_0) ==> C_0 + (X - Y)
        if (ConstantExprRef CE = castAs<ConstantExpr>(r->getSecondChild()))
          return createAdd(CE, createSub(l, r->getFirstChild()));
        break;
      }
      default:
        break;
    }
    return ExprBuilder::createSub(l, r);
  }

  ExprRef SymbolicExprBuilder::createMul(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (ConstantExprRef ce_l = castAs<ConstantExpr>(l))
        return createMul(ce_l, nce_r);
    }

    return ExprBuilder::createMul(l, r);
  }

  ExprRef SymbolicExprBuilder::createMul(ConstantExprRef l, NonConstantExprRef r) {
    // C_0 * (C_1 * x) ==> (C_0 * C_1) * x
    if (auto me = castAs<MulExpr>(r)) {
      if (ConstantExprRef ce = castAs<ConstantExpr>(r->getLeft())) {
        return createMul(createMul(l, ce), r->getRight());
      }
    }

    // C_0 * (C_1 + x) ==> C_0 * C_1 + C_0 * x
    if (auto ae = castAs<AddExpr>(r)) {
      if (ConstantExprRef ce = castAs<ConstantExpr>(r->getLeft())) {
        return createAdd(createMul(l, ce), createMul(l, r->getRight()));
      }
    }

    return ExprBuilder::createMul(l, r);
  }

  ExprRef SymbolicExprBuilder::createSDiv(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_l = castAs<NonConstantExpr>(l)) {
      if (ConstantExprRef ce_r = castAs<ConstantExpr>(r))
        return createSDiv(nce_l, ce_r);
    }

    return ExprBuilder::createSDiv(l, r);
  }

  ExprRef SymbolicExprBuilder::createSDiv(NonConstantExprRef l, ConstantExprRef r) {
    // x /s -1 = -x
    if (r->isAllOnes())
      return createNeg(l);

    // SExt(x) /s y && x->bits() >= y->getActiveBits() ==> SExt(x /s y)
    // Only works when y != -1, but already handled by the above statement
    if (auto sext_l = castAs<SExtExpr>(l)) {
      ExprRef x = sext_l->expr();
      if (x->bits() >= r->getActiveBits()) {
        return createSExt(
            createSDiv(x,
                       createExtract(r, 0, x->bits())),
            l->bits());
      }
    }

    // TODO: add overflow check
    // (x / C_0) / C_1 = (x / (C_0 * C_1))
    if (auto se = castAs<SDivExpr>(l)) {
      if (ConstantExprRef ce = castAs<ConstantExpr>(l->getRight())) {
        return createSDiv(l->getLeft(), createMul(ce, r));
      }
    }
    return ExprBuilder::createSDiv(l, r);
  }

  ExprRef SymbolicExprBuilder::createUDiv(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_l = castAs<NonConstantExpr>(l)) {
      if (ConstantExprRef ce_r = castAs<ConstantExpr>(r))
        return createUDiv(nce_l, ce_r);
    }

    return ExprBuilder::createUDiv(l, r);
  }

  ExprRef SymbolicExprBuilder::createUDiv(NonConstantExprRef l, ConstantExprRef r) {
    // C(0, x) / y && y->getActiveBits() <= x->bits()
    // == C(0, x/E(y, 0, x->bits()))
    if (auto ce = castAs<ConcatExpr>(l)) {
      ExprRef ce_l = ce->getLeft();
      ExprRef ce_r = ce->getRight();
      if (ce_l->isZero()) {
        if (r->getActiveBits() <= ce_r->bits()) {
          ExprRef e = createConcat(
              ce_l,
              createUDiv(
                  ce_r,
                  createExtract(r, 0, ce_r->bits())));
          return e;
        }
      }
    }

    // TODO: add overflow check
    // (x / C_0) / C_1 = (x / (C_0 * C_1))
    if (auto se = castAs<UDivExpr>(l)) {
      if (ConstantExprRef ce = castAs<ConstantExpr>(l->getRight())) {
        return createUDiv(l->getLeft(), createMul(ce, r));
      }
    }
    return ExprBuilder::createUDiv(l, r);
  }

  ExprRef SymbolicExprBuilder::createAnd(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (ConstantExprRef ce_l = castAs<ConstantExpr>(l))
        return createAnd(ce_l, nce_r);
      else {
        NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
        QSYM_ASSERT(nce_l != NULL);
        return createAnd(nce_l, nce_r);
      }
    }
    else
      return ExprBuilder::createAnd(l, r);
  }

  ExprRef SymbolicExprBuilder::createAnd(ConstantExprRef l, NonConstantExprRef r) {
    return ExprBuilder::createAnd(l, r);
  }

  ExprRef SymbolicExprBuilder::createAnd(NonConstantExprRef l, NonConstantExprRef r) {
    // A & A  ==> A
    if (l == r)
      return l;

    // C(x, y) & C(w, v) ==> C(x & w, y & v)
    if (auto ce_l = castAs<ConcatExpr>(l)) {
      if (auto ce_r = castAs<ConcatExpr>(r)) {
        if (ce_l->getLeft()->bits() == ce_r->getLeft()->bits()) {
          // right bits are same, because it is binary operation
          return createConcat(
              createAnd(l->getLeft(), r->getLeft()),
              createAnd(l->getRight(), r->getRight()));
        }
      }
    }
    return ExprBuilder::createAnd(l, r);
  }

  ExprRef SymbolicExprBuilder::createOr(ExprRef l, ExprRef r) {
    if (ExprRef e = simplifyExclusiveExpr(l, r))
      return e;

    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (ConstantExprRef ce_l = castAs<ConstantExpr>(l))
        return createOr(ce_l, nce_r);
      else {
        NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
        QSYM_ASSERT(nce_l != NULL);
        return createOr(nce_l, nce_r);
      }
    }
    else
      return ExprBuilder::createOr(l, r);
  }

  ExprRef SymbolicExprBuilder::createOr(ConstantExprRef l, NonConstantExprRef r) {
    if (auto ce = castAs<ConcatExpr>(r)) {
      // C_0 | C(x, y) ==> C(C_0 | x, C_0 | y)
      // TODO: only for constant case
      return createConcat(
          createOr(
              createExtract(l, ce->getRight()->bits(), ce->getLeft()->bits()),
              ce->getLeft()),
          createOr(
              createExtract(l, 0, ce->getRight()->bits()),
              ce->getRight()));
    }

    return ExprBuilder::createOr(l, r);
  }

  ExprRef SymbolicExprBuilder::createOr(NonConstantExprRef l, NonConstantExprRef r) {
    // A | A = A
    if (l == r)
      return l;

    // C(x, y) & C(w, v) == C(x | w, y | v)
    if (auto ce_l = castAs<ConcatExpr>(l)) {
      if (auto ce_r = castAs<ConcatExpr>(r)) {
        if (ce_l->getLeft()->bits() == ce_r->getLeft()->bits()) {
          // right bits are same, because it is binary operation
          return createConcat(
              createOr(l->getLeft(), r->getLeft()),
              createOr(l->getRight(), r->getRight()));
        }
      }
    }

    return ExprBuilder::createOr(l, r);
  }

  ExprRef SymbolicExprBuilder::createXor(ExprRef l, ExprRef r) {
    if (NonConstantExprRef nce_r = castAs<NonConstantExpr>(r)) {
      if (NonConstantExprRef nce_l = castAs<NonConstantExpr>(l)) {
        return createXor(nce_l, nce_r);
      }
    }

    return ExprBuilder::createXor(l, r);
  }

  ExprRef SymbolicExprBuilder::createXor(NonConstantExprRef l, NonConstantExprRef r) {
    if (l == r)
      return createConstant(0, l->bits());

    return ExprBuilder::createXor(l, r);
  }

  ExprRef SymbolicExprBuilder::createEqual(ExprRef l, ExprRef r) {
    if (l == r)
      return createTrue();

    // add by zgf to optimize equal builder
    // equal(lnot(a),1) -> lnot(a)
    // equal(lnot(a),0) -> a
    if (auto N_L = castAs<LNotExpr>(l)){
      if (auto B_R = castAs<BoolExpr>(r)){
        if (B_R->value() == true){
          return N_L;
        }else{
          return N_L->getChild(0);
        }
      }
    }else if(auto N_R = castAs<LNotExpr>(r)){
      if (auto B_L = castAs<BoolExpr>(l)){
        if (B_L->value() == true){
          return N_R;
        }else{
          return N_R->getChild(0);
        }
      }
    }

    // equal(0, zext(a)) -> equal(0, a)
    // equal(1, zext(a)) -> equal(1, a)
    if (auto ZEXT_L = castAs<ZExtExpr>(l)){
      if (r->isZero() || r->isOne()){
        ExprRef child  = ZEXT_L->getChild(0);
        ExprRef boolVal = createConstant(r->isOne(), child->bits());
        return createEqual(child, boolVal);
      }
    }
    if (auto ZEXT_R = castAs<ZExtExpr>(r)){
      if (l->isZero() || l->isOne()){
        ExprRef child  = ZEXT_R->getChild(0);
        ExprRef boolVal = createConstant(l->isOne(), child->bits());
        return createEqual(boolVal, child);
      }
    }


    // equal(0, ite(a, 1, 0)) -> lnot(a)
    // equal(0, ite(a, 0, 1)) -> a
    // equal(1, ite(a, 1, 0)) -> a
    // equal(1, ite(a, 0, 1)) -> lnot(a)
    if (auto ITE_L = castAs<IteExpr>(l)){
      if (r->isZero() || r->isOne()){
        if ((ITE_L->getChild(1)->isOne() && r->isOne()) ||
            (ITE_L->getChild(1)->isZero() && r->isZero())){
          return ITE_L->getChild(0);
        }else if ((ITE_L->getChild(1)->isOne() && r->isZero()) ||
                  (ITE_L->getChild(1)->isZero() && r->isOne())){
          return createLNot(ITE_L->getChild(0));
        }
      }
    }
    if (auto ITE_R = castAs<IteExpr>(r)){
      if (l->isZero() || l->isOne()){
        if ((ITE_R->getChild(1)->isOne() && l->isOne()) ||
            (ITE_R->getChild(1)->isZero() && l->isZero())){
          return ITE_R->getChild(0);
        }else if ((ITE_R->getChild(1)->isOne() && l->isZero()) ||
                  (ITE_R->getChild(1)->isZero() && l->isOne())){
          return createLNot(ITE_R->getChild(0));
        }
      }
    }

    return ExprBuilder::createEqual(l, r);
  }

  ExprRef SymbolicExprBuilder::createDistinct(ExprRef l, ExprRef r) {
    // add by zgf to optimize distinct builder

    // distinct(ite(a,1,0), 1) -> lnot(a)
    // distinct(ite(a,1,0), 0) -> a
    // distinct(ite(a,0,1), 1) -> a
    // distinct(ite(a,0,1), 0) -> lnot(a)
    if (auto ITE_L = castAs<IteExpr>(l)){
      if ((r->isOne() && ITE_L->getChild(1)->isOne()) ||
          (r->isZero() && ITE_L->getChild(1)->isZero()) ){
        return createLNot(ITE_L->getChild(0));
      }
      if ((r->isOne() && ITE_L->getChild(1)->isZero()) ||
          (r->isZero() && ITE_L->getChild(1)->isOne()) ){
        return ITE_L->getChild(0);
      }
    }
    if (auto ITE_R = castAs<IteExpr>(r)){
      if ((l->isOne() && ITE_R->getChild(1)->isOne()) ||
          (l->isZero() && ITE_R->getChild(1)->isZero()) ){
        return createLNot(ITE_R->getChild(0));
      }
      if ((l->isOne() && ITE_R->getChild(1)->isZero()) ||
          (l->isZero() && ITE_R->getChild(1)->isOne()) ){
        return ITE_R->getChild(0);
      }
    }

    // distinct(0, zext(a)) -> equal(1, a)
    // distinct(1, zext(a)) -> equal(0, a)
    if (auto ZEXT_L = castAs<ZExtExpr>(l)){
      if (r->isZero() || r->isOne()){
        ExprRef child  = ZEXT_L->getChild(0);
        ExprRef boolVal = createConstant(r->isOne() ? 0 : 1, child->bits());
        return createEqual(child, boolVal);
      }
    }
    if (auto ZEXT_R = castAs<ZExtExpr>(r)){
      if (l->isZero() || l->isOne()){
        ExprRef child  = ZEXT_R->getChild(0);
        ExprRef boolVal = createConstant(r->isOne() ? 0 : 1, child->bits());
        return createEqual(boolVal, child);
      }
    }

    // distinct(lnot(a), true) -> a
    // distinct(lnot(a), false) -> lnot(a)
    if (auto LNOT_L = castAs<LNotExpr>(l)){
      if (r->isOne()){
        return LNOT_L->getChild(0);
      }
      if (r->isZero()){
        return LNOT_L;
      }
    }
    if (auto LNOT_R = castAs<LNotExpr>(r)){
      if (l->isOne()){
        return LNOT_R->getChild(0);
      }
      if (l->isZero()){
        return LNOT_R;
      }
    }

    return createLNot(createEqual(l, r));
  }

  ExprRef SymbolicExprBuilder::createLOr(ExprRef l, ExprRef r) {
    if (auto BE_L = castAs<BoolExpr>(l))
      return BE_L->value() ? createTrue() : r;

    if (auto BE_R = castAs<BoolExpr>(r))
      return BE_R->value() ? createTrue() : l;

    // fix by zgf
    if ( !l->isCmpOrBit() ){
      ExprRef lb = bitToBool(l);
      ExprRef rb = bitToBool(r);
      return ExprBuilder::createLOr(l, r);
    }

    return ExprBuilder::createLOr(l, r);
  }

  ExprRef SymbolicExprBuilder::createLAnd(ExprRef l, ExprRef r) {
    if (auto BE_L = castAs<BoolExpr>(l))
      return BE_L->value() ? r : createFalse();

    if (auto BE_R = castAs<BoolExpr>(r))
      return BE_R->value() ? l : createFalse();

    // fix by zgf
    if ( !l->isCmpOrBit() ){
      ExprRef lb = bitToBool(l);
      ExprRef rb = bitToBool(r);
      return ExprBuilder::createLAnd(lb, rb);
    }

    return ExprBuilder::createLAnd(l, r);
  }

  ExprRef SymbolicExprBuilder::createLNot(ExprRef e) {
    if (auto BE = castAs<BoolExpr>(e))
      return createBool(!BE->value());
    if (auto NE = castAs<LNotExpr>(e))
      return NE->expr();

    // fix by zgf
    if ( !e->isCmpOrBit() ){
      ExprRef boolExpr = bitToBool(e);
      return ExprBuilder::createLNot(boolExpr);
    }
    return ExprBuilder::createLNot(e);
  }

  ExprRef SymbolicExprBuilder::createIte(
      ExprRef expr_cond,
      ExprRef expr_true,
      ExprRef expr_false) {
    if (auto BE = castAs<BoolExpr>(expr_cond))
      return BE->value() ? expr_true : expr_false;
    if (auto NE = castAs<LNotExpr>(expr_cond))
      return createIte(NE->expr(), expr_false, expr_true);
    return ExprBuilder::createIte(expr_cond, expr_true, expr_false);
  }


  //////////////// CommutativeExprBuilder /////////////////

  ExprRef CommutativeExprBuilder::createSub(ExprRef l, ExprRef r){
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL) {
      // X - C_0 = -C_0 + X
      return createAdd(createNeg(ce_r), nce_l);
    }
    else
      return ExprBuilder::createSub(l, r);
  }

  ExprRef CommutativeExprBuilder::createAdd(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createAdd(ce_r, nce_l);

    return ExprBuilder::createAdd(l, r);
  }

  ExprRef CommutativeExprBuilder::createMul(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createMul(ce_r, nce_l);

    return ExprBuilder::createMul(l, r);
  }

  ExprRef CommutativeExprBuilder::createAnd(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createAnd(ce_r, nce_l);

    return ExprBuilder::createAnd(l, r);
  }

  ExprRef CommutativeExprBuilder::createOr(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createOr(ce_r, nce_l);

    return ExprBuilder::createOr(l, r);
  }

  ExprRef CommutativeExprBuilder::createXor(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createXor(ce_r, nce_l);

    return ExprBuilder::createXor(l, r);
  }

  ExprRef CommutativeExprBuilder::createEqual(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createEqual(ce_r, nce_l);

    return ExprBuilder::createEqual(l, r);
  }

  ExprRef CommutativeExprBuilder::createDistinct(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createDistinct(ce_r, nce_l);

    return ExprBuilder::createDistinct(l, r);
  }

  ExprRef CommutativeExprBuilder::createUlt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createUgt(ce_r, nce_l);

    return ExprBuilder::createUlt(l, r);
  }

  ExprRef CommutativeExprBuilder::createUle(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createUge(ce_r, nce_l);

    return ExprBuilder::createUle(l, r);
  }

  ExprRef CommutativeExprBuilder::createUgt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createUlt(ce_r, nce_l);

    return ExprBuilder::createUgt(l, r);
  }

  ExprRef CommutativeExprBuilder::createUge(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createUle(ce_r, nce_l);

    return ExprBuilder::createUge(l, r);
  }

  ExprRef CommutativeExprBuilder::createSlt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createSgt(ce_r, nce_l);

    return ExprBuilder::createSlt(l, r);
  }

  ExprRef CommutativeExprBuilder::createSle(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createSge(ce_r, nce_l);

    return ExprBuilder::createSle(l, r);
  }

  ExprRef CommutativeExprBuilder::createSgt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createSlt(ce_r, nce_l);

    return ExprBuilder::createSgt(l, r);
  }

  ExprRef CommutativeExprBuilder::createSge(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createSle(ce_r, nce_l);

    return ExprBuilder::createSge(l, r);
  }

  ExprRef CommutativeExprBuilder::createFAdd(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFAdd(ce_r, nce_l);

    return ExprBuilder::createFAdd(l, r);
  }

  ExprRef CommutativeExprBuilder::createFSub(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFSub(ce_r, nce_l);

    return ExprBuilder::createFSub(l, r);
  }

  ExprRef CommutativeExprBuilder::createFMul(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFMul(ce_r, nce_l);

    return ExprBuilder::createFMul(l, r);
  }

  ExprRef CommutativeExprBuilder::createFDiv(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFDiv(ce_r, nce_l);

    return ExprBuilder::createFDiv(l, r);
  }

  ExprRef CommutativeExprBuilder::createFRem(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFRem(ce_r, nce_l);

    return ExprBuilder::createFRem(l, r);
  }

  ExprRef CommutativeExprBuilder::createFMax(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFRem(ce_r, nce_l);

    return ExprBuilder::createFMax(l, r);
  }

  ExprRef CommutativeExprBuilder::createFMin(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFMin(ce_r, nce_l);

    return ExprBuilder::createFMin(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOgt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOlt(ce_r, nce_l);

    return ExprBuilder::createFOgt(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOge(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOle(ce_r, nce_l);

    return ExprBuilder::createFOge(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOlt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOgt(ce_r, nce_l);

    return ExprBuilder::createFOlt(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOle(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOge(ce_r, nce_l);

    return ExprBuilder::createFOle(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOeq(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOeq(ce_r, nce_l);

    return ExprBuilder::createFOeq(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOne(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOne(ce_r, nce_l);

    return ExprBuilder::createFOne(l, r);
  }

  ExprRef CommutativeExprBuilder::createFOrd(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFOrd(ce_r, nce_l);

    return ExprBuilder::createFOrd(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUno(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUno(ce_r, nce_l);

    return ExprBuilder::createFUno(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUgt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUlt(ce_r, nce_l);

    return ExprBuilder::createFUgt(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUge(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUle(ce_r, nce_l);

    return ExprBuilder::createFUge(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUlt(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUgt(ce_r, nce_l);

    return ExprBuilder::createFUlt(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUle(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUge(ce_r, nce_l);

    return ExprBuilder::createFUle(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUeq(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUeq(ce_r, nce_l);

    return ExprBuilder::createFUeq(l, r);
  }

  ExprRef CommutativeExprBuilder::createFUne(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createFUne(ce_r, nce_l);

    return ExprBuilder::createFUne(l, r);
  }

  ExprRef CommutativeExprBuilder::createLAnd(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createLAnd(ce_r, nce_l);

    return ExprBuilder::createLAnd(l, r);
  }

  ExprRef CommutativeExprBuilder::createLOr(ExprRef l, ExprRef r) {
    NonConstantExprRef nce_l = castAs<NonConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (nce_l != NULL && ce_r != NULL)
      return createLOr(ce_r, nce_l);

    return ExprBuilder::createLOr(l, r);
  }


  ////////////////// ConstantFoldingExprBuilder ////////////////////

  ExprBuilder* ConstantFoldingExprBuilder::create() {
    // constant folding -> base
    ExprBuilder* const_folding = new ConstantFoldingExprBuilder();
    ExprBuilder* base = new BaseExprBuilder();

    // commu -> symbolic -> common -> constant folding -> base
    const_folding->setNext(base);
    return const_folding;
  }

  ExprRef ConstantFoldingExprBuilder::createDistinct(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value() != ce_r->value());
    }

    BoolExprRef be0 = castAs<BoolExpr>(l);
    BoolExprRef be1 = castAs<BoolExpr>(r);

    if (be0 != NULL && be1 != NULL) {
      return createBool(be0->value() != be1->value());
    }

    return ExprBuilder::createDistinct(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createEqual(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value() == ce_r->value());
    }

    BoolExprRef be0 = castAs<BoolExpr>(l);
    BoolExprRef be1 = castAs<BoolExpr>(r);

    if (be0 != NULL && be1 != NULL)
      return createBool(be0->value() == be1->value());

    return ExprBuilder::createEqual(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createLAnd(ExprRef l, ExprRef r) {
    BoolExprRef be0 = castAs<BoolExpr>(l);
    BoolExprRef be1 = castAs<BoolExpr>(r);

    if (be0 != NULL && be1 != NULL)
      return createBool(be0->value() && be1->value());
    else
      return ExprBuilder::createLAnd(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createLOr(ExprRef l, ExprRef r) {
    BoolExprRef be0 = castAs<BoolExpr>(l);
    BoolExprRef be1 = castAs<BoolExpr>(r);

    if (be0 != NULL && be1 != NULL)
      return createBool(be0->value() || be1->value());
    else
      return ExprBuilder::createLOr(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFRem(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->frem(*ce_r), l->bits());
    }
    else
      return ExprBuilder::createFRem(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFMax(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->fpmax(*ce_r), l->bits());
    }
    else
      return ExprBuilder::createFMax(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFMin(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->fpmin(*ce_r), l->bits());
    }
    else
      return ExprBuilder::createFMin(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFAbs(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e))
      return createConstantFloat(expr->fabs(), e->bits());
    else
      return ExprBuilder::createFAbs(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFNeg(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e))
      return createConstantFloat(expr->fneg(), e->bits());
    else
      return ExprBuilder::createFNeg(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFSqrt(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e))
      return createConstantFloat(expr->fsqrt(), e->bits());
    else
      return ExprBuilder::createFSqrt(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsZero(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isZero())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsZero(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsNorm(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isNormal())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsNorm(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsSubNorm(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isDenormal())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsSubNorm(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsInf(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isInfinity())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsInf(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsNan(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isNaN())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsNan(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsNeg(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isNegative())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsNeg(e);
  }

  ExprRef ConstantFoldingExprBuilder::createFPIsPos(ExprRef e) {
    if(ConstantFloatExprRef expr = castAs<ConstantFloatExpr>(e)){
      if (expr->value().isNegative() && expr->value().isNonZero())
        return ExprBuilder::createTrue();
      return ExprBuilder::createFalse();
    }
    else
      return ExprBuilder::createFPIsNeg(e);
  }

  ExprRef ConstantFoldingExprBuilder::createConcat(ExprRef l, ExprRef r) {
    // C(l, r) && l == constant && r == constant  => l << r_bits | r
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      UINT32 bits = ce_l->bits() + ce_r->bits();
      llvm::APInt lval = ce_l->value().zext(bits);
      llvm::APInt rval = ce_r->value().zext(bits);
      llvm::APInt res = (lval << ce_r->bits()) | rval;
      return createConstant(res, bits);
    }
    else
      return ExprBuilder::createConcat(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createIte(ExprRef expr_cond,
                                                ExprRef expr_true,
                                                ExprRef expr_false) {
    if (auto be = castAs<BoolExpr>(expr_cond)) {
      if (be->value())
        return expr_true;
      else
        return expr_false;
    }

    return ExprBuilder::createIte(expr_cond, expr_true, expr_false);
  }

  ExprRef ConstantFoldingExprBuilder::createExtract(
      ExprRef e, UINT32 index, UINT32 bits) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e)) {
      llvm::APInt v = ce->value().lshr(index);
      v = v.zextOrTrunc(bits);
      return createConstant(v, bits);
    }
    else
      return ExprBuilder::createExtract(e, index, bits);
  }

  ExprRef ConstantFoldingExprBuilder::createZExt(ExprRef e, UINT32 bits) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e)) {
      return createConstant(ce->value().zext(bits), bits);
    }
    else
      return ExprBuilder::createZExt(e, bits);
  }

  ExprRef ConstantFoldingExprBuilder::createSExt(ExprRef e, UINT32 bits) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e)) {
      return createConstant(ce->value().sext(bits), bits);
    }
    else
      return ExprBuilder::createSExt(e, bits);
  }

  ExprRef ConstantFoldingExprBuilder::createRol(ExprRef e, UINT32 bits) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e)) {
      return createConstant(ce->value().rotl(bits), bits);
    }
    else
      return ExprBuilder::createRol(e, bits);
  }

  ExprRef ConstantFoldingExprBuilder::createRor(ExprRef e, UINT32 bits) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e)) {
      return createConstant(ce->value().rotr(bits), bits);
    }
    else
      return ExprBuilder::createRor(e, bits);
  }

  ExprRef ConstantFoldingExprBuilder::createNeg(ExprRef e) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e))
      return createConstant(-ce->value(), ce->bits());
    else
      return ExprBuilder::createNeg(e);
  }

  ExprRef ConstantFoldingExprBuilder::createNot(ExprRef e) {
    if (ConstantExprRef ce = castAs<ConstantExpr>(e))
      return createConstant(~ce->value(), ce->bits());
    else
      return ExprBuilder::createNot(e);
  }

  ExprRef ConstantFoldingExprBuilder::createLNot(ExprRef e) {
    if (BoolExprRef be = castAs<BoolExpr>(e))
      return createBool(!be->value());
    else
      return ExprBuilder::createLNot(e);
  }

  ExprRef ConstantFoldingExprBuilder::createAShr(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().ashr(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createAShr(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createAdd(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() + ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createAdd(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createAnd(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() & ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createAnd(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFAdd(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->value() + ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createFAdd(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFDiv(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->value() / ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createFDiv(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFMul(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->value() * ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createFMul(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOeq(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->foeq(*ce_r));
    }
    else
      return ExprBuilder::createFOeq(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOge(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->foge(*ce_r));
    }
    else
      return ExprBuilder::createFOge(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOgt(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->fogt(*ce_r));
    }
    else
      return ExprBuilder::createFOgt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOle(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->fole(*ce_r));
    }
    else
      return ExprBuilder::createFOle(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOlt(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->folt(*ce_r));
    }
    else
      return ExprBuilder::createFOlt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFOne(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->fone(*ce_r));
    }
    else
      return ExprBuilder::createFOne(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createFSub(ExprRef l, ExprRef r) {
    ConstantFloatExprRef ce_l = castAs<ConstantFloatExpr>(l);
    ConstantFloatExprRef ce_r = castAs<ConstantFloatExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstantFloat(ce_l->value() - ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createFSub(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createLShr(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().lshr(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createLShr(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createMul(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() * ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createMul(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createOr(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() | ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createOr(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSDiv(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().sdiv(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createSDiv(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSRem(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().srem(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createSRem(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSMod(ExprRef l, ExprRef r) {
    return ExprBuilder::createSMod(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSge(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().sge(ce_r->value()));
    }
    else
      return ExprBuilder::createSge(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSgt(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().sgt(ce_r->value()));
    }
    else
      return ExprBuilder::createSgt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createShl(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() << ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createShl(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSle(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().sle(ce_r->value()));
    }
    else
      return ExprBuilder::createSle(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSlt(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().slt(ce_r->value()));
    }
    else
      return ExprBuilder::createSlt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createSub(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() - ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createSub(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createUDiv(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().udiv(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createUDiv(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createURem(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value().urem(ce_r->value()), l->bits());
    }
    else
      return ExprBuilder::createURem(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createUge(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().uge(ce_r->value()));
    }
    else
      return ExprBuilder::createUge(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createUgt(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().ugt(ce_r->value()));
    }
    else
      return ExprBuilder::createUgt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createUle(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().ule(ce_r->value()));
    }
    else
      return ExprBuilder::createUle(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createUlt(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createBool(ce_l->value().ult(ce_r->value()));
    }
    else
      return ExprBuilder::createUlt(l, r);
  }

  ExprRef ConstantFoldingExprBuilder::createXor(ExprRef l, ExprRef r) {
    ConstantExprRef ce_l = castAs<ConstantExpr>(l);
    ConstantExprRef ce_r = castAs<ConstantExpr>(r);

    if (ce_l != NULL && ce_r != NULL) {
      QSYM_ASSERT(l->bits() == r->bits());
      return createConstant(ce_l->value() ^ ce_r->value(), l->bits());
    }
    else
      return ExprBuilder::createXor(l, r);
  }

} // namespace qsym