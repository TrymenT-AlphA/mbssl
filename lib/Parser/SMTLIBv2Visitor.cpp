#include <variant>

#include "Parser/SMTLIBv2Visitor.h"

namespace MBSSL {

namespace AST {
  struct SpecConstant {
    enum SpecConstant_t {
      numeral,
      decimal,
      hexadecimal,
      binary,
      string,
    };
    SMTLIBv2Parser::Spec_constantContext* context;
    SpecConstant_t spec_constant_t;
    std::any field;

  };

  struct SExpr {
    enum SExpr_t {
      spec_constant,
      symbol,
      keyword,
      ss_expr,
    };
    SMTLIBv2Parser::S_exprContext* context;
    SExpr_t s_expr_t;
    std::any field;

  };

  struct Index {
    enum Index_t {
      numeral,
      symbol,
    };
    SMTLIBv2Parser::IndexContext* context;
    Index_t index_t;
    std::any field;
  };

  struct Identifier {
    SMTLIBv2Parser::IdentifierContext* context;
    std::string symbol;
    std::vector<Index> index;
  };

  struct VarBinding {
    SMTLIBv2Parser::Var_bindingContext* context;
    std::string symbol;
    ExprRef expr;
  };

  struct SortedVar {
    SMTLIBv2Parser::Sorted_varContext* context;
    std::string symbol;
    ref<Sort> sort;
  };
}

using namespace MBSSL::AST;

std::any /* void */ SMTLIBv2Visitor::visitStart(SMTLIBv2Parser::StartContext *context){
  visit(context->script());
  return std::any{};
}

std::any SMTLIBv2Visitor::visitResponse(SMTLIBv2Parser::ResponseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any /* std::string */ SMTLIBv2Visitor::visitGeneralReservedWord(SMTLIBv2Parser::GeneralReservedWordContext *context){
  #define Case(type) if (context->type())
  Case(GRW_Exclamation) return context->GRW_Exclamation()->getText();
  Case(GRW_Underscore) return context->GRW_Underscore()->getText();
  Case(GRW_As) return context->GRW_As()->getText();
  Case(GRW_Binary) return context->GRW_Binary()->getText();
  Case(GRW_Decimal) return context->GRW_Decimal()->getText();
  Case(GRW_Exists) return context->GRW_Exists()->getText();
  Case(GRW_Hexadecimal) return context->GRW_Hexadecimal()->getText();
  Case(GRW_Forall) return context->GRW_Forall()->getText();
  Case(GRW_Let) return context->GRW_Let()->getText();
  Case(GRW_Match) return context->GRW_Match()->getText();
  Case(GRW_Numeral) return context->GRW_Numeral()->getText();
  Case(GRW_Par) return context->GRW_Par()->getText();
  Case(GRW_String) return context->GRW_String()->getText();
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* std::string */ SMTLIBv2Visitor::visitSimpleSymbol(SMTLIBv2Parser::SimpleSymbolContext *context){
  #define Case(type) if (context->type())
  Case(predefSymbol) return visit(context->predefSymbol());
  Case(UndefinedSymbol) return context->UndefinedSymbol()->getText();
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* std::string */ SMTLIBv2Visitor::visitQuotedSymbol(SMTLIBv2Parser::QuotedSymbolContext *context){
  return context->QuotedSymbol()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitPredefSymbol(SMTLIBv2Parser::PredefSymbolContext *context){
  #define Case(type) if (context->type())
  Case(PS_Not) return context->PS_Not()->getText();
  Case(PS_Bool) return context->PS_Bool()->getText();
  Case(PS_ContinuedExecution) return context->PS_ContinuedExecution()->getText();
  Case(PS_Error) return context->PS_Error()->getText();
  Case(PS_False) return context->PS_False()->getText();
  Case(PS_ImmediateExit) return context->PS_ImmediateExit()->getText();
  Case(PS_Incomplete) return context->PS_Incomplete()->getText();
  Case(PS_Logic) return context->PS_Logic()->getText();
  Case(PS_Memout) return context->PS_Memout()->getText();
  Case(PS_Sat) return context->PS_Sat()->getText();
  Case(PS_Success) return context->PS_Success()->getText();
  Case(PS_Theory) return context->PS_Theory()->getText();
  Case(PS_True) return context->PS_True()->getText();
  Case(PS_Unknown) return context->PS_Unknown()->getText();
  Case(PS_Unsupported) return context->PS_Unsupported()->getText();
  Case(PS_Unsat) return context->PS_Unsat()->getText();
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* std::string */ SMTLIBv2Visitor::visitPredefKeyword(SMTLIBv2Parser::PredefKeywordContext *context){
  #define Case(type) if (context->type())
  Case(PK_AllStatistics) return context->PK_AllStatistics()->getText();
  Case(PK_AssertionStackLevels) return context->PK_AssertionStackLevels()->getText();
  Case(PK_Authors) return context->PK_Authors()->getText();
  Case(PK_Category) return context->PK_Category()->getText();
  Case(PK_Chainable) return context->PK_Chainable()->getText();
  Case(PK_Definition) return context->PK_Definition()->getText();
  Case(PK_DiagnosticOutputChannel) return context->PK_DiagnosticOutputChannel()->getText();
  Case(PK_ErrorBehaviour) return context->PK_ErrorBehaviour()->getText();
  Case(PK_Extension) return context->PK_Extension()->getText();
  Case(PK_Funs) return context->PK_Funs()->getText();
  Case(PK_FunsDescription) return context->PK_FunsDescription()->getText();
  Case(PK_GlobalDeclarations) return context->PK_GlobalDeclarations()->getText();
  Case(PK_InteractiveMode) return context->PK_InteractiveMode()->getText();
  Case(PK_Language) return context->PK_Language()->getText();
  Case(PK_LeftAssoc) return context->PK_LeftAssoc()->getText();
  Case(PK_License) return context->PK_License()->getText();
  Case(PK_Named) return context->PK_Named()->getText();
  Case(PK_Name) return context->PK_Name()->getText();
  Case(PK_Notes) return context->PK_Notes()->getText();
  Case(PK_Pattern) return context->PK_Pattern()->getText();
  Case(PK_PrintSuccess) return context->PK_PrintSuccess()->getText();
  Case(PK_ProduceAssertions) return context->PK_ProduceAssertions()->getText();
  Case(PK_ProduceAssignments) return context->PK_ProduceAssignments()->getText();
  Case(PK_ProduceModels) return context->PK_ProduceModels()->getText();
  Case(PK_ProduceProofs) return context->PK_ProduceProofs()->getText();
  Case(PK_ProduceUnsatAssumptions) return context->PK_ProduceUnsatAssumptions()->getText();
  Case(PK_ProduceUnsatCores) return context->PK_ProduceUnsatCores()->getText();
  Case(PK_RandomSeed) return context->PK_RandomSeed()->getText();
  Case(PK_ReasonUnknown) return context->PK_ReasonUnknown()->getText();
  Case(PK_RegularOutputChannel) return context->PK_RegularOutputChannel()->getText();
  Case(PK_ReproducibleResourceLimit) return context->PK_ReproducibleResourceLimit()->getText();
  Case(PK_RightAssoc) return context->PK_RightAssoc()->getText();
  Case(PK_SmtLibVersion) return context->PK_SmtLibVersion()->getText();
  Case(PK_Sorts) return context->PK_Sorts()->getText();
  Case(PK_SortsDescription) return context->PK_SortsDescription()->getText();
  Case(PK_Source) return context->PK_Source()->getText();
  Case(PK_Status) return context->PK_Status()->getText();
  Case(PK_Theories) return context->PK_Theories()->getText();
  Case(PK_Values) return context->PK_Values()->getText();
  Case(PK_Verbosity) return context->PK_Verbosity()->getText();
  Case(PK_Version) return context->PK_Version()->getText();
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* std::string */ SMTLIBv2Visitor::visitSymbol(SMTLIBv2Parser::SymbolContext *context){
  return visit(context->children[0]);
}

std::any /* uint64_t */ SMTLIBv2Visitor::visitNumeral(SMTLIBv2Parser::NumeralContext *context){
  return std::stoul(context->Numeral()->getText(), nullptr, 10);
}

std::any /* long double */ SMTLIBv2Visitor::visitDecimal(SMTLIBv2Parser::DecimalContext *context){
  return std::stold(context->Decimal()->getText(), nullptr);
}

std::any /* uint64_t */ SMTLIBv2Visitor::visitHexadecimal(SMTLIBv2Parser::HexadecimalContext *context){
  std::string hexString = context->HexDecimal()->getText().substr(2);
  return std::stoul(hexString, nullptr, 16);
}

std::any /* uint64_t */ SMTLIBv2Visitor::visitBinary(SMTLIBv2Parser::BinaryContext *context){
  std::string binString = context->Binary()->getText().substr(2);
  return std::stoul(binString, nullptr, 2);
}

std::any /* std::string */SMTLIBv2Visitor::visitString(SMTLIBv2Parser::StringContext *context){
  return context->String()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitKeyword(SMTLIBv2Parser::KeywordContext *context){
  #define Case(type) if (context->type())
  Case(predefKeyword) return visit(context->predefKeyword());
  Case(simpleSymbol) return ":" + std::any_cast<std::string>(visit(context->simpleSymbol()));
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* SpecConstant */ SMTLIBv2Visitor::visitSpec_constant(SMTLIBv2Parser::Spec_constantContext *context){
  #define Case(type) if (context->type())
  Case(numeral) return SpecConstant{context, SpecConstant::numeral, visit(context->numeral())};
  Case(decimal) return SpecConstant{context, SpecConstant::decimal, visit(context->decimal())};
  Case(hexadecimal) return SpecConstant{context, SpecConstant::hexadecimal, visit(context->hexadecimal())};
  Case(binary) return SpecConstant{context, SpecConstant::binary, visit(context->binary())};
  Case(string) return SpecConstant{context, SpecConstant::string, visit(context->string())};
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* SExpr */ SMTLIBv2Visitor::visitS_expr(SMTLIBv2Parser::S_exprContext *context){
  #define Case(type) if (context->type())
  Case(spec_constant) return SExpr{context, SExpr::spec_constant, visit(context->spec_constant())};
  Case(symbol) return SExpr{context, SExpr::symbol, visit(context->symbol())};
  Case(keyword) return SExpr{context, SExpr::keyword, visit(context->keyword())};
  Case(s_expr().size) {
    std::vector<std::any> ss_expr;
    ss_expr.reserve(context->s_expr().size());
    for (const auto& each : context->s_expr()) ss_expr.push_back(visit(each));
    return SExpr{context, SExpr::ss_expr, ss_expr};
  }
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* Index */ SMTLIBv2Visitor::visitIndex(SMTLIBv2Parser::IndexContext *context){
  #define Case(type) if (context->type())
  Case(numeral) return Index{context, Index::numeral, visit(context->numeral())};
  Case(symbol) return Index{context, Index::symbol, visit(context->symbol())};
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* Identifier */ SMTLIBv2Visitor::visitIdentifier(SMTLIBv2Parser::IdentifierContext *context){
  Identifier identifier{
    context,
    context->symbol()->getText(),
    std::vector<Index>{}
  };
  identifier.index.reserve(context->index().size());
  for (const auto& each : context->index())
    identifier.index.push_back(std::any_cast<Index>(visit(each)));
  return identifier;
}

std::any SMTLIBv2Visitor::visitAttribute_value(SMTLIBv2Parser::Attribute_valueContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitAttribute(SMTLIBv2Parser::AttributeContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any /* ref<Sort> */ SMTLIBv2Visitor::visitSort(SMTLIBv2Parser::SortContext *context){
  #define CaseId(sym, idxs, ss) if (identifier.symbol == sym && identifier.index.size() == idxs && context->sort().size() == ss)
  Identifier identifier = std::any_cast<Identifier>(visit(context->identifier()));
  CaseId("Bool", 0, 0) return SortBool();
  CaseId("Array", 0, 2) {
    ref<Sort> domain = std::any_cast<ref<Sort>>(visit(context->sort(0)));
    ref<Sort> range = std::any_cast<ref<Sort>>(visit(context->sort(1)));
    return SortArray(domain, range);
  }
  CaseId("BitVec", 1, 0) {
    uint64_t m = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
    return SortBitVec(m);
  }
  CaseId("RoundingMode", 0, 0) return SortRoundingMode();
  CaseId("Real", 0, 0) return SortReal();
  CaseId("FloatingPoint", 2, 0) {
    uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
    uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
    return SortFloatingPoint(eb, sb);
  }
  CaseId("Float16", 0, 0) return SortFloat16();
  CaseId("Float32", 0, 0) return SortFloat32();
  CaseId("Float64", 0, 0) return SortFloat64();
  CaseId("Float128", 0, 0) return SortFloat128();

  if (auto res = current_sort_scope->lookup(identifier.context->getText()); res.has_value()) {
    return res.value();
  }

  #undef CaseId
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any /* Identifier */ SMTLIBv2Visitor::visitQual_identifier(SMTLIBv2Parser::Qual_identifierContext *context){
  return visit(context->identifier());
}

std::any /* VarBinding */ SMTLIBv2Visitor::visitVar_binding(SMTLIBv2Parser::Var_bindingContext *context){
  return VarBinding{
    context,
    std::any_cast<std::string>(visit(context->symbol())),
    std::any_cast<ExprRef>(visit(context->term()))
  };
}

std::any /* SortedVar */ SMTLIBv2Visitor::visitSorted_var(SMTLIBv2Parser::Sorted_varContext *context){
  return SortedVar{
    context,
    std::any_cast<std::string>(visit(context->symbol())),
    std::any_cast<ref<Sort>>(visit(context->sort()))
  };
}

std::any SMTLIBv2Visitor::visitPattern(SMTLIBv2Parser::PatternContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitMatch_case(SMTLIBv2Parser::Match_caseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any /* ExprRef */ SMTLIBv2Visitor::visitTerm(SMTLIBv2Parser::TermContext *context){
  #define Case(type) if (context->type())
  Case(spec_constant) {
    #define CaseSpecConstant(type) if (spec_constant.spec_constant_t == SpecConstant::type)
    SpecConstant spec_constant = std::any_cast<SpecConstant>(visit(context->spec_constant()));
    CaseSpecConstant(numeral) throw Unsupported(__PRETTY_FUNCTION__);
    CaseSpecConstant(decimal) throw Unsupported(__PRETTY_FUNCTION__);
    CaseSpecConstant(hexadecimal) {
      uint64_t m = (spec_constant.context->getText().length() - 2) * 4;
      uint64_t val = static_cast<uint64_t>(std::any_cast<uint64_t>(spec_constant.field));
      return exprBuilder->createConstant(val, m);
    }
    CaseSpecConstant(binary) {
      uint64_t m = spec_constant.context->getText().length() - 2;
      uint64_t val = static_cast<uint64_t>(std::any_cast<uint64_t>(spec_constant.field));
      return exprBuilder->createConstant(val, m);
    }
    CaseSpecConstant(string) throw Unsupported(__PRETTY_FUNCTION__);
    #undef CaseSpecConstant
  }
  Case(qual_identifier) {
    #define CaseId(sym, idxs, ts) if (identifier.symbol == sym && identifier.index.size() == idxs && context->term().size() == ts)
    Identifier identifier = std::any_cast<Identifier>(visit(context->qual_identifier()));

    // Common //
    CaseId("true", 0, 0) {
      return exprBuilder->createBool(true);
    }
    CaseId("false", 0, 0) {
      return exprBuilder->createBool(false);
    }
    CaseId("concat", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createConcat(lhs, rhs);
    }
    CaseId("extract", 2, 1) {
      uint64_t hi = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field)); // from
      uint64_t lo = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field)); // end
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createExtract(operand, lo /*index*/, hi - lo + 1 /*bits*/);
    }
    CaseId("zero_extend", 1, 1) {
      uint64_t i = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createZExt(operand, i);
    }
    CaseId("sign_extend", 1, 1) {
      uint64_t i = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createSExt(operand, i);
    }

    // Logic   //
    // LNot    ~
    // LAnd    &
    // LOr     |
    // LXor    LNot(a = b)
    // LNor    LNot(a LOr b)
    // LNand   LNot(a LAnd b)
    CaseId("not", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createLNot(operand);
    }
    CaseId("=>", 0, 2) {  // (lnot lhs) lor rhs
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createLOr(exprBuilder->createLNot(lhs), rhs);
    }
    CaseId("and", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createLAnd(lhs, rhs);
    }
    CaseId("or", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createLOr(lhs, rhs);
    }
    CaseId("xor", 0, 2) { // not (a | b)
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createLNot(exprBuilder->createEqual(lhs, rhs));
    }

    // Compare //
    CaseId("=", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createEqual(lhs, rhs);
    }
    CaseId("distinct", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createDistinct(lhs, rhs);
    }
    CaseId("bvult", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createUlt(lhs, rhs);
    }
    CaseId("bvule", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createUle(lhs, rhs);
    }
    CaseId("bvugt", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createUgt(lhs, rhs);
    }
    CaseId("bvuge", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createUge(lhs, rhs);
    }
    CaseId("bvslt", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSlt(lhs, rhs);
    }
    CaseId("bvsle", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSle(lhs, rhs);
    }
    CaseId("bvsgt", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSgt(lhs, rhs);
    }
    CaseId("bvsge", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSge(lhs, rhs);
    }

    // Special //
    CaseId("ite", 0, 3) {
      ExprRef cond = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef iftrue = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef iffalse = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createIte(cond, iftrue, iffalse);
    }

    // Array //
    CaseId("select", 0, 2) {
      ExprRef ary = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef idx = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSelect(ary, idx);
    }
    CaseId("store", 0, 3) {
      ExprRef ary = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef idx = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef elm = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createStore(ary, idx, elm);
    }

    // Bit //
    CaseId("bvnot", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createNot(operand);
    }
    CaseId("bvand", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createAnd(lhs, rhs);
    }
    CaseId("bvor", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createOr(lhs, rhs);
    }
    CaseId("bvnor", 0, 2) { // not (a or b)
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createNot(exprBuilder->createOr(lhs, rhs));
    }
    CaseId("bvxor", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createXor(lhs, rhs);
    }
    CaseId("bvxnor", 0, 2) {  // not (a ^ b)
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createNot(exprBuilder->createXor(lhs, rhs));
    }
    CaseId("bvnand", 0, 2) {  // not(a & b)
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createNot(exprBuilder->createAnd(lhs, rhs));
    }
    CaseId("bvlshr", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createLShr(lhs, rhs);
    }
    CaseId("bvshl", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createShl(lhs, rhs);
    }
    CaseId("bvashr", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createAShr(lhs, rhs);
    }
    CaseId("bvneg", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createNeg(operand);
    }

    // Arithmetic //
    CaseId("bvadd", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createAdd(lhs, rhs);
    }
    CaseId("bvsub", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSub(lhs, rhs);
    }
    CaseId("bvmul", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createMul(lhs, rhs);
    }
    CaseId("bvudiv", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createUDiv(lhs, rhs);
    }
    CaseId("bvsdiv", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSDiv(lhs, rhs);
    }
    CaseId("bvurem", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createURem(lhs, rhs);
    }
    CaseId("bvsrem", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSRem(lhs, rhs);
    }
    CaseId("bvcomp", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      // compare each bits from lhs and rhs
      ExprRef trueExpr = exprBuilder->createBool(true);
      for (unsigned idx = 0; idx < lhs->bits(); idx ++){
        ExprRef lbit = exprBuilder->createExtract(lhs, idx, 1);
        ExprRef rbit = exprBuilder->createExtract(rhs, idx, 1);
        ExprRef eqExpr = exprBuilder->createEqual(lbit, rbit);
        trueExpr = exprBuilder->createAnd(trueExpr, eqExpr);
      }
      return trueExpr;
    }
    CaseId("bvsmod", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createSMod(lhs, rhs);
    }

    CaseId("repeat", 1, 1) {
      uint64_t i = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      // concat i * op
      ExprRef concatExpr = operand;
      for (unsigned idx = 0; idx < i - 1; idx ++)
        concatExpr = exprBuilder->createConcat(concatExpr, operand);
      return concatExpr;
    }
    CaseId("rotate_left", 1, 1) {
      uint64_t i = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createRol(operand, i);
    }
    CaseId("rotate_right", 1, 1) {
      uint64_t i = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createRor(operand, i);
    }

    CaseId("roundNearestTiesToEven", 0, 0) {
      return exprBuilder->createRNE();
    }
    CaseId("RNE", 0, 0) {
      return exprBuilder->createRNE();
    }
    CaseId("roundNearestTiesToAway", 0, 0) {
      return exprBuilder->createRNA();
    }
    CaseId("RNA", 0, 0) {
      return exprBuilder->createRNA();
    }
    CaseId("roundTowardPositive", 0, 0) {
      return exprBuilder->createRTP();
    }
    CaseId("RTP", 0, 0) {
      return exprBuilder->createRTP();
    }
    CaseId("roundTowardNegative", 0, 0) {
      return exprBuilder->createRTN();
    }
    CaseId("RTN", 0, 0) {
      return exprBuilder->createRTN();
    }
    CaseId("roundTowardZero", 0, 0) {
      return exprBuilder->createRTZ();
    }
    CaseId("RTZ", 0, 0) {
      return exprBuilder->createRTZ();
    }

    CaseId("fp", 0, 3) {
      ExprRef sign = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef exp = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef sig = std::any_cast<ExprRef>(visit(context->term(2)));
      uint64_t bits = sign->bits() + exp->bits() + sig->bits();
      assert ((bits == 32 || bits == 64) && "Only support 32/64 FP !");

      ExprRef bv = exprBuilder->createConcat(sign, exprBuilder->createConcat(exp, sig));
      bool isDouble = bv->bits() == 64;
      return exprBuilder->bitsToFloat(bv, isDouble);
    }
    CaseId("+oo", 2, 0) {
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      uint64_t bits = eb + sb;
      if (bits == 32){
        float inf = std::numeric_limits<float>::infinity();
        llvm::APFloat apf(inf);
        return exprBuilder->createConstantFloat(apf, bits);
      }else if (bits == 64){
        double inf = std::numeric_limits<double>::infinity();
        llvm::APFloat apf(inf);
        return exprBuilder->createConstantFloat(apf, bits);
      }
      assert(false && "Only support 32/64 FP +oo !");
    }
    CaseId("-oo", 2, 0) {
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      uint64_t bits = eb + sb;
      if (bits == 32){
        float neg_inf = - std::numeric_limits<float>::infinity();
        llvm::APFloat apf(neg_inf);
        return exprBuilder->createConstantFloat(apf, bits);
      }else if (bits == 64){
        double neg_inf = - std::numeric_limits<double>::infinity();
        llvm::APFloat apf(neg_inf);
        return exprBuilder->createConstantFloat(apf, bits);
      }
      assert(false && "Only support 32/64 FP -oo !");
    }
    CaseId("+zero", 2, 0) {
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      uint64_t bits = eb + sb;
      if (bits == 32){
        float pos_zero = std::numeric_limits<float>::min();
        llvm::APFloat apf(pos_zero);
        return exprBuilder->createConstantFloat(apf, bits);
      }else if (bits == 64){
        double pos_zero = std::numeric_limits<double>::min();
        llvm::APFloat apf(pos_zero);
        return exprBuilder->createConstantFloat(apf, bits);
      }
      assert(false && "Only support 32/64 FP +zero !");
    }
    CaseId("-zero", 2, 0) {
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      uint64_t bits = eb + sb;
      if (bits == 32){
        float pos_zero = std::numeric_limits<float>::min();
        llvm::APFloat apf(pos_zero);
        return exprBuilder->createConstantFloat(apf, bits);
      }else if (bits == 64){
        double pos_zero = std::numeric_limits<double>::min();
        llvm::APFloat apf(pos_zero);
        return exprBuilder->createConstantFloat(apf, bits);
      }
      assert(false && "Only support 32/64 FP -zero !");
    }
    CaseId("NaN", 2, 0) {
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      uint64_t bits = eb + sb;
      if (bits == 32){
        float nan_value = std::numeric_limits<float>::quiet_NaN();
        llvm::APFloat apf(nan_value);
        return exprBuilder->createConstantFloat(apf, bits);
      }else if (bits == 64){
        double nan_value = std::numeric_limits<double>::quiet_NaN();
        llvm::APFloat apf(nan_value);
        return exprBuilder->createConstantFloat(apf, bits);
      }
      assert(false && "Only support 32/64 FP NaN !");
    }

    CaseId("fp.add", 0, 3) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createFAdd(lhs, rhs);
    }
    CaseId("fp.sub", 0, 3) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createFSub(lhs, rhs);
    }
    CaseId("fp.mul", 0, 3) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createFMul(lhs, rhs);
    }
    CaseId("fp.div", 0, 3) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(2)));
      return exprBuilder->createFDiv(lhs, rhs);
    }
    CaseId("fp.rem", 0, 2) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFRem(lhs, rhs);
    }
    CaseId("fp.fma", 0, 4) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef x = std::any_cast<ExprRef>(visit(context->term(1)));
      ExprRef y = std::any_cast<ExprRef>(visit(context->term(2)));
      ExprRef z = std::any_cast<ExprRef>(visit(context->term(3)));
      return exprBuilder->createFAdd(exprBuilder->createFMul(x, y), z);
    }
    CaseId("fp.sqrt", 0, 2) {
      //ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFSqrt(operand);
    }
    CaseId("fp.min", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFMin(lhs, rhs);
    }
    CaseId("fp.max", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFMax(lhs, rhs);
    }
    CaseId("fp.abs", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFAbs(operand);
    }
    CaseId("fp.neg", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFNeg(operand);
    }

    CaseId("fp.leq", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFOle(lhs, rhs);
    }
    CaseId("fp.lt", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFOlt(lhs, rhs);
    }
    CaseId("fp.geq", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFOge(lhs, rhs);
    }
    CaseId("fp.gt", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFOgt(lhs, rhs);
    }
    CaseId("fp.eq", 0, 2) {
      ExprRef lhs = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef rhs = std::any_cast<ExprRef>(visit(context->term(1)));
      return exprBuilder->createFOeq(lhs, rhs);
    }

    // check
    CaseId("fp.isNormal", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsNorm(operand);
    }
    CaseId("fp.isSubnormal", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsSubNorm(operand);
    }
    CaseId("fp.isZero", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsZero(operand);
    }
    CaseId("fp.isInfinite", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsInf(operand);
    }
    CaseId("fp.isNaN", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsNan(operand);
    }
    CaseId("fp.isNegative", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsNeg(operand);
    }
    CaseId("fp.isPositive", 0, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      return exprBuilder->createFPIsPos(operand);
    }
    CaseId("to_fp", 2, 1) {
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(0)));
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      return exprBuilder->bitsToFloat(operand, eb + sb);
    }
    CaseId("to_fp", 2, 2) {
      ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(1)));
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      if (isFloatingPoint(operand->getSort())) {
        return exprBuilder->floatToFloat(operand, eb + sb);
      }
      if (isReal(operand->getSort())) {
        throw Unsupported(__PRETTY_FUNCTION__);
      }
      if (isBitVec(operand->getSort())) {
        bool isDouble = eb + sb == 64;
        return exprBuilder->intToFloat(operand, isDouble, true);
      }
    }
    CaseId("to_fp_unsigned", 2, 2) {
      ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(1)));
      uint64_t eb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      uint64_t sb = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[1].field));
      bool isDouble = eb + sb == 64;
      return exprBuilder->intToFloat(operand, isDouble, false);
    }
    CaseId("fp.to_ubv", 1, 2) {
      ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(1)));
      uint64_t m = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      return exprBuilder->floatToUnsignInt(operand, m);
    }
    CaseId("fp.to_sbv", 1, 2) {
      ExprRef rm = std::any_cast<ExprRef>(visit(context->term(0)));
      ExprRef operand = std::any_cast<ExprRef>(visit(context->term(1)));
      uint64_t m = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      return exprBuilder->floatToSignInt(operand, m);
    }
    CaseId("fp.to_real", 0, 2) {
      throw Unsupported(__PRETTY_FUNCTION__);
    }
    std::string bv = "bv";
    if (std::equal(bv.begin(), bv.end(), identifier.symbol.begin()) && identifier.index.size() == 1) {
      uint64_t X = static_cast<uint64_t>(std::stoul(identifier.symbol.substr(2)));
      uint64_t n = static_cast<uint64_t>(std::any_cast<uint64_t>(identifier.index[0].field));
      return exprBuilder->createConstant(X, n);
    }

    // fix by zgf
    std::string varName = identifier.context->getText();
    // firstly, check if variable
    auto varRes = global_var.find(varName);
    if (varRes != global_var.end())
      return varRes->second;

    if (auto res = current_func_scope->lookup(varName); res.has_value()) {
      ref<Expr> func = res.value();
      std::vector<ExprRef> parameters;
      parameters.reserve(context->term().size());
      for (const auto& each : context->term())
        parameters.push_back(std::any_cast<ExprRef>(each));

      // must be func decl
      return exprBuilder->createApp(func, parameters);
    }

    if (auto res = current_expr_scope->lookup(varName); res.has_value()) {
      return res.value();
    }
    #undef CaseId
  }
  Case(GRW_Let) {
    enterScope();
    for (const auto& each : context->var_binding()) {
      VarBinding var_binding = std::any_cast<VarBinding>(visit(each));
      if (!current_expr_scope->insert(var_binding.symbol, var_binding.expr))
        throw DefineExistingSymbol(__PRETTY_FUNCTION__);
    }
    ExprRef expr = std::any_cast<ExprRef>(visit(context->term(0)));
    leaveScope();
    return expr;
  }
  /*
  Case(GRW_Forall) {
    std::vector<ExprRef> parameters;
    parameters.reserve(context->sorted_var().size());
    for (const auto& each : context->sorted_var()) {
      SortedVar tmp = std::any_cast<SortedVar>(visit(each));
      parameters.push_back(Par(tmp.sort));
    }
    return Forall(parameters, std::any_cast<ExprRef>(visit(context->term(0))));
  }
  Case(GRW_Exists) {
    std::vector<ExprRef> parameters;
    parameters.reserve(context->sorted_var().size());
    for (const auto& each : context->sorted_var()) {
      SortedVar tmp = std::any_cast<SortedVar>(visit(each));
      parameters.push_back(Par(tmp.sort));
    }
    return Exists(parameters, std::any_cast<ExprRef>(visit(context->term(0))));
  }*/
  Case(GRW_Forall) throw Unsupported(__PRETTY_FUNCTION__);
  Case(GRW_Exists) throw Unsupported(__PRETTY_FUNCTION__);
  Case(GRW_Match) throw Unsupported(__PRETTY_FUNCTION__);

  Case(GRW_Match) throw Unsupported(__PRETTY_FUNCTION__);
  Case(GRW_Exclamation) throw Unsupported(__PRETTY_FUNCTION__);
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitSort_symbol_decl(SMTLIBv2Parser::Sort_symbol_declContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitMeta_spec_constant(SMTLIBv2Parser::Meta_spec_constantContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitFun_symbol_decl(SMTLIBv2Parser::Fun_symbol_declContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitPar_fun_symbol_decl(SMTLIBv2Parser::Par_fun_symbol_declContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitTheory_attribute(SMTLIBv2Parser::Theory_attributeContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitTheory_decl(SMTLIBv2Parser::Theory_declContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitLogic_attribue(SMTLIBv2Parser::Logic_attribueContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitLogic(SMTLIBv2Parser::LogicContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitSort_dec(SMTLIBv2Parser::Sort_decContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitSelector_dec(SMTLIBv2Parser::Selector_decContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitConstructor_dec(SMTLIBv2Parser::Constructor_decContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitDatatype_dec(SMTLIBv2Parser::Datatype_decContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitFunction_dec(SMTLIBv2Parser::Function_decContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitFunction_def(SMTLIBv2Parser::Function_defContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitProp_literal(SMTLIBv2Parser::Prop_literalContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitScript(SMTLIBv2Parser::ScriptContext *context){
  for (auto each : context->command()) visit(each);
  return std::any{};
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_assert(SMTLIBv2Parser::Cmd_assertContext *context){
  return context->CMD_Assert()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_checkSat(SMTLIBv2Parser::Cmd_checkSatContext *context){
  return context->CMD_CheckSat()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_checkSatAssuming(SMTLIBv2Parser::Cmd_checkSatAssumingContext *context){
  return context->CMD_CheckSatAssuming()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_declareConst(SMTLIBv2Parser::Cmd_declareConstContext *context){
  return context->CMD_DeclareConst()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_declareDatatype(SMTLIBv2Parser::Cmd_declareDatatypeContext *context){
  return context->CMD_DeclareDatatype()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_declareDatatypes(SMTLIBv2Parser::Cmd_declareDatatypesContext *context){
  return context->CMD_DeclareDatatypes()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_declareFun(SMTLIBv2Parser::Cmd_declareFunContext *context){
  return context->CMD_DeclareFun()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_declareSort(SMTLIBv2Parser::Cmd_declareSortContext *context){
  return context->CMD_DeclareSort()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_defineFun(SMTLIBv2Parser::Cmd_defineFunContext *context){
  return context->CMD_DefineFun()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_defineFunRec(SMTLIBv2Parser::Cmd_defineFunRecContext *context){
  return context->CMD_DefineFunRec()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_defineFunsRec(SMTLIBv2Parser::Cmd_defineFunsRecContext *context){
  return context->CMD_DefineFunsRec()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_defineSort(SMTLIBv2Parser::Cmd_defineSortContext *context){
  return context->CMD_DefineSort()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_echo(SMTLIBv2Parser::Cmd_echoContext *context){
  return context->CMD_Echo()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_exit(SMTLIBv2Parser::Cmd_exitContext *context){
  return context->CMD_Exit()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getAssertions(SMTLIBv2Parser::Cmd_getAssertionsContext *context){
  return context->CMD_GetAssertions()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getAssignment(SMTLIBv2Parser::Cmd_getAssignmentContext *context){
  return context->CMD_GetAssignment()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getInfo(SMTLIBv2Parser::Cmd_getInfoContext *context){
  return context->CMD_GetInfo()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getModel(SMTLIBv2Parser::Cmd_getModelContext *context){
  return context->CMD_GetModel()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getOption(SMTLIBv2Parser::Cmd_getOptionContext *context){
  return context->CMD_GetOption()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getProof(SMTLIBv2Parser::Cmd_getProofContext *context){
  return context->CMD_GetProof()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getUnsatAssumptions(SMTLIBv2Parser::Cmd_getUnsatAssumptionsContext *context){
  return context->CMD_GetUnsatAssumptions()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getUnsatCore(SMTLIBv2Parser::Cmd_getUnsatCoreContext *context){
  return context->CMD_GetUnsatCore()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_getValue(SMTLIBv2Parser::Cmd_getValueContext *context){
  return context->CMD_GetValue()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_pop(SMTLIBv2Parser::Cmd_popContext *context){
  return context->CMD_Pop()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_push(SMTLIBv2Parser::Cmd_pushContext *context){
  return context->CMD_Push()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_reset(SMTLIBv2Parser::Cmd_resetContext *context){
  return context->CMD_Reset()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_resetAssertions(SMTLIBv2Parser::Cmd_resetAssertionsContext *context){
  return context->CMD_ResetAssertions()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_setInfo(SMTLIBv2Parser::Cmd_setInfoContext *context){
  return context->CMD_SetInfo()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_setLogic(SMTLIBv2Parser::Cmd_setLogicContext *context){
  return context->CMD_SetLogic()->getText();
}

std::any /* std::string */ SMTLIBv2Visitor::visitCmd_setOption(SMTLIBv2Parser::Cmd_setOptionContext *context){
  return context->CMD_SetOption()->getText();
}

std::any /* void */ SMTLIBv2Visitor::visitCommand(SMTLIBv2Parser::CommandContext *context){
  #define Case(type) if (context->type())
  Case(cmd_assert) {
    ExprRef expr = std::any_cast<ExprRef>(visit(context->term(0)));
    solver->getEngine()->Assert(expr);
    return std::any{};
  }
  Case(cmd_checkSat) {
    ref<Response> satRes = solver->getEngine()->CheckSat();
    std::cout << *satRes << std::endl;
    if (satRes->isSat()){
      solver->getEngine()->getAssignFromModel();
    }
    return std::any{};
  }
  Case(cmd_checkSatAssuming) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_declareConst) {
    std::string symbol = std::any_cast<std::string>(visit(context->symbol(0)));
    ref<Sort> sort = std::any_cast<ref<Sort>>(visit(context->sort(0)));
    if (!current_sort_scope->insert(symbol, sort))
      throw DefineExistingSymbol(__PRETTY_FUNCTION__);
    return std::any{};
  }
  Case(cmd_declareDatatype) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_declareDatatypes) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_declareFun) {
    std::string symbol = std::any_cast<std::string>(visit(context->symbol(0)));
    std::vector<ExprRef> parameters;
    parameters.reserve(context->sort().size() - 1);
    for (const auto& each : context->sort()) if (each != context->sort().back()){
      SortRef eachSort = std::any_cast<ref<Sort>>(visit(each));
      parameters.push_back(exprBuilder->createPara(eachSort));
    }
    // func sort
    SortRef funcSort = std::any_cast<ref<Sort>>(visit(context->sort().back()));

    // note by zgf
    if (parameters.empty()){
      // must be declare variable
      UINT32 bits = funcSort->getParameter<uint64_t>(0);
      UINT32 bytes = bits / 8;
      ExprRef varByte = exprBuilder->createReadWithName(0, symbol);
      { // add by chk
        auto res = solver->getEngine()->ConsVar().insert(std::make_pair(symbol + std::to_string(0), varByte));
        if (!res.second)
          throw DefineExistingSymbol(__PRETTY_FUNCTION__);
      }
      // modify by chk
      for (unsigned idx = 1; idx < bytes; idx ++) {
        auto tByte = exprBuilder->createReadWithName(idx, symbol);
        varByte = exprBuilder->createConcat(varByte, tByte);
        // add by chk
        auto res = solver->getEngine()->ConsVar().insert(std::make_pair(symbol + std::to_string(idx), tByte));
        if (!res.second)
          throw DefineExistingSymbol(__PRETTY_FUNCTION__);
      }

      auto res = global_var.insert(std::make_pair(symbol, varByte));
      if (!res.second)
        throw DefineExistingSymbol(__PRETTY_FUNCTION__);
      
      // add by chk
      // for (unsigned idx = 0; idx < bytes; idx ++) {
      //   ExprRef varByte = exprBuilder->createReadWithName(idx, symbol);
      //   auto res = solver->getEngine()->ConsVar().insert(std::make_pair(symbol + std::to_string(idx), varByte));
      //   if (!res.second)
      //     throw DefineExistingSymbol(__PRETTY_FUNCTION__);
      // }
    }else{
      // declare function
      ExprRef func = exprBuilder->createFunc(funcSort, parameters);
      if (!current_func_scope->insert(symbol, func))
        throw DefineExistingSymbol(__PRETTY_FUNCTION__);
    }
    return std::any{};
  }
  Case(cmd_declareSort) {
    std::string symbol = std::any_cast<std::string>(visit(context->symbol(0)));
    size_t arity = static_cast<size_t>(std::any_cast<uint64_t>(visit(context->numeral())));
    if (!current_sort_scope->insert(symbol, SortUninterpreted(arity)))
      throw DefineExistingSymbol(__PRETTY_FUNCTION__);
    return std::any{};
  }
  Case(cmd_defineFun) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_defineFunRec) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_defineFunsRec) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_defineSort) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_echo) {
    std::cout << context->string()->getText() << std::endl;
    return std::any{};
  }
  Case(cmd_exit) {
    return std::any{};
  }
  Case(cmd_getAssertions) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getAssignment) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getInfo) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getModel) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getOption) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getProof) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getUnsatAssumptions) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getUnsatCore) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_getValue) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_pop) throw Unsupported(__PRETTY_FUNCTION__);
  // {
  //   uint64_t n = std::any_cast<uint64_t>(visit(context->numeral()));
  //   while(n--) solver->Pop();
  //   return std::any{};
  // }
  Case(cmd_push)throw Unsupported(__PRETTY_FUNCTION__);
  // {
  //   uint64_t n = std::any_cast<uint64_t>(visit(context->numeral()));
  //   while(n--) solver->Push();
  //   return std::any{};
  // }
  Case(cmd_reset) throw Unsupported(__PRETTY_FUNCTION__);
  Case(cmd_resetAssertions) {
    solver->ResetAssertions();
    return std::any{};
  }
  Case(cmd_setInfo) {
    return std::any{};
  }
  Case(cmd_setLogic) {
    return std::any{};
  }
  Case(cmd_setOption) throw Unsupported(__PRETTY_FUNCTION__);
  #undef Case
  throw Unreachable(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitB_value(SMTLIBv2Parser::B_valueContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitOption(SMTLIBv2Parser::OptionContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitInfo_flag(SMTLIBv2Parser::Info_flagContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitError_behaviour(SMTLIBv2Parser::Error_behaviourContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitReason_unknown(SMTLIBv2Parser::Reason_unknownContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitModel_response(SMTLIBv2Parser::Model_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitInfo_response(SMTLIBv2Parser::Info_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitValuation_pair(SMTLIBv2Parser::Valuation_pairContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitT_valuation_pair(SMTLIBv2Parser::T_valuation_pairContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitCheck_sat_response(SMTLIBv2Parser::Check_sat_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitEcho_response(SMTLIBv2Parser::Echo_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_assertions_response(SMTLIBv2Parser::Get_assertions_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_assignment_response(SMTLIBv2Parser::Get_assignment_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_info_response(SMTLIBv2Parser::Get_info_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_model_response(SMTLIBv2Parser::Get_model_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_option_response(SMTLIBv2Parser::Get_option_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_proof_response(SMTLIBv2Parser::Get_proof_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_unsat_assump_response(SMTLIBv2Parser::Get_unsat_assump_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_unsat_core_response(SMTLIBv2Parser::Get_unsat_core_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGet_value_response(SMTLIBv2Parser::Get_value_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitSpecific_success_response(SMTLIBv2Parser::Specific_success_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

std::any SMTLIBv2Visitor::visitGeneral_response(SMTLIBv2Parser::General_responseContext *context){
  throw Unsupported(__PRETTY_FUNCTION__);
}

}
