#pragma once

#include <any>
#include <memory>
#include "Support/Ref.h"

namespace MBSSL {

// /// @brief cast a ref of a base class to a ref of a drive class
// /// @tparam <Drive_t> a drive class
// /// @tparam <Base_t> a base class
// /// @param <operand> ref of a base class
// /// @return whether <Base_t> "is a" <Drive_t>
// template <class Drive_t, class Base_t>
// [[nodiscard]] inline ref<Drive_t> isa(const ref<Base_t>& operand) {
//   return std::dynamic_pointer_cast<Drive_t>(operand);
// }

// /// @brief cast a ref of a drive class to a ref of a base class
// /// @tparam <Base_t> a base class
// /// @tparam <Drive_t> a drive class
// /// @param <operand> ref of a drive class
// /// @return the "class of" <Drive_t> is <Base_t>
// /// @note <Base_t> should be the base class of <Drive_t>
// template <class Base_t, class Drive_t>
// [[nodiscard]] inline ref<Base_t> classof(const ref<Drive_t>& operand) {
//   return std::reinterpret_pointer_cast<Base_t>(operand);
// }

/// @brief get value from operand with <Value_t> type
/// @tparam <Value_t> type expected to get from operand
/// @param <operand> any value in std::any
/// @return a value with <Value_t> type or throw
template <class Value_t>
inline Value_t get(const std::any& operand) {
  return std::any_cast<Value_t>(operand);
}
template <class Value_t>
inline Value_t get(std::any& operand) {
  return std::any_cast<Value_t>(operand);
}
template <class Value_t>
inline Value_t get(std::any&& operand) {
  return std::any_cast<Value_t>(operand);
}

/// @brief get raw pointer to operand with <Value_t>* type
/// @tparam <Value_t> type expected to get from operand
/// @param <operand> any value in std::any
/// @return a raw pointer with <Value_t>* type or null
template <class Value_t>
[[nodiscard]] inline const Value_t* get(const std::any* operand) noexcept {
  return std::any_cast<Value_t>(operand);
}
template <class Value_t>
[[nodiscard]] inline Value_t* get(std::any* operand) noexcept {
  return std::any_cast<Value_t>(operand);
}
}  // namespace MBSSL
