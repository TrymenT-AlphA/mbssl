#pragma once

#include <cassert>
#include <exception>
#include <string>
#include <string_view>

namespace MBSSL {

class Exception : public std::exception {
 protected:
  std::string_view err;

 public:
  [[nodiscard]] explicit Exception(const std::string_view &err) : err(err) {}
  virtual ~Exception() = default;
  virtual const char *what() const noexcept override { return err.cbegin(); }
};

#define RegisterException(name)                            \
  class name : public Exception {                          \
   public:                                                 \
    [[nodiscard]] explicit name(const std::string &err)    \
        : Exception(std::string(#name) + ":\n\t" + err) {} \
  };

RegisterException(NotImpletement);
RegisterException(Unreachable);
RegisterException(Unsupported);
RegisterException(UseOfUndefinedSymbol);
RegisterException(DefineExistingSymbol);

}  // namespace MBSSL
