#pragma once

#include <memory>

namespace MBSSL {

/// @brief shared pointer
template <class T>
using ref = std::shared_ptr<T>;

/// @brief construct a shared pointer
template <class T, class... Args>
inline ref<T> mkref(Args&&... args) {
  return std::make_shared<T>(args...);
}

}  // namespace MBSSL
