#pragma once

#include <optional>
#include <string>
#include <vector>
#include <any>
#include <cassert>

#include "Support/Ref.h"
#include "Support/Casting.h"

namespace MBSSL {

  enum class Sort_t {
    Uninterpreted,
    Bool,
    Array,
    BitVec,
    RoundingMode,
    Real,
    FloatingPoint,
  };

  class Sort {
  protected:
    Sort_t sort_t;
    std::optional<std::string> symbol;
    size_t arity;
    std::vector<std::any> parameters;

  public:
    [[nodiscard]] explicit Sort(
        Sort_t sort_t,
        size_t arity
    ) noexcept;
    [[nodiscard]] explicit Sort(
        Sort_t sort_t,
        const std::vector<std::any>& parameters
    ) noexcept;
    [[nodiscard]] explicit Sort(
        Sort_t sort_t,
        const std::string& symbol,
        size_t arity
    ) noexcept;
    [[nodiscard]] explicit Sort(
        Sort_t sort_t,
        const std::string& symbol,
        const std::vector<std::any>& parameters
    ) noexcept;

    [[nodiscard]] Sort_t getSortT() const noexcept;

    [[nodiscard]] bool hasSymbol() const noexcept;
    [[nodiscard]] const std::string& getSymbol() const;

    [[nodiscard]] size_t getArity() const noexcept;
    [[nodiscard]] const std::vector<std::any>& getParameters() const noexcept;
    template<class T> [[nodiscard]] T getParameter(size_t idx) const {
      assert(idx < arity);
      return get<T>(parameters[idx]);
    }

    friend bool operator==(const Sort& lhs, const Sort& rhs) noexcept;
  };

/////// Boolean //////
  [[nodiscard]] bool isBool(ref<Sort> sort) noexcept;
  [[nodiscard]] ref<Sort> SortBool() noexcept;

/////// Array ///////
  [[nodiscard]] bool isArray(ref<Sort> sort) noexcept;
  [[nodiscard]] ref<Sort> getDomain(ref<Sort> sort);
  [[nodiscard]] ref<Sort> getRange(ref<Sort> sort);
  [[nodiscard]] ref<Sort> SortArray(ref<Sort> domain, ref<Sort> range) noexcept;

/////// BV ///////
  [[nodiscard]] bool isBitVec(ref<Sort> sort) noexcept;
  [[nodiscard]] uint64_t getWidth(ref<Sort> sort);
  [[nodiscard]] ref<Sort> SortBitVec(uint64_t width) noexcept;

/////// Floating Point ///////
  [[nodiscard]] bool isRoundingMode(ref<Sort> sort) noexcept;
  [[nodiscard]] ref<Sort> SortRoundingMode() noexcept;

  [[nodiscard]] bool isReal(ref<Sort> sort) noexcept;
  [[nodiscard]] ref<Sort> SortReal() noexcept;

  [[nodiscard]] bool isFloatingPoint(ref<Sort> sort) noexcept;
  [[nodiscard]] uint64_t geteb(ref<Sort> sort);
  [[nodiscard]] uint64_t getsb(ref<Sort> sort);
  [[nodiscard]] ref<Sort> SortFloatingPoint(uint64_t eb, uint64_t sb) noexcept;
  [[nodiscard]] ref<Sort> SortFloatingPoint(uint64_t bits) noexcept;
  [[nodiscard]] ref<Sort> SortFloat16() noexcept;
  [[nodiscard]] ref<Sort> SortFloat32() noexcept;
  [[nodiscard]] ref<Sort> SortFloat64() noexcept;
  [[nodiscard]] ref<Sort> SortFloat128() noexcept;

/////// UF ///////
  [[nodiscard]] bool isUninterpreted(ref<Sort> sort) noexcept;
  [[nodiscard]] ref<Sort> SortUninterpreted(size_t arity) noexcept;
  [[nodiscard]] ref<Sort> SortUninterpreted(const std::string& symbol, size_t arity) noexcept;



}  // namespace MBSSL
