#ifndef QSYM_EXPR_BUILDER_H_
#define QSYM_EXPR_BUILDER_H_

#include <list>

#include "ExprCache.h"
#include "logging.h"

namespace qsym {

  class ExprBuilder {
  public:
    ExprBuilder();
    void setNext(ExprBuilder* next);

    // {BEGIN:FUNC}
    virtual ExprRef createBool(bool b);
    virtual ExprRef createConstant(ADDRINT value, UINT32 bits);
    virtual ExprRef createConstant(llvm::APInt value, UINT32 bits);
    virtual ExprRef createConstantFloat(llvm::APFloat value, UINT32 bits);
    virtual ExprRef createRead(ADDRINT off);
    virtual ExprRef createConcat(ExprRef l, ExprRef r);
    virtual ExprRef createExtract(ExprRef e, UINT32 index, UINT32 bits);
    virtual ExprRef createZExt(ExprRef e, UINT32 bits);
    virtual ExprRef createSExt(ExprRef e, UINT32 bits);
    virtual ExprRef createRol(ExprRef e, UINT32 bits);
    virtual ExprRef createRor(ExprRef e, UINT32 bits);
    virtual ExprRef createAdd(ExprRef l, ExprRef r);
    virtual ExprRef createSub(ExprRef l, ExprRef r);
    virtual ExprRef createMul(ExprRef l, ExprRef r);
    virtual ExprRef createUDiv(ExprRef l, ExprRef r);
    virtual ExprRef createSDiv(ExprRef l, ExprRef r);
    virtual ExprRef createURem(ExprRef l, ExprRef r);
    virtual ExprRef createSRem(ExprRef l, ExprRef r);
    virtual ExprRef createNeg(ExprRef e);
    virtual ExprRef createSMod(ExprRef l, ExprRef r);
    virtual ExprRef createNot(ExprRef e);
    virtual ExprRef createAnd(ExprRef l, ExprRef r);
    virtual ExprRef createOr(ExprRef l, ExprRef r);
    virtual ExprRef createXor(ExprRef l, ExprRef r);
    virtual ExprRef createShl(ExprRef l, ExprRef r);
    virtual ExprRef createLShr(ExprRef l, ExprRef r);
    virtual ExprRef createAShr(ExprRef l, ExprRef r);
    virtual ExprRef createEqual(ExprRef l, ExprRef r);
    virtual ExprRef createDistinct(ExprRef l, ExprRef r);
    virtual ExprRef createUlt(ExprRef l, ExprRef r);
    virtual ExprRef createUle(ExprRef l, ExprRef r);
    virtual ExprRef createUgt(ExprRef l, ExprRef r);
    virtual ExprRef createUge(ExprRef l, ExprRef r);
    virtual ExprRef createSlt(ExprRef l, ExprRef r);
    virtual ExprRef createSle(ExprRef l, ExprRef r);
    virtual ExprRef createSgt(ExprRef l, ExprRef r);
    virtual ExprRef createSge(ExprRef l, ExprRef r);
    virtual ExprRef createLOr(ExprRef l, ExprRef r);
    virtual ExprRef createFAdd(ExprRef l, ExprRef r);
    virtual ExprRef createFSub(ExprRef l, ExprRef r);
    virtual ExprRef createFMul(ExprRef l, ExprRef r);
    virtual ExprRef createFDiv(ExprRef l, ExprRef r);
    virtual ExprRef createFRem(ExprRef l, ExprRef r);
    virtual ExprRef createFMax(ExprRef l, ExprRef r);
    virtual ExprRef createFMin(ExprRef l, ExprRef r);
    virtual ExprRef createFAbs(ExprRef e);
    virtual ExprRef createFNeg(ExprRef e);
    virtual ExprRef createFSqrt(ExprRef e);
    virtual ExprRef createFOgt(ExprRef l, ExprRef r);
    virtual ExprRef createFOge(ExprRef l, ExprRef r);
    virtual ExprRef createFOlt(ExprRef l, ExprRef r);
    virtual ExprRef createFOle(ExprRef l, ExprRef r);
    virtual ExprRef createFOeq(ExprRef l, ExprRef r);
    virtual ExprRef createFOne(ExprRef l, ExprRef r);
    virtual ExprRef createFOrd(ExprRef l, ExprRef r);
    virtual ExprRef createFUno(ExprRef l, ExprRef r);
    virtual ExprRef createFUgt(ExprRef l, ExprRef r);
    virtual ExprRef createFUge(ExprRef l, ExprRef r);
    virtual ExprRef createFUlt(ExprRef l, ExprRef r);
    virtual ExprRef createFUle(ExprRef l, ExprRef r);
    virtual ExprRef createFUeq(ExprRef l, ExprRef r);
    virtual ExprRef createFUne(ExprRef l, ExprRef r);
    virtual ExprRef createLAnd(ExprRef l, ExprRef r);
    virtual ExprRef createFPIsZero(ExprRef e);
    virtual ExprRef createFPIsNorm(ExprRef e);
    virtual ExprRef createFPIsSubNorm(ExprRef e);
    virtual ExprRef createFPIsInf(ExprRef e);
    virtual ExprRef createFPIsNan(ExprRef e);
    virtual ExprRef createFPIsNeg(ExprRef e);
    virtual ExprRef createFPIsPos(ExprRef e);
    virtual ExprRef createLNot(ExprRef e);
    virtual ExprRef createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false);
    virtual ExprRef createArray(std::string name);
    virtual ExprRef createStore(ExprRef array, ExprRef index, ExprRef newV);
    virtual ExprRef createSelect(ExprRef array, ExprRef index);
    virtual ExprRef createPara(SortRef sort) ;
    virtual ExprRef createFunc(SortRef sort, const std::vector<ExprRef>& para_sort);
    virtual ExprRef createApp(ExprRef func, const std::vector<ExprRef> &para_expr);
    // {END:FUNC}

    // floating-point rounding mode
    ExprRef createRNE();
    ExprRef createRNA();
    ExprRef createRTP();
    ExprRef createRTN();
    ExprRef createRTZ();

    // utility functions
    ExprRef createTrue();
    ExprRef createFalse();
    ExprRef createMsb(ExprRef);
    ExprRef createLsb(ExprRef);

    ExprRef bitToBool(ExprRef e);
    ExprRef boolToBit(ExprRef e, UINT32 bits);
    ExprRef createBinaryExpr(Kind kind, ExprRef l, ExprRef r);
    ExprRef createUnaryExpr(Kind kind, ExprRef e);
    ExprRef createExtExpr(Kind kind, ExprRef e, uint32_t bits);
    ExprRef createConcat(std::list<ExprRef> exprs);
    ExprRef createLAnd(std::list<ExprRef> exprs);
    ExprRef createTrunc(ExprRef e, UINT32 bits);
    ExprRef createReadWithName(ADDRINT off,std::string var_name);
    ExprRef intToFloat(ExprRef e, int is_double, int is_signed);
    ExprRef floatToFloat(ExprRef e, int to_double);
    ExprRef bitsToFloat(ExprRef e, int to_double);
    ExprRef floatToBits(ExprRef e);
    ExprRef floatToSignInt(ExprRef e, uint8_t bits);
    ExprRef floatToUnsignInt(ExprRef e, uint8_t bits);
    ExprRef simplifyBVToFP(ExprRef l);

  protected:
    ExprBuilder* next_;
  };

  class BaseExprBuilder : public ExprBuilder {
  public:
    ExprRef createExtract(ExprRef e, UINT32 index, UINT32 bits) override;
    ExprRef createRead(ADDRINT off) override;
    ExprRef createArray(std::string name) override;

    // {BEGIN:BASE}
    ExprRef createBool(bool b) override;
    ExprRef createConstant(ADDRINT value, UINT32 bits) override;
    ExprRef createConstant(llvm::APInt value, UINT32 bits) override;
    ExprRef createConstantFloat(llvm::APFloat value, UINT32 bits) override;
    ExprRef createConcat(ExprRef l, ExprRef r) override;
    ExprRef createZExt(ExprRef e, UINT32 bits) override;
    ExprRef createSExt(ExprRef e, UINT32 bits) override;
    ExprRef createRol(ExprRef e, UINT32 bits) override;
    ExprRef createRor(ExprRef e, UINT32 bits) override;
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createSub(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef l, ExprRef r) override;
    ExprRef createUDiv(ExprRef l, ExprRef r) override;
    ExprRef createSDiv(ExprRef l, ExprRef r) override;
    ExprRef createURem(ExprRef l, ExprRef r) override;
    ExprRef createSRem(ExprRef l, ExprRef r) override;
    ExprRef createNeg(ExprRef e) override;
    ExprRef createSMod(ExprRef l, ExprRef r) override;
    ExprRef createNot(ExprRef e) override;
    ExprRef createAnd(ExprRef l, ExprRef r) override;
    ExprRef createOr(ExprRef l, ExprRef r) override;
    ExprRef createXor(ExprRef l, ExprRef r) override;
    ExprRef createShl(ExprRef l, ExprRef r) override;
    ExprRef createLShr(ExprRef l, ExprRef r) override;
    ExprRef createAShr(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createDistinct(ExprRef l, ExprRef r) override;
    ExprRef createUlt(ExprRef l, ExprRef r) override;
    ExprRef createUle(ExprRef l, ExprRef r) override;
    ExprRef createUgt(ExprRef l, ExprRef r) override;
    ExprRef createUge(ExprRef l, ExprRef r) override;
    ExprRef createSlt(ExprRef l, ExprRef r) override;
    ExprRef createSle(ExprRef l, ExprRef r) override;
    ExprRef createSgt(ExprRef l, ExprRef r) override;
    ExprRef createSge(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFSub(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;
    ExprRef createFDiv(ExprRef l, ExprRef r) override;
    ExprRef createFRem(ExprRef l, ExprRef r) override;
    ExprRef createFMax(ExprRef l, ExprRef r) override;
    ExprRef createFMin(ExprRef l, ExprRef r) override;
    ExprRef createFAbs(ExprRef e) override;
    ExprRef createFNeg(ExprRef e) override;
    ExprRef createFSqrt(ExprRef e) override;
    ExprRef createFOgt(ExprRef l, ExprRef r) override;
    ExprRef createFOge(ExprRef l, ExprRef r) override;
    ExprRef createFOlt(ExprRef l, ExprRef r) override;
    ExprRef createFOle(ExprRef l, ExprRef r) override;
    ExprRef createFOeq(ExprRef l, ExprRef r) override;
    ExprRef createFOne(ExprRef l, ExprRef r) override;
    ExprRef createFOrd(ExprRef l, ExprRef r) override;
    ExprRef createFUno(ExprRef l, ExprRef r) override;
    ExprRef createFUgt(ExprRef l, ExprRef r) override;
    ExprRef createFUge(ExprRef l, ExprRef r) override;
    ExprRef createFUlt(ExprRef l, ExprRef r) override;
    ExprRef createFUle(ExprRef l, ExprRef r) override;
    ExprRef createFUeq(ExprRef l, ExprRef r) override;
    ExprRef createFUne(ExprRef l, ExprRef r) override;
    ExprRef createFPIsZero(ExprRef e) override;
    ExprRef createFPIsNorm(ExprRef e) override;
    ExprRef createFPIsSubNorm(ExprRef e) override;
    ExprRef createFPIsInf(ExprRef e) override;
    ExprRef createFPIsNan(ExprRef e) override;
    ExprRef createFPIsNeg(ExprRef e) override;
    ExprRef createFPIsPos(ExprRef e) override;
    ExprRef createLOr(ExprRef l, ExprRef r) override;
    ExprRef createLAnd(ExprRef l, ExprRef r) override;
    ExprRef createLNot(ExprRef e) override;
    ExprRef createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false) override;
    ExprRef createStore(ExprRef array, ExprRef index, ExprRef newV) override;
    ExprRef createSelect(ExprRef array, ExprRef index) override;
    ExprRef createPara(SortRef sort) override;
    ExprRef createFunc(SortRef sort, const std::vector<ExprRef>& para_sort) override;
    ExprRef createApp(ExprRef func, const std::vector<ExprRef> &para_expr) override;
    // {END:BASE}
  };

  class CacheExprBuilder : public ExprBuilder {
  public:
    // {BEGIN:CACHE}
    ExprRef createConcat(ExprRef l, ExprRef r) override;
    ExprRef createExtract(ExprRef e, UINT32 index, UINT32 bits) override;
    ExprRef createZExt(ExprRef e, UINT32 bits) override;
    ExprRef createSExt(ExprRef e, UINT32 bits) override;
    ExprRef createRol(ExprRef e, UINT32 bits) override;
    ExprRef createRor(ExprRef e, UINT32 bits) override;
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createSub(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef l, ExprRef r) override;
    ExprRef createUDiv(ExprRef l, ExprRef r) override;
    ExprRef createSDiv(ExprRef l, ExprRef r) override;
    ExprRef createURem(ExprRef l, ExprRef r) override;
    ExprRef createSRem(ExprRef l, ExprRef r) override;
    ExprRef createNeg(ExprRef e) override;
    ExprRef createSMod(ExprRef l, ExprRef r) override;
    ExprRef createNot(ExprRef e) override;
    ExprRef createAnd(ExprRef l, ExprRef r) override;
    ExprRef createOr(ExprRef l, ExprRef r) override;
    ExprRef createXor(ExprRef l, ExprRef r) override;
    ExprRef createShl(ExprRef l, ExprRef r) override;
    ExprRef createLShr(ExprRef l, ExprRef r) override;
    ExprRef createAShr(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createDistinct(ExprRef l, ExprRef r) override;
    ExprRef createUlt(ExprRef l, ExprRef r) override;
    ExprRef createUle(ExprRef l, ExprRef r) override;
    ExprRef createUgt(ExprRef l, ExprRef r) override;
    ExprRef createUge(ExprRef l, ExprRef r) override;
    ExprRef createSlt(ExprRef l, ExprRef r) override;
    ExprRef createSle(ExprRef l, ExprRef r) override;
    ExprRef createSgt(ExprRef l, ExprRef r) override;
    ExprRef createSge(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFSub(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;
    ExprRef createFDiv(ExprRef l, ExprRef r) override;
    ExprRef createFRem(ExprRef l, ExprRef r) override;
    ExprRef createFMax(ExprRef l, ExprRef r) override;
    ExprRef createFMin(ExprRef l, ExprRef r) override;
    ExprRef createFAbs(ExprRef e) override;
    ExprRef createFNeg(ExprRef e) override;
    ExprRef createFSqrt(ExprRef e) override;
    ExprRef createFOgt(ExprRef l, ExprRef r) override;
    ExprRef createFOge(ExprRef l, ExprRef r) override;
    ExprRef createFOlt(ExprRef l, ExprRef r) override;
    ExprRef createFOle(ExprRef l, ExprRef r) override;
    ExprRef createFOeq(ExprRef l, ExprRef r) override;
    ExprRef createFOne(ExprRef l, ExprRef r) override;
    ExprRef createFOrd(ExprRef l, ExprRef r) override;
    ExprRef createFUno(ExprRef l, ExprRef r) override;
    ExprRef createFUgt(ExprRef l, ExprRef r) override;
    ExprRef createFUge(ExprRef l, ExprRef r) override;
    ExprRef createFUlt(ExprRef l, ExprRef r) override;
    ExprRef createFUle(ExprRef l, ExprRef r) override;
    ExprRef createFUeq(ExprRef l, ExprRef r) override;
    ExprRef createFUne(ExprRef l, ExprRef r) override;
    ExprRef createFPIsZero(ExprRef e) override;
    ExprRef createFPIsNorm(ExprRef e) override;
    ExprRef createFPIsSubNorm(ExprRef e) override;
    ExprRef createFPIsInf(ExprRef e) override;
    ExprRef createFPIsNan(ExprRef e) override;
    ExprRef createFPIsNeg(ExprRef e) override;
    ExprRef createFPIsPos(ExprRef e) override;
    ExprRef createLOr(ExprRef l, ExprRef r) override;
    ExprRef createLAnd(ExprRef l, ExprRef r) override;
    ExprRef createLNot(ExprRef e) override;
    ExprRef createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false) override;
    ExprRef createStore(ExprRef array, ExprRef index, ExprRef newV) override;
    ExprRef createSelect(ExprRef array, ExprRef index) override;
    // {END:CACHE}

  protected:
    ExprCache cache_;

    void insertToCache(ExprRef e);
    ExprRef findInCache(ExprRef e);
    ExprRef findOrInsert(ExprRef new_expr);

  };

  class CommutativeExprBuilder : public ExprBuilder {
  public:
    // {BEGIN:COMMUTATIVE}
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef l, ExprRef r) override;
    ExprRef createAnd(ExprRef l, ExprRef r) override;
    ExprRef createOr(ExprRef l, ExprRef r) override;
    ExprRef createXor(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createDistinct(ExprRef l, ExprRef r) override;
    ExprRef createUlt(ExprRef l, ExprRef r) override;
    ExprRef createUle(ExprRef l, ExprRef r) override;
    ExprRef createUgt(ExprRef l, ExprRef r) override;
    ExprRef createUge(ExprRef l, ExprRef r) override;
    ExprRef createSlt(ExprRef l, ExprRef r) override;
    ExprRef createSle(ExprRef l, ExprRef r) override;
    ExprRef createSgt(ExprRef l, ExprRef r) override;
    ExprRef createSge(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFSub(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;
    ExprRef createFDiv(ExprRef l, ExprRef r) override;
    ExprRef createFRem(ExprRef l, ExprRef r) override;
    ExprRef createFMax(ExprRef l, ExprRef r) override;
    ExprRef createFMin(ExprRef l, ExprRef r) override;
    ExprRef createFOgt(ExprRef l, ExprRef r) override;
    ExprRef createFOge(ExprRef l, ExprRef r) override;
    ExprRef createFOlt(ExprRef l, ExprRef r) override;
    ExprRef createFOle(ExprRef l, ExprRef r) override;
    ExprRef createFOeq(ExprRef l, ExprRef r) override;
    ExprRef createFOne(ExprRef l, ExprRef r) override;
    ExprRef createFOrd(ExprRef l, ExprRef r) override;
    ExprRef createFUno(ExprRef l, ExprRef r) override;
    ExprRef createFUgt(ExprRef l, ExprRef r) override;
    ExprRef createFUge(ExprRef l, ExprRef r) override;
    ExprRef createFUlt(ExprRef l, ExprRef r) override;
    ExprRef createFUle(ExprRef l, ExprRef r) override;
    ExprRef createFUeq(ExprRef l, ExprRef r) override;
    ExprRef createFUne(ExprRef l, ExprRef r) override;
    ExprRef createLAnd(ExprRef l, ExprRef r) override;
    ExprRef createLOr(ExprRef l, ExprRef r) override;
    // {END:COMMUTATIVE}
    ExprRef createSub(ExprRef l, ExprRef r) override;
  };

  class CommonSimplifyExprBuilder : public ExprBuilder {
    // expression builder for common simplification
  public:
    ExprRef createConcat(ExprRef l, ExprRef r) override;
    ExprRef createExtract(ExprRef e, UINT32 index, UINT32 bits) override;
    ExprRef createZExt(ExprRef e, UINT32 bits) override;
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef, ExprRef) override;
    ExprRef createAnd(ExprRef, ExprRef) override;
    ExprRef createOr(ExprRef, ExprRef) override;
    ExprRef createXor(ExprRef, ExprRef) override;
    ExprRef createShl(ExprRef l, ExprRef r) override;
    ExprRef createLShr(ExprRef l, ExprRef r) override;
    ExprRef createAShr(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;

  private:
    ExprRef simplifyAnd(ExprRef l, ExprRef r);
    ExprRef simplifyOr(ExprRef l, ExprRef r);
    ExprRef simplifyXor(ExprRef l, ExprRef r);
  };

  class ConstantFoldingExprBuilder : public ExprBuilder {
  public:
    ExprRef createConcat(ExprRef l, ExprRef r) override;
    ExprRef createExtract(ExprRef e, UINT32 index, UINT32 bits) override;
    ExprRef createZExt(ExprRef e, UINT32 bits) override;
    ExprRef createSExt(ExprRef e, UINT32 bits) override;
    ExprRef createRol(ExprRef e, UINT32 bits) override;
    ExprRef createRor(ExprRef e, UINT32 bits) override;
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createSub(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef l, ExprRef r) override;
    ExprRef createUDiv(ExprRef l, ExprRef r) override;
    ExprRef createSDiv(ExprRef l, ExprRef r) override;
    ExprRef createURem(ExprRef l, ExprRef r) override;
    ExprRef createSRem(ExprRef l, ExprRef r) override;
    ExprRef createNeg(ExprRef e) override;
    ExprRef createNot(ExprRef e) override;
    ExprRef createSMod(ExprRef l, ExprRef r) override;
    ExprRef createAnd(ExprRef l, ExprRef r) override;
    ExprRef createOr(ExprRef l, ExprRef r) override;
    ExprRef createXor(ExprRef l, ExprRef r) override;
    ExprRef createShl(ExprRef l, ExprRef r) override;
    ExprRef createLShr(ExprRef l, ExprRef r) override;
    ExprRef createAShr(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createDistinct(ExprRef l, ExprRef r) override;
    ExprRef createUlt(ExprRef l, ExprRef r) override;
    ExprRef createUle(ExprRef l, ExprRef r) override;
    ExprRef createUgt(ExprRef l, ExprRef r) override;
    ExprRef createUge(ExprRef l, ExprRef r) override;
    ExprRef createSlt(ExprRef l, ExprRef r) override;
    ExprRef createSle(ExprRef l, ExprRef r) override;
    ExprRef createSgt(ExprRef l, ExprRef r) override;
    ExprRef createSge(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFSub(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;
    ExprRef createFDiv(ExprRef l, ExprRef r) override;
    ExprRef createFRem(ExprRef l, ExprRef r) override;
    ExprRef createFMax(ExprRef l, ExprRef r) override;
    ExprRef createFMin(ExprRef l, ExprRef r) override;
    ExprRef createFAbs(ExprRef e) override;
    ExprRef createFNeg(ExprRef e) override;
    ExprRef createFSqrt(ExprRef e) override;
    ExprRef createFOgt(ExprRef l, ExprRef r) override;
    ExprRef createFOge(ExprRef l, ExprRef r) override;
    ExprRef createFOlt(ExprRef l, ExprRef r) override;
    ExprRef createFOle(ExprRef l, ExprRef r) override;
    ExprRef createFOeq(ExprRef l, ExprRef r) override;
    ExprRef createFOne(ExprRef l, ExprRef r) override;
    ExprRef createFPIsZero(ExprRef e) override;
    ExprRef createFPIsNorm(ExprRef e) override;
    ExprRef createFPIsSubNorm(ExprRef e) override;
    ExprRef createFPIsInf(ExprRef e) override;
    ExprRef createFPIsNan(ExprRef e) override;
    ExprRef createFPIsNeg(ExprRef e) override;
    ExprRef createFPIsPos(ExprRef e) override;
    ExprRef createLOr(ExprRef l, ExprRef r) override;
    ExprRef createLAnd(ExprRef l, ExprRef r) override;
    ExprRef createLNot(ExprRef e) override;
    ExprRef createIte(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false) override;

    static ExprBuilder* create();
  };

  class SymbolicExprBuilder : public ExprBuilder {
  public:
    ExprRef createConcat(ExprRef l, ExprRef r) override;
    ExprRef createAdd(ExprRef l, ExprRef r) override;
    ExprRef createSub(ExprRef l, ExprRef r) override;
    ExprRef createMul(ExprRef l, ExprRef r) override;
    ExprRef createSDiv(ExprRef l, ExprRef r) override;
    ExprRef createUDiv(ExprRef l, ExprRef r) override;
    ExprRef createFAdd(ExprRef l, ExprRef r) override;
    ExprRef createFSub(ExprRef l, ExprRef r) override;
    ExprRef createFMul(ExprRef l, ExprRef r) override;
    ExprRef createFDiv(ExprRef l, ExprRef r) override;
    ExprRef createFMax(ExprRef l, ExprRef r) override;
    ExprRef createFMin(ExprRef l, ExprRef r) override;
    ExprRef createFAbs(ExprRef e) override;
    ExprRef createFNeg(ExprRef e) override;
    ExprRef createFSqrt(ExprRef e) override;
    ExprRef createAnd(ExprRef l, ExprRef r) override;
    ExprRef createOr(ExprRef l, ExprRef r) override;
    ExprRef createXor(ExprRef l, ExprRef r) override;
    ExprRef createEqual(ExprRef l, ExprRef r) override;
    ExprRef createDistinct(ExprRef l, ExprRef r) override;
    ExprRef createLOr(ExprRef l, ExprRef r) override;
    ExprRef createLAnd(ExprRef l, ExprRef r) override;
    ExprRef createLNot(ExprRef e) override;
    ExprRef createIte(
        ExprRef expr_cond,
        ExprRef expr_true,
        ExprRef expr_false) override;
    ExprRef createExtract(ExprRef op, UINT32 index, UINT32 bits) override;

    ExprRef createAdd(ConstantExprRef l, NonConstantExprRef r);
    ExprRef createAdd(NonConstantExprRef l, NonConstantExprRef r);
    ExprRef createSub(ConstantExprRef l, NonConstantExprRef r);
    ExprRef createSub(NonConstantExprRef l, NonConstantExprRef r);
    ExprRef createMul(ConstantExprRef l, NonConstantExprRef r);
    ExprRef createAnd(ConstantExprRef l, NonConstantExprRef r);
    ExprRef createAnd(NonConstantExprRef l, NonConstantExprRef r);
    ExprRef createOr(ConstantExprRef l, NonConstantExprRef r);
    ExprRef createOr(NonConstantExprRef l, NonConstantExprRef r);
    ExprRef createXor(NonConstantExprRef l, NonConstantExprRef r);
    ExprRef createSDiv(NonConstantExprRef l, ConstantExprRef r);
    ExprRef createUDiv(NonConstantExprRef l, ConstantExprRef r);

    static ExprBuilder* create();

  private:
    ExprRef simplifyLNot(ExprRef);
    ExprRef simplifyExclusiveExpr(ExprRef l, ExprRef r);
  };

  extern ExprBuilder *g_expr_builder;

} // namespace qsym

#endif // QSYM_EXPR_BUILDER_H_
