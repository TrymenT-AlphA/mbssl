#ifndef QSYM_COMMON_H_
#define QSYM_COMMON_H_

#include <llvm/ADT/APSInt.h>
#include "pin.h"

#define EXPR_COMPLEX_LEVEL_THRESHOLD 4
#define XXH_STATIC_LINKING_ONLY

#define xglue(x, y) x ## y
#define glue(x, y) xglue(x, y)

#define likely(x)       __builtin_expect((x), 1)
#define unlikely(x)     __builtin_expect((x), 0)

inline void CRASH() {
    ((void(*)())0)();
}

inline INT32 getBitCount(INT32 x) {
  return __builtin_popcount(x);
}

inline ADDRINT toTruncValue(llvm::APInt val) {
  return val.trunc(sizeof(ADDRINT) * CHAR_BIT).getZExtValue();
}

inline ADDRINT getMask(ADDRINT size) {
  switch (size) {
    case 1:
      return 0xff;
    case 2:
      return 0xffff;
    case 4:
      return 0xffffffff;
#ifdef __x86_64__
    case 8:
      return 0xffffffffffffffff;
#endif
    default:
      CRASH();
      return -1;
  }
}

#endif
