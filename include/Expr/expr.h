#ifndef QSYM_EXPR_H_
#define QSYM_EXPR_H_

#include <iostream>
#include <iomanip>
#include <map>
#include <unordered_set>
#include <set>
#include <sstream>
#include <vector>

#include "dependency.h"
#include "range.h"
#include "common.h"
#include "xxhash.h"
#include "sort.h"

#include <llvm/ADT/APFloat.h>

#define INDEXBITS 64 // bits of array index
#define VALUEBITS 8 // bits of array value

using namespace MBSSL;

// XXX: need to change into non-global variable?
namespace qsym {
  enum Kind {
    Bool, // 0
    Constant, // 1
    Read, // 2
    Concat, // 3
    Extract, // 4

    ZExt, // 5
    SExt, // 6

    // Bv Arithmetic
    Add, // 7
    Sub, // 8
    Mul, // 9
    UDiv, // 10
    SDiv, // 11
    URem, // 12
    SRem, // 13
    SMod, // 14
    Neg,  // 15

    // Bits
    Not, // 16
    And,
    Or,
    Xor,
    Shl,
    LShr,
    AShr,
    Rol,
    Ror,

    // Compare
    Equal, // 25
    Distinct,
    Ult,
    Ule,
    Ugt,
    Uge,
    Slt,
    Sle,
    Sgt,
    Sge,

    // Logical
    LOr, // 35
    LAnd,
    LNot,

    // Special
    Ite, // 38

    // Array read/write
    Array, // 39
    Select,
    Store,

    // Float Arithmetic
    Float, // 42
    FAdd,
    FSub,
    FMul,
    FDiv,
    FRem,
    FMax,
    FMin,
    FAbs,
    FNeg,
    FSqrt,

    // Float Compare
    FOgt, // 49
    FOge,
    FOlt,
    FOle,
    FOeq,
    FOne,
    FOrd,
    FUno,
    FUgt,
    FUge,
    FUlt,
    FUle,
    FUeq,
    FUne,

    // FP check
    FPIsZero,
    FPIsNorm,
    FPIsSubNorm,
    FPIsInf,
    FPIsNan,
    FPIsNeg,
    FPIsPos,

    // Cast
    FPToBV, // 64
    BVToFP,
    FPToSI,
    FPToUI,
    FPToFP,
    SIToFP,
    UIToFP,

    // Rounding Mode
    RNE,
    RNA,
    RTP,
    RTN,
    RTZ,

    // Command
    Para,
    Func,
    App,

    Invalid
  };

  Kind swapKind(Kind kind);
  Kind negateKind(Kind kind);
  bool isNegatableKind(Kind kind);

// forward declaration
#define DECLARE_EXPR(cls) \
  class cls; \
  typedef std::shared_ptr<cls> glue(cls, Ref);

  DECLARE_EXPR(Expr);
  DECLARE_EXPR(ConstantExpr);
  DECLARE_EXPR(ConstantFloatExpr);
  DECLARE_EXPR(NonConstantExpr);
  DECLARE_EXPR(BoolExpr);

  typedef std::shared_ptr<Sort> glue(Sort, Ref);

  typedef std::weak_ptr<Expr> WeakExprRef;
  typedef std::vector<WeakExprRef> WeakExprRefVectorTy;

  template <class T>
  inline std::shared_ptr<T> castAs(ExprRef e) {
    if (T::classOf(*e))
      return static_pointer_cast<T>(e);
    else
      return NULL;
  }

  template <class T>
  inline std::shared_ptr<T> castAsNonNull(ExprRef e) {
    assert(T::classOf(*e));
    return static_pointer_cast<T>(e);
  }

  class ExprBuilder;

  typedef std::set<INT32> DepSet;

  class Expr : public DependencyNode {
  public:
    Expr(Kind kind, SortRef sort, UINT32 bits);
    virtual ~Expr();
    Expr(const Expr& that) = delete;

    SortRef sort_;
    SortRef getSort() {return sort_;}

    XXH32_hash_t hash();

    Kind kind() const {
      return kind_;
    }

    UINT32 bits() const {
      return bits_;
    }

    UINT32 bytes() const {
      // utility function to convert from bits to bytes
      assert(bits() % CHAR_BIT == 0);
      return bits() / CHAR_BIT;
    }

    inline ExprRef getChild(UINT32 index) const {
      return children_[index];
    }

    inline INT32 num_children() const {
      return children_.size();
    }

    inline ExprRef getFirstChild() const {
      return getChild(0);
    }

    inline ExprRef getSecondChild() const {
      return getChild(1);
    }

    inline ExprRef getLeft() const {
      return getFirstChild();
    }

    inline ExprRef getRight() const {
      return getSecondChild();
    }

    inline std::vector<ExprRef> getChildrens() const {
      return children_;
    }

    INT32 depth();

    bool isConcrete() const {
      return isConcrete_;
    }

    bool isConstant() const {
      return kind_ == Constant;
    }

    bool isBool() const {
      return kind_ == Bool;
    }

    bool isCmpOrBit() const {
      return (kind_ <= qsym::FUne && kind_ >= qsym::FOgt) ||
             (kind_ <= qsym::LNot && kind_ >= qsym::Not);
    }

    bool isZero() const;
    bool isAllOnes() const;
    bool isOne() const;

    DepSet& getDeps() {
      if (deps_ == NULL) {
        deps_ = new DepSet();
        DepSet& deps = *deps_;
        for (INT32 i = 0; i < num_children(); i++) {
          DepSet& other = getChild(i)->getDeps();
          deps.insert(other.begin(), other.end());
        }
      }
      return *deps_;
    }

    DependencySet computeDependencies() override;

    UINT32 countLeadingZeros() {
      if (leading_zeros_ == (UINT32)-1)
        leading_zeros_ = _countLeadingZeros();
      return leading_zeros_;
    }
    virtual UINT32 _countLeadingZeros() const { return 0; }

    virtual void print(ostream& os=std::cerr, UINT depth=0) const;
    void printConstraints();
    std::string toString() const;

    friend bool equalMetadata(const Expr& l, const Expr& r) {
      return (const_cast<Expr&>(l).hash() == const_cast<Expr&>(r).hash()
              && l.kind_ == r.kind_
              && l.num_children() == r.num_children()
              && l.bits_ == r.bits_
              && l.equalAux(r));
    }

    friend bool equalShallowly(const Expr& l, const Expr& r) {
      // Check equality only in 1 depth if not return false
      if (!equalMetadata(l, r))
        return false;

      // If one of childrens is different, then false
      for (INT32 i = 0; i < l.num_children(); i++) {
        if (l.children_[i] != r.children_[i])
          return false;
      }
      return true;
    }

    friend bool operator==(const Expr& l, const Expr& r) {
      // 1. if metadata are different -> different
      if (!equalMetadata(l, r))
        return false;

      // 2. if metadata of children are different -> different
      for (INT32 i = 0; i < l.num_children(); i++) {
        if (!equalMetadata(*l.children_[i], *r.children_[i]))
          return false;
      }

      // 3. if all childrens are same --> same
      for (INT32 i = 0; i < l.num_children(); i++) {
        if (l.children_[i] != r.children_[i]
            && *l.children_[i] != *r.children_[i])
          return false;
      }
      return true;
    }

    friend bool operator!=(const Expr& l, const Expr& r) {
      return !(l == r);
    }

    inline void addChild(ExprRef e) {
      children_.push_back(e);
      if (!e->isConcrete())
        isConcrete_ = false;
    }
    inline void addUse(WeakExprRef e) { uses_.push_back(e); }

    void addConstraint(Kind kind, llvm::APInt rhs, llvm::APInt adjustment);
    RangeSet* getRangeSet(bool is_unsigned) const { return range_sets[is_unsigned]; }
    void setRangeSet(bool is_unsigned, RangeSet* rs) { range_sets[is_unsigned] = rs; }
    RangeSet* getSignedRangeSet() const { return getRangeSet(false); }
    RangeSet* getUnsignedRangeSet() const { return getRangeSet(true); }

    ExprRef evaluate();

  protected:
    Kind kind_;
    UINT32 bits_;
    std::vector< ExprRef > children_;
    XXH32_hash_t* hash_;
    RangeSet *range_sets[2];

    // concretization
    bool isConcrete_;

    INT32 depth_;
    DepSet* deps_;
    WeakExprRefVectorTy uses_;
    UINT32 leading_zeros_;
    ExprRef evaluation_;

    void printChildren(ostream& os, bool start, UINT depth) const;

    virtual bool printAux(ostream&) const {
      return false;
    }

    void addConstraint(RangeSet& rs,
                       Kind kind,
                       llvm::APInt& rhs,
                       llvm::APInt& adjustment);
    void addConstraint(bool is_unsigned,
                       Kind kind,
                       llvm::APInt& rhs,
                       llvm::APInt& adjustment);

    virtual std::string getName() const = 0;
    virtual void hashAux(XXH32_state_t*) { return; }
    virtual bool equalAux(const Expr&) const { return true; }
    virtual ExprRef evaluateImpl() = 0;
  }; // class Expr


  class ConstantExpr : public Expr {
  public:
    ConstantExpr(ADDRINT value, UINT32 bits) :
        Expr(Constant, SortBitVec(bits), bits),
        value_(bits, value) {}

    ConstantExpr(const llvm::APInt& value, UINT32 bits) :
        Expr(Constant, SortBitVec(bits), bits),
        value_(value) {}

    inline llvm::APInt value() const { return value_; }
    inline bool isZero() const { return value_ == 0; }
    inline bool isOne() const { return value_ == 1; }
    inline bool isAllOnes() const { return value_.isAllOnesValue(); }
    static bool classOf(const Expr& e) { return e.kind() == Constant; }
    UINT32 getActiveBits() const { return value_.getActiveBits(); }
    void print(ostream& os, UINT depth) const override;

    UINT32 _countLeadingZeros() const override {
      return value_.countLeadingZeros();
    }

  protected:
    std::string getName() const override {
      return "Constant";
    }

    bool printAux(ostream& os) const override {
      os << "value=0x" << value_.toString(16, false)
         << ", bits=" << bits_;
      return true;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state,
                   value_.getRawData(),
                   value_.getNumWords() * sizeof(uint64_t));
    }

    bool equalAux(const Expr& other) const override {
      const ConstantExpr& typed_other = static_cast<const ConstantExpr&>(other);
      return value_ == typed_other.value();
    }

    ExprRef evaluateImpl() override;
    llvm::APInt value_;
  };

  class ConstantFloatExpr : public Expr {
  public:
    ConstantFloatExpr(const llvm::APFloat &value, UINT32 bits) :
        Expr(Float, SortFloatingPoint(bits), bits),
        value_(value) {}

    inline llvm::APFloat value() const { return value_; }

    static bool classOf(const Expr &e) { return e.kind() == Float; }

    void print(ostream &os, UINT depth) const override;

    bool fogt(ConstantFloatExpr &RHS) {
      return value_.compare(RHS.value()) == llvm::APFloat::cmpGreaterThan;
    }

    bool foge(ConstantFloatExpr &RHS) {
      auto result = value_.compare(RHS.value());
      return result == llvm::APFloat::cmpGreaterThan || result == llvm::APFloat::cmpEqual;
    }

    bool folt(ConstantFloatExpr &RHS) {
      return value_.compare(RHS.value()) == llvm::APFloat::cmpLessThan;
    }

    bool fole(ConstantFloatExpr &RHS) {
      auto result = value_.compare(RHS.value());
      return result == llvm::APFloat::cmpLessThan || result == llvm::APFloat::cmpEqual;
    }

    bool foeq(ConstantFloatExpr &RHS) {
      return value_.compare(RHS.value()) == llvm::APFloat::cmpEqual;
    }

    bool fone(ConstantFloatExpr &RHS) {
      return value_.compare(RHS.value()) != llvm::APFloat::cmpEqual;
    }

    llvm::APFloat frem(ConstantFloatExpr &RHS) {
      llvm::APFloat Result(*&value_);
      Result.mod(RHS.value());
      return Result;
    }

    llvm::APFloat fpmax(ConstantFloatExpr &RHS) {
      return llvm::maxnum(value_, RHS.value());
    }

    llvm::APFloat fpmin(ConstantFloatExpr &RHS) {
      return llvm::minnum(value_, RHS.value());
    }

    llvm::APFloat fabs() {
      llvm::APFloat Result(*&value_);
      return llvm::abs(Result);
    }

    llvm::APFloat fneg() {
      llvm::APFloat Result(*&value_);
      return llvm::neg(Result);
    }

    llvm::APFloat fsqrt() {
      double val = value_.convertToDouble();
      double sqrtVal = std::sqrt(val);
      llvm::APFloat Result(sqrtVal);
      return Result;
    }

  protected:
    std::string getName() const override {
      return "ConstantFloat";
    }

    bool printAux(ostream &os) const override {
      os << "value=0x" << value_.bitcastToAPInt().toString(16, false)
         << ", bits=" << bits_;
      return true;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state,
                   value_.bitcastToAPInt().getRawData(),
                   value_.bitcastToAPInt().getNumWords() * sizeof(uint64_t));
    }

    ExprRef evaluateImpl() override;

    llvm::APFloat value_;
  };

  class NonConstantExpr : public Expr {
  public:
    using Expr::Expr;
    static bool classOf(const Expr& e) { return !ConstantExpr::classOf(e); }
  };

/// Floating-Point Rounding Mode Expr
#define ROUNDINGMODE_EXPR_CLASS(_class_kind)        \
  class _class_kind ## Expr : public Expr {         \
  public:                                           \
    _class_kind ## Expr() :                         \
      Expr(_class_kind, SortRoundingMode(), 0) {}   \
    static bool classOf(const Expr& e) {            \
      return e.kind() == _class_kind;               \
    }                                               \
  protected:                                        \
    std::string getName() const override {          \
      return #_class_kind;                          \
    }                                               \
    ExprRef evaluateImpl() override;                \
  };
  ROUNDINGMODE_EXPR_CLASS(RNE)
  ROUNDINGMODE_EXPR_CLASS(RNA)
  ROUNDINGMODE_EXPR_CLASS(RTP)
  ROUNDINGMODE_EXPR_CLASS(RTN)
  ROUNDINGMODE_EXPR_CLASS(RTZ)
#undef ROUNDINGMODE_EXPR_CLASS

  /// Cast Expr
  class CastExpr : public NonConstantExpr {
  public:
    CastExpr(Kind kind, SortRef sort, ExprRef e, UINT32 bits)
        : NonConstantExpr(kind, sort, bits) {
      addChild(e);
    }
    ExprRef expr() const { return getFirstChild(); }
  };

#define CAST_EXPR_CLASS(_class_kind, _sort_kind)           \
  class _class_kind ## Expr : public CastExpr {            \
  public:                                                  \
      _class_kind ## Expr(ExprRef e, UINT32 bits) :              \
          CastExpr(_class_kind, _sort_kind(bits), e, e->bits()){}\
      static bool classOf(const Expr& e) {                 \
          return e.kind() == _class_kind;                  \
      }                                                    \
  protected:                                               \
      std::string getName() const override {               \
          return #_class_kind;                             \
      }                                                    \
      ExprRef evaluateImpl() override;                     \
  };
  CAST_EXPR_CLASS(FPToBV, SortBitVec)
  CAST_EXPR_CLASS(FPToSI, SortBitVec)
  CAST_EXPR_CLASS(FPToUI, SortBitVec)

  CAST_EXPR_CLASS(BVToFP, SortFloatingPoint)
  CAST_EXPR_CLASS(FPToFP, SortFloatingPoint)
  CAST_EXPR_CLASS(SIToFP, SortFloatingPoint)
  CAST_EXPR_CLASS(UIToFP, SortFloatingPoint)
#undef CAST_EXPR_CLASS

  class UnaryExpr : public NonConstantExpr {
  public:
    UnaryExpr(Kind kind, SortRef sort, ExprRef e, UINT32 bits)
        : NonConstantExpr(kind, sort, bits) {
      addChild(e);
    }
    UnaryExpr(Kind kind, SortRef sort,ExprRef e)
        : UnaryExpr(kind, sort, e, e->bits()) {}

    ExprRef expr() const { return getFirstChild(); }

  protected:
    ExprRef evaluateImpl() override;
  };

  class BinaryExpr : public NonConstantExpr {
  public:
    BinaryExpr(Kind kind, SortRef sort,
               ExprRef l, ExprRef r, UINT32 bits) :
        NonConstantExpr(kind, sort, bits) {
      addChild(l);
      addChild(r);
    }

    BinaryExpr(Kind kind, SortRef sort,
               ExprRef l, ExprRef r) :
        BinaryExpr(kind, sort, l, r, l->bits()) {}

    void print(ostream& os, UINT depth, const char* op) const;
  protected:
    ExprRef evaluateImpl() override;
  };

  class LinearBinaryExpr : public BinaryExpr {
    using BinaryExpr::BinaryExpr;
  };

  class NonLinearBinaryExpr : public BinaryExpr {
    using BinaryExpr::BinaryExpr;
  };

  class BoolExpr : public NonConstantExpr {
  public:
    BoolExpr(bool value) :
        NonConstantExpr(Bool, SortBool(), 1),
        value_(value) {}

    inline bool value() const { return value_; }
    static bool classOf(const Expr& e) { return e.kind() == Bool; }

  protected:
    std::string getName() const override {
      return "Bool";
    }

    bool printAux(ostream& os) const override {
      os << "value=" << value_;
      return true;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state, &value_, sizeof(value_));
    }

    bool equalAux(const Expr& other) const override {
      const BoolExpr& typed_other = static_cast<const BoolExpr&>(other);
      return value_ == typed_other.value();
    }

    ExprRef evaluateImpl() override;
    bool value_;
  };

  class ReadExpr : public NonConstantExpr {
  public:
    ReadExpr(UINT32 index, std::string name)
        : NonConstantExpr(Read, SortBitVec(8), 8), index_(index) {
      deps_ = new DepSet();
      deps_->insert(index);
      isConcrete_ = false;
      name_ = name;
    }

    ReadExpr(UINT32 index)
        : NonConstantExpr(Read, SortBitVec(8), 8), index_(index) {
      deps_ = new DepSet();
      deps_->insert(index);
      isConcrete_ = false;
    }

    std::string getName() const override {
      return "Read";
    }

    inline UINT32 index() const { return index_; }
    inline std::string name() const { return name_; }
    static bool classOf(const Expr& e) { return e.kind() == Read; }

  protected:
    bool printAux(ostream& os) const override {
      if (name_ != "")
        os << name_ + ",idx=" << hexstr(index_);
      else
        os << "idx=" << hexstr(index_);
      return true;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state, &index_, sizeof(index_));
      XXH32_update(state, name_.c_str(), name_.size());
    }

    bool equalAux(const Expr& other) const override {
      const ReadExpr& typed_other = static_cast<const ReadExpr&>(other);
      return index_ == typed_other.index();
    }

    ExprRef evaluateImpl() override;
    UINT32 index_;
    std::string name_; // add a name for each read expression
  };

  class ConcatExpr : public NonConstantExpr {
  public:
    ConcatExpr(ExprRef l, ExprRef r)
        : NonConstantExpr(
            Concat,
            SortBitVec(l->bits() + r->bits()),
            l->bits() + r->bits()) {
      addChild(l);
      addChild(r);
    }

    void print(ostream& os, UINT depth) const override;

    std::string getName() const override {
      return "Concat";
    }

    static bool classOf(const Expr& e) { return e.kind() == Concat; }

    UINT32 _countLeadingZeros() const override {
      UINT32 result = getChild(0)->countLeadingZeros();
      if (result == getChild(0)->bits())
        result += getChild(1)->countLeadingZeros();
      return result;
    }

  protected:

    ExprRef evaluateImpl() override;
  };

  class ExtractExpr : public UnaryExpr {
  public:
    ExtractExpr(ExprRef e, UINT32 index, UINT32 bits)
        : UnaryExpr(Extract, SortBitVec(bits), e, bits), index_(index) {
      assert(bits + index <= e->bits());
    }

    UINT32 index() const { return index_; }
    ExprRef expr() const { return getFirstChild(); }
    static bool classOf(const Expr& e) { return e.kind() == Extract; }

  protected:
    std::string getName() const override {
      return "Extract";
    }

    bool printAux(ostream& os) const override {
      os << "index=" << index_ << ", bits=" << bits_;
      return true;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state, &index_, sizeof(index_));
    }

    bool equalAux(const Expr& other) const override {
      const ExtractExpr& typed_other = static_cast<const ExtractExpr&>(other);
      return index_ == typed_other.index();
    }

    ExprRef evaluateImpl() override;

    UINT32 index_;
  };

  class ExtExpr : public UnaryExpr {
  public:
    using UnaryExpr::UnaryExpr;

    ExprRef expr() const { return getFirstChild(); }
    static bool classOf(const Expr& e) {
      return e.kind() == ZExt || e.kind() == SExt;
    }
  };

  class ZExtExpr : public ExtExpr {
  public:
    ZExtExpr(ExprRef e, UINT32 bits)
        : ExtExpr(ZExt, SortBitVec(bits), e, bits) {}

    std::string getName() const override {
      return "ZExt";
    }

    static bool classOf(const Expr& e) { return e.kind() == ZExt; }

    UINT32 _countLeadingZeros() const override {
      return bits_ - getChild(0)->bits();
    }

  protected:

    bool printAux(ostream& os) const override {
      os << "bits=" << bits_;
      return true;
    }

    ExprRef evaluateImpl() override;
  };

  class SExtExpr : public ExtExpr {
  public:
    SExtExpr(ExprRef e, UINT32 bits)
        : ExtExpr(SExt, SortBitVec(bits), e, bits) {}

    std::string getName() const override {
      return "SExt";
    }

    static bool classOf(const Expr& e) { return e.kind() == SExt; }

  protected:
    bool printAux(ostream& os) const override {
      os << "bits=" << bits_;
      return true;
    }

    ExprRef evaluateImpl() override;
  };

  class RolExpr : public UnaryExpr {
  public:
    RolExpr(ExprRef e, UINT32 bits)
        : UnaryExpr(Rol, SortBitVec(bits), e, bits) {}

    std::string getName() const override { return "Rol"; }
    static bool classOf(const Expr& e) { return e.kind() == Rol; }
  protected:
    bool printAux(ostream& os) const override {
      os << "bits=" << bits_;
      return true;
    }
    ExprRef evaluateImpl() override;
  };

  class RorExpr : public UnaryExpr {
  public:
    RorExpr(ExprRef e, UINT32 bits)
        : UnaryExpr(SExt, SortBitVec(bits), e, bits) {}

    std::string getName() const override { return "Ror"; }
    static bool classOf(const Expr& e) { return e.kind() == Ror; }
  protected:
    bool printAux(ostream& os) const override {
      os << "bits=" << bits_;
      return true;
    }
    ExprRef evaluateImpl() override;
  };


#define NON_LINEAR_BINARY_EXPR_CLASS(_class_kind, _sort_kind)                \
  class _class_kind ## Expr : public NonLinearBinaryExpr {                   \
  public:                                                                    \
      _class_kind ## Expr(ExprRef l, ExprRef r)                              \
        : NonLinearBinaryExpr(_class_kind, _sort_kind(l->bits()), l, r) {}   \
      static bool classOf(const Expr& e) { return e.kind() == _class_kind; } \
  protected:                                                                 \
      std::string getName() const override {                                 \
          return #_class_kind;                                               \
      }                                                                      \
  };
  NON_LINEAR_BINARY_EXPR_CLASS(And, SortBitVec)
  NON_LINEAR_BINARY_EXPR_CLASS(Or, SortBitVec)
  NON_LINEAR_BINARY_EXPR_CLASS(Xor, SortBitVec)
  NON_LINEAR_BINARY_EXPR_CLASS(Shl, SortBitVec)
  NON_LINEAR_BINARY_EXPR_CLASS(LShr, SortBitVec)
  NON_LINEAR_BINARY_EXPR_CLASS(AShr, SortBitVec)
#undef NON_LINEAR_BINARY_EXPR_CLASS

#define LOGIC_EXPR_CLASS(_class_kind)                                        \
  class _class_kind ## Expr : public NonLinearBinaryExpr {                   \
    public:                                                                  \
      _class_kind ## Expr(ExprRef l, ExprRef r)                              \
        : NonLinearBinaryExpr(_class_kind, SortBool(), l, r) {}              \
      static bool classOf(const Expr& e) { return e.kind() == _class_kind; } \
  protected:                                                                 \
      std::string getName() const override { return #_class_kind; }          \
  };

  LOGIC_EXPR_CLASS(LAnd)
  LOGIC_EXPR_CLASS(LOr)
#undef LOGIC_EXPR_CLASS

#define BOOL_EXPR_CLASS(_class_kind)                                        \
  class _class_kind ## Expr : public UnaryExpr {                   \
    public:                                                                    \
      _class_kind ## Expr(ExprRef e) : UnaryExpr(_class_kind, SortBool(), e) {}              \
      static bool classOf(const Expr& e) { return e.kind() == _class_kind; } \
  protected:                                                                 \
      std::string getName() const override { return #_class_kind; }          \
  };

  BOOL_EXPR_CLASS(LNot)
  BOOL_EXPR_CLASS(FPIsZero)
  BOOL_EXPR_CLASS(FPIsNorm)
  BOOL_EXPR_CLASS(FPIsSubNorm)
  BOOL_EXPR_CLASS(FPIsInf)
  BOOL_EXPR_CLASS(FPIsNan)
  BOOL_EXPR_CLASS(FPIsNeg)
  BOOL_EXPR_CLASS(FPIsPos)
#undef BOOL_EXPR_CLASS

  class CompareExpr : public LinearBinaryExpr {
  public:
    CompareExpr(Kind kind, ExprRef l, ExprRef r) :
        LinearBinaryExpr(kind, SortBool(), l, r, 1) {
      assert(l->bits() == r->bits());
    }
  };

#define COMPARE_EXPR_CLASS(_class_kind)                                        \
  class _class_kind ## Expr : public CompareExpr {                           \
  public:                                                                    \
      _class_kind ## Expr(ExprRef l, ExprRef h)                              \
              : CompareExpr(_class_kind, l, h) {}                            \
      static bool classOf(const Expr& e) { return e.kind() == _class_kind; } \
  protected:                                                                 \
      std::string getName() const override { return #_class_kind; }          \
  };

  // logic / bv common
  COMPARE_EXPR_CLASS(Equal)
  COMPARE_EXPR_CLASS(Distinct)

  // bv compare
  COMPARE_EXPR_CLASS(Ult)
  COMPARE_EXPR_CLASS(Ule)
  COMPARE_EXPR_CLASS(Ugt)
  COMPARE_EXPR_CLASS(Uge)
  COMPARE_EXPR_CLASS(Slt)
  COMPARE_EXPR_CLASS(Sle)
  COMPARE_EXPR_CLASS(Sgt)
  COMPARE_EXPR_CLASS(Sge)

  // floating-point compare
  COMPARE_EXPR_CLASS(FOeq)
  COMPARE_EXPR_CLASS(FOne)
  COMPARE_EXPR_CLASS(FOgt)
  COMPARE_EXPR_CLASS(FOge)
  COMPARE_EXPR_CLASS(FOlt)
  COMPARE_EXPR_CLASS(FOle)
  COMPARE_EXPR_CLASS(FOrd)

  COMPARE_EXPR_CLASS(FUeq)
  COMPARE_EXPR_CLASS(FUne)
  COMPARE_EXPR_CLASS(FUno)
  COMPARE_EXPR_CLASS(FUgt)
  COMPARE_EXPR_CLASS(FUge)
  COMPARE_EXPR_CLASS(FUlt)
  COMPARE_EXPR_CLASS(FUle)
#undef COMPARE_EXPR_CLASS

#define ARITHMETIC_EXPR_CLASS(_class_kind, _is_linear, _sort_kind)           \
  class _class_kind ## Expr : public _is_linear ## BinaryExpr {              \
  public:                                                                    \
    _class_kind ## Expr(ExprRef l, ExprRef r)                                \
      : _is_linear ## BinaryExpr(_class_kind, _sort_kind(l->bits()),l, r) {} \
    static bool classOf(const Expr &e) { return e.kind() == _class_kind; }   \
    void print(ostream &os, UINT depth) const override;                      \
  protected:                                                                 \
    std::string getName() const override { return #_class_kind; }            \
  };

  // bv arithmetic
  ARITHMETIC_EXPR_CLASS(Add, Linear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(Sub, Linear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(Mul, NonLinear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(UDiv, NonLinear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(SDiv, NonLinear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(URem, NonLinear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(SRem, NonLinear, SortBitVec)
  ARITHMETIC_EXPR_CLASS(SMod, NonLinear, SortBitVec)

  // floating-point arithmetic
  ARITHMETIC_EXPR_CLASS(FAdd, Linear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FSub, Linear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FMul, NonLinear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FDiv, NonLinear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FRem, NonLinear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FMax, NonLinear, SortFloatingPoint)
  ARITHMETIC_EXPR_CLASS(FMin, NonLinear, SortFloatingPoint)

#undef ARITHMETIC_EXPR_CLASS


#define UNARY_EXPR_CLASS(_class_kind, _sort_kind)                                 \
  class _class_kind ## Expr : public UnaryExpr {                                  \
  public:                                                                         \
      _class_kind ## Expr(ExprRef e) :                                            \
        UnaryExpr(_class_kind, _sort_kind(e->bits()), e) {} \
      static bool classOf(const Expr &e) { return e.kind() == _class_kind; }      \
  protected:                                                                      \
      std::string getName() const override { return #_class_kind; }               \
  };

  UNARY_EXPR_CLASS(Not,   SortBitVec)
  UNARY_EXPR_CLASS(Neg,   SortBitVec)
  UNARY_EXPR_CLASS(FNeg,  SortFloatingPoint)
  UNARY_EXPR_CLASS(FAbs,  SortFloatingPoint)
  UNARY_EXPR_CLASS(FSqrt, SortFloatingPoint)
#undef UNARY_EXPR_CLASS

  class IteExpr : public NonConstantExpr {
  public:
    IteExpr(ExprRef expr_cond, ExprRef expr_true, ExprRef expr_false)
        : NonConstantExpr(Ite, expr_true->getSort(), expr_true->bits()) {
      assert(expr_true->bits() == expr_false->bits());
      addChild(expr_cond);
      addChild(expr_true);
      addChild(expr_false);
    }

    static bool classOf(const Expr& e) { return e.kind() == Ite; }
    ExprRef expr_cond() const { return getChild(0); }
    ExprRef expr_true() const { return getChild(1); }
    ExprRef expr_false() const { return getChild(2); }

  protected:
    std::string getName() const override {
      return "Ite";
    }

    ExprRef evaluateImpl() override;
  };

// Array Expression
  class ArrayExpr : public NonConstantExpr {
  public:
    ArrayExpr(std::string _name, UINT32 _domain = INDEXBITS, UINT32 _range = VALUEBITS) :
        NonConstantExpr(Array, SortArray(SortBitVec(_domain), SortBitVec(_range)), 0),
        domain(_domain), range(_range), name(_name) {}

    static bool classOf(const Expr& e) { return e.kind() == Array; }
    void print(ostream& os, UINT depth) const override;

    UINT32 getDomain() { return domain; }
    UINT32 getRange() { return range; }
    std::string arrayName() { return name; }

  protected:
    std::string getName() const override {
      return name;
    }

    void hashAux(XXH32_state_t* state) override {
      XXH32_update(state, name.c_str(), name.size());
    }

    bool printAux(ostream& os) const override {
      os << " ";
      return true;
    }

    ExprRef evaluateImpl() override;

    /// domain - size of array index [64 bits]
    /// range - size of the number stored in array [8 bits]
    UINT32 domain, range;
    std::string name;
  };

/// (store a i newV)
  class StoreExpr : public NonConstantExpr {
  public:
    StoreExpr(ExprRef e, ExprRef index, ExprRef newV) :
        NonConstantExpr(Store, e->getSort(), 0) {
      assert(e->kind() == Array || e->kind() == Store);

      if (auto array = castAs<ArrayExpr>(e)) {
        domain = array->getDomain();
        range = array->getRange();
      } else if (auto write_term = castAs<StoreExpr>(e)) {
        domain = write_term->getDomain();
        range = write_term->getRange();
      } else {
        std::cerr << "@StoreExpr---------------Store expression is built incorrectly" << std::endl;
        exit(-1);
      }

      addChild(e);
      addChild(index);
      addChild(newV);
    }

    static bool classOf(const Expr& e) { return e.kind() == Store; }

    UINT32 getDomain() { return domain; }
    UINT32 getRange() { return range; }

  protected:
    std::string getName() const override {
      return "Store ";
    }

    ExprRef evaluateImpl() override;

    /// domain - size of array index [64 bits]
    /// range - size of the number stored in array [8 bits]
    UINT32 domain, range;
  };

//// a[i] - (select a i)
  class SelectExpr : public NonConstantExpr {
  public:
    SelectExpr(ExprRef e, ExprRef index)
        : NonConstantExpr(Select, SortBitVec(8), 8) {
      assert(e->kind() == Array || e->kind() == Store);

      addChild(e);
      addChild(index);
    }

    static bool classOf(const Expr& e) { return e.kind() == Select; }

  protected:
    std::string getName() const override {
      return "Select ";
    }

    ExprRef evaluateImpl() override;
  };

  class ParaExpr : public Expr {
  public:
    ParaExpr(SortRef sort) : Expr(Para, sort, 0) {}
    static bool classOf(const Expr& e) { return e.kind() == Para; }
    void print(ostream& os, UINT depth) const override;

  protected:
    std::string getName() const override { return "Para";}
    ExprRef evaluateImpl() override;
  };

  class FuncExpr : public Expr {
  public:
    FuncExpr(SortRef sort, const std::vector<ExprRef>& para_sort)
      : Expr(Func, sort, 0) {
      for (ExprRef p : para_sort)
        addChild(p);
    }
    static bool classOf(const Expr& e) { return e.kind() == Func; }
    void print(ostream& os, UINT depth) const override;

  protected:
    std::string getName() const override { return "Func";}
    ExprRef evaluateImpl() override;
  };

  class AppExpr : public Expr {
  public:
    ExprRef funcExpr;
    AppExpr(ExprRef func, const std::vector<ExprRef> &para_expr)
      : Expr(App, func->getSort(), 0) {
      funcExpr = func;
      for (ExprRef e : para_expr)
        addChild(e);
    }
    static bool classOf(const Expr& e) { return e.kind() == App; }
    void print(ostream& os, UINT depth) const override;
  protected:
    std::string getName() const override { return "App";}
    ExprRef evaluateImpl() override;
  };

// utility functions
  bool isZeroBit(ExprRef e, UINT32 idx);
  bool isOneBit(ExprRef e, UINT32 idx);
  bool isRelational(const Expr* e);
  bool isConstant(ExprRef e);
  bool isConstSym(ExprRef e);
  UINT32 getMSB(ExprRef e);

} // namespace qsym
#endif // QSYM_EXPR_H_