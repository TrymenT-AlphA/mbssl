#pragma once

#include <ostream>

#include "Support/Ref.h"

namespace MBSSL {

enum class Response_t {
  Sat,
  Unsat,
  Unknown,
};

class Response {
 protected:
  Response_t response_t;

 public:
  [[nodiscard]] explicit Response(
    Response_t response_t
  ) noexcept;

  [[nodiscard]] Response_t getResponseT() const noexcept;

  [[nodiscard]] bool isSat() const noexcept;
  [[nodiscard]] bool isUnsat() const noexcept;
  [[nodiscard]] bool isUnknown() const noexcept;

  friend std::ostream& operator<<(std::ostream& os, const Response& res) noexcept;
};

[[nodiscard]] ref<Response> Sat() noexcept;
[[nodiscard]] ref<Response> Unsat() noexcept;
[[nodiscard]] ref<Response> Unknown() noexcept;

}  // namespace MBSSL
