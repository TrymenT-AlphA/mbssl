#pragma once

#include <map>

#include "Support/Ref.h"
#include "Expr/expr.h"
#include "Response.h"

using namespace qsym;

namespace MBSSL {

class EngineInterface {
 public:
  // add by chk
  using ConsVar_t = std::map<std::string, ExprRef>;

 public:
  virtual ~EngineInterface() = default;

 public:
  virtual ConsVar_t& ConsVar() noexcept = 0;

  virtual void Assert(ExprRef expr) noexcept = 0;
  [[nodiscard]] virtual ref<Response> CheckSat() noexcept = 0;
  virtual void ResetAssertions() noexcept = 0;

  [[nodiscard]] virtual ref<Response> Prove(ExprRef expr) const noexcept = 0;

  // get assignment
  virtual void getAssignFromModel() noexcept = 0;
};

}
