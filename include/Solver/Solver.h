#pragma once

#include <list>
#include <stack>

#include "Support/Ref.h"

#include "Expr/expr.h"
#include "EngineInterface.h"

namespace MBSSL {

class Solver {
 protected:
  using AssertionLevel_t = std::list<ref<Expr>>;
  using AssertionStack_t = std::stack<ref<AssertionLevel_t>>;

  ref<EngineInterface> engine;
  ref<AssertionStack_t> assertion_stack;
  ref<AssertionLevel_t> current_assertion_level;

 public:
  [[nodiscard]] explicit Solver(
    ref<EngineInterface> engine
  ) noexcept;

  [[nodiscard]] ref<EngineInterface> getEngine() const noexcept { return engine; }

  void Assert(ref<Expr> expr) noexcept;
  void Push() noexcept;
  void Pop() noexcept;
  void ResetAssertions() noexcept;
  [[nodiscard]] ref<Response> CheckSat() const noexcept;
};

}  // namespace MBSSL
