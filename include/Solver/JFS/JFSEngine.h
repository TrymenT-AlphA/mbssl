#pragma once

#include "Solver/EngineInterface.h"

#include "Support/Ref.h"
#include "Expr/expr.h"

#include <optional>
#include <vector>

#include "jfs/JFSBuilder.h"
#include "jfs/Core/JFSContext.h"
#include "jfs/Core/ScopedJFSContextErrorHandler.h"
#include "jfs/Core/ToolErrorHandler.h"

namespace MBSSL {
  namespace JFS {

    class JFSEngine {
      using ConsVar_t = std::map<std::string, ExprRef>;
      using Assign_t  = std::map<std::string, std::vector<UINT8>>;

    protected:
      ConsVar_t cons_var;
      Assign_t lastAssign;
      std::string filename;
      std::optional<bool> success;
      __attribute__((unused)) std::map<std::string, uint64_t> fuzzSeeds;

      klee::JFSSolver jfsSolver;

    public:
      JFSEngine(std::string filename, std::string pathToExecutable);
      ~JFSEngine() {};

      ConsVar_t& ConsVar() noexcept;

      void Assert(ref<Expr> expr) noexcept;
      ref<Response> CheckSat() noexcept;
      void ResetAssertions() noexcept;
      ref<Response> Prove(ref<Expr> expr) const noexcept;

      void getAssignFromModel() noexcept;
    };

  }
}
