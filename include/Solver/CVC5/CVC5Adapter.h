#pragma once

#include <unordered_map>

// #include "cvc5.h"
#include <cvc5/cvc5.h>

#include "Support/Ref.h"
#include "Support/Error.h"

#include "Expr/expr.h"
#include "Expr/sort.h"
#include "Solver/Response.h"

// #include "Expr/sort.h"
// #include "Expr/expr.h"
// #include "IR/Function.h"
// #include "Solver/Response.h"

using namespace qsym;

namespace MBSSL {
namespace CVC5 {

  struct ExprHash  {
    unsigned operator()(const ExprRef &e) const { return e->hash(); }
  };

  struct ExprCmp {
    bool operator()(const ExprRef &a, const ExprRef &b) const {
      return a==b;
    }
  };

  template <class T>
  class ExprHashMap : public std::unordered_map<ExprRef, T, ExprHash, ExprCmp> {};

class CVC5Builder {
  using FromResponse_t = cvc5::Result;
  using ConsCache_t = std::unordered_map<ref<Expr>, cvc5::Term>;
  using FuncCache_t = std::unordered_map<ref<Expr>, cvc5::Term>;
  using Factory_t = cvc5::Solver;

public:
  cvc5::Solver solver;
  CVC5Builder() {
    solver.setOption("produce-models", "true");
  };

  cvc5::Term toExpr(ref<Expr> expr);
  ref<Response> toResponse(cvc5::Result res);

public:
  ExprHashMap<cvc5::Term> consCache;
  ExprHashMap<cvc5::Term> funcCache;

private:
  // cvc5::Sort getRoundingMode();
  cvc5::Sort getBvSort(unsigned width);
  cvc5::Term getTrue();
  cvc5::Term getFalse();
  cvc5::Term varExpr(const std::string &name, cvc5::Sort sort);

  cvc5::Sort toCVC5Sort(ref<Sort> sort);
  cvc5::Term toCVC5Expr(ref<Expr> expr);
  cvc5::Term toCVC5ExprActual(ref<Expr> expr);
  cvc5::Term toCVC5Func(ref<FuncExpr> func);
  // [[nodiscard]] explicit Adapter() noexcept;

  // [[nodiscard]] ref<ToSort_t> operator()(ref<Sort> sort);
  // [[nodiscard]] ref<ToExpr_t> operator()(ref<Expr> expr);
  // [[nodiscard]] ref<ToFunc_t> operator()(ref<Expr> func);
  // [[nodiscard]] ref<ToFunc_t> toCVC5Func(ref<Expr> func);
  // [[nodiscard]] ref<Response> operator()(ref<FromResponse_t> res);

};

}
}
