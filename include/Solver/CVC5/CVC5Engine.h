// #pragma once

// #include "Engine/EngineInterface.h"
// #include "Engine/CVC5/CVC5Adapter.h"

// namespace MBSSL {
// namespace CVC5 {

// class Engine : public EngineInterface {
//  protected:
//   using Adapter_t = Adapter;
//   using Solver_t = cvc5::Solver;

//   Adapter_t adapter;
//   Solver_t solver;

//  public:
//   [[nodiscard]] explicit Engine() noexcept;

//  public:
//   void Assert(ref<Expr> expr) noexcept;
//   [[nodiscard]] ref<Response> CheckSat() noexcept;
//   void ResetAssertions() noexcept;

//   [[nodiscard]] ref<Response> Prove(ref<Expr> expr) const noexcept;
// };

// }
// }

#pragma once

#include "Solver/EngineInterface.h"
#include "Solver/CVC5/CVC5Adapter.h"

namespace MBSSL {
namespace CVC5 {

class Engine : public EngineInterface {
 protected:
  using Adapter_t = CVC5Builder;
  // using Solver_t  = z3::solver;
  using Solver_t = cvc5::Solver;
  // using Model_t   = z3::model;
  using Model_t = std::string;
  using Assign_t  = std::map<std::string, std::vector<UINT8>>;

  Adapter_t adapter;
  Solver_t  solver;

  ConsVar_t cons_var;

  // get assignment
  // Model_t getModel() noexcept;

  Model_t getModel() noexcept;

 public:
  Engine() noexcept;

 public:
  ConsVar_t& ConsVar() noexcept;

  void Assert(ref<Expr> expr) noexcept;
  ref<Response> CheckSat() noexcept;
  void ResetAssertions() noexcept;
  ref<Response> Prove(ref<Expr> expr) const noexcept;

  void getAssignFromModel() noexcept;
};

}
}

