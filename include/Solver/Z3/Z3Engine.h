#pragma once

#include "Solver/EngineInterface.h"
#include "Solver/Z3/Z3Adapter.h"

namespace MBSSL {
namespace Z3 {

class Engine : public EngineInterface {
 protected:
  using Adapter_t = Z3Builder;
  using Solver_t  = z3::solver;
  using Model_t   = z3::model;
  using Assign_t  = std::map<std::string, std::vector<UINT8>>;

  Adapter_t adapter;
  Solver_t  solver;

  ConsVar_t cons_var;

  // get assignment
  Model_t getModel() noexcept;

 public:
  Engine() noexcept;

 public:
  ConsVar_t& ConsVar() noexcept;

  void Assert(ref<Expr> expr) noexcept;
  ref<Response> CheckSat() noexcept;
  void ResetAssertions() noexcept;
  ref<Response> Prove(ref<Expr> expr) const noexcept;

  void getAssignFromModel() noexcept;
};

}
}
