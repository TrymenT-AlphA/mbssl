#pragma once

#include <unordered_map>

#include "z3.h"
#include "z3++.h"

#include "Support/Ref.h"
#include "Support/Error.h"

#include "Expr/expr.h"
#include "Expr/sort.h"
#include "Solver/Response.h"

using namespace qsym;

namespace MBSSL {
namespace Z3 {
  template <typename T> class Z3NodeHandle {
    // Internally these Z3 types are pointers
    // so storing these should be cheap.
    // It would be nice if we could infer the Z3_context from the node
    // but I can't see a way to do this from Z3's API.
  protected:
    T node;
    ::Z3_context context;

  public:
    // To be specialised
    inline ::Z3_ast as_ast();

  public:
    Z3NodeHandle() : node(NULL), context(NULL) {}
    Z3NodeHandle(const T _node, const ::Z3_context _context)
        : node(_node), context(_context) {
      if (node && context) {
        ::Z3_inc_ref(context, as_ast());
      }
    };
    ~Z3NodeHandle() {
      if (node && context) {
        ::Z3_dec_ref(context, as_ast());
      }
    }
    Z3NodeHandle(const Z3NodeHandle &b) : node(b.node), context(b.context) {
      if (node && context) {
        ::Z3_inc_ref(context, as_ast());
      }
    }
    Z3NodeHandle &operator=(const Z3NodeHandle &b) {
      if (node == NULL && context == NULL) {
        // Special case for when this object was constructed
        // using the default constructor. Try to inherit a non null
        // context.
        context = b.context;
      }
      assert(context == b.context && "Mismatched Z3 contexts!");
      // node != nullptr ==> context != NULL
      assert((node == NULL || context) &&
             "Can't have non nullptr node with nullptr context");

      if (node && context) {
        ::Z3_dec_ref(context, as_ast());
      }
      node = b.node;
      if (node && context) {
        ::Z3_inc_ref(context, as_ast());
      }
      return *this;
    }

    // add by zgf
    bool isNull() const { return node == NULL; }

    // To be specialised
    void dump();

    operator T() const { return node; }
  };

  // Specialise for Z3_sort
  template <> inline ::Z3_ast Z3NodeHandle<Z3_sort>::as_ast() {
    // In Z3 internally this call is just a cast. We could just do that
    // instead to simplify our implementation but this seems cleaner.
    return ::Z3_sort_to_ast(context, node);
  }
  typedef Z3NodeHandle<Z3_sort> Z3SortHandle;
  template <> void Z3NodeHandle<Z3_sort>::dump() __attribute__((used));

  // Specialise for Z3_ast
  template <> inline ::Z3_ast Z3NodeHandle<Z3_ast>::as_ast() { return node; }
  typedef Z3NodeHandle<Z3_ast> Z3ASTHandle;
  template <> void Z3NodeHandle<Z3_ast>::dump() __attribute__((used));

  // add by zgf : Specialise for Z3_func_decl
  template <> inline ::Z3_ast Z3NodeHandle<Z3_func_decl>::as_ast() {
    // In Z3 internally this call is just a cast. We could just do that
    // instead to simplify our implementation but this seems cleaner.
    return ::Z3_func_decl_to_ast(context, node);
  }
  typedef Z3NodeHandle<Z3_func_decl> Z3FuncDeclHandle;
  template <> void Z3NodeHandle<Z3_func_decl>::dump() __attribute__((used));

  struct ExprHash  {
    unsigned operator()(const ExprRef &e) const { return e->hash(); }
  };

  struct ExprCmp {
    bool operator()(const ExprRef &a, const ExprRef &b) const {
      return a==b;
    }
  };

  template <class T>
  class ExprHashMap : public std::unordered_map<ExprRef, T, ExprHash, ExprCmp> {};

//  struct Adapter {
//    using FromResponse_t = z3::check_result;
//    using ConsCache_t = std::unordered_map<ref<Expr>, Z3ASTHandle>;
//    using FuncCache_t = std::unordered_map<ref<Expr>, Z3FuncDeclHandle>;
//    using Factory_t = z3::context;
//
//    ConsCache_t cons_cache;
//    FuncCache_t func_cache;
//    z3::context factory;
//
//    explicit Adapter() noexcept;
//
//    Z3SortHandle toSort(ref<Sort> sort);
//    Z3ASTHandle toExpr(ref<Expr> expr);
//    Z3FuncDeclHandle toFunc(ref<FuncExpr> func);
//    ref<Response> toResponse(FromResponse_t res);
//
//  };

  class Z3Builder{
    using FromResponse_t = z3::check_result;
    using ConsCache_t = std::unordered_map<ref<Expr>, Z3ASTHandle>;
    using FuncCache_t = std::unordered_map<ref<Expr>, Z3FuncDeclHandle>;
    using Factory_t = z3::context;

  public:
    z3::context context;
    Z3Builder() {};
    Z3_context getCtx() { return (Z3_context) context;}

    z3::expr toExpr(ref<Expr> expr);
    ref<Response> toResponse(z3::check_result res);

  private:
    ExprHashMap<Z3ASTHandle> consCache;
    ExprHashMap<Z3FuncDeclHandle> funcCache;

    Z3ASTHandle getRoundingMode();
    Z3SortHandle getBvSort(unsigned width);
    Z3ASTHandle getTrue();
    Z3ASTHandle getFalse();
    Z3ASTHandle varExpr(const std::string &name, Z3SortHandle sort);

    Z3SortHandle toZ3Sort(ref<Sort> sort);
    Z3ASTHandle toZ3Expr(ref<Expr> expr);
    Z3ASTHandle toZ3ExprActual(ref<Expr> expr);
    Z3FuncDeclHandle toZ3Func(ref<FuncExpr> func);
  };
}
}
